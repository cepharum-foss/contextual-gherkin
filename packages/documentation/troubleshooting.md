# Troubleshooting

## Solutions to common errors

### Error: history of searched elements lacks &lt;name> instances (queried as set of "&lt;alt-name>") at index &lt;num>

At first glance, this error is due to late Gherkin steps addressing elements found by earlier Gherkin steps of same Scenario. For solution, make sure

* there are Gherkin steps referring to the mentioned elements by the same name or via some aliasing.
* those Gherkin steps belong to the same Gherkin scenario for every scenario starts another collection of elements tracked for contextual queries.

In addition to this obvious cause, there are additional, less obvious ones:

* The failing step is using an alias which has not been properly declared, yet.
* The previously matching elements have been substituted with equivalently looking replacements.
 
  **contextual-gherkin** tries to keep track of previously matching elements moving around. But as soon as they are substituted with freshly created instances, some internally managed properties for tracking them - namely the `data-contextual-gherkin-id attribute` - might be missing so that selectors tracked in history are failing to pick up those substitutes again. As a solution, you would have to either revise the tested application's implementation to prevent those substitutions or simply repeat those previous Gherkin steps re-querying the elements more closely prior to the Gherkin step failing here.
* <Badge text="<1.1.0" type="tip"/> The query for picking up matches of a previous query for further inspection or interaction have a mismatching [cardinality](glossary.md#cardinality).

  The following example may illustrate this issue as it applies to current release of **contextual-gherkin**:

  ```gherkin
  Given there is an item with text "foo"
  And there is an item with text "bar"
  Then I click the first item
  ```

  In this example, the first two rules look up a single item each and either item is tracked in separate records of history. They have singular cardinality for they explicitly look for sole matches. The phrase `the first item` used in third rule is basically an alias of `the first of these items`. It has a plural [source cardinality](glossary.md#cardinality) and thus is searching history for a single record tracking a potential set of items. Because of that, it fails to pick up any item tracked due to matching the first two rules.

  This issue may be fixed in a future release for the phrasing isn't wrong from a natural language perspective. As a workaround, the phrasing must be adjusted to use `the former item` or `the latter item` instead of `the first item` or `the second item`.

## Code assisting and code inspection issues

### Issue

In IDEs such as Jetbrains WebStorm or Visual Studio Code, plugins for Cucumber/Gherkin exist to offer code assistance as well as to highlight issues with used Gherkin phrases. Using **contextual-gherkin**, neither work sufficiently.

### Solution

#### Visual Studio Code

* Make sure official plugin of **cucumber.io** has been installed. It is named just **Cucumber**.
* Open settings of Visual Studio Code and search for `cucumber.glue`.
* Click on **Edit in settings.json**.
* Add the following path preferably at the beginning of that list:

  `"node_modules/@cepharum/contextual-gherkin/steps/**/*.js"`

  After changing, the setting should look like this:

  ```json
  {
    "cucumber.glue": [
      "node_modules/@cepharum/contextual-gherkin/steps/**/*.js",        
      "src/test/**/*.java",
      "features/**/*.js",
      "features/**/*.jsx",
      "features/**/*.ts",
      "features/**/*.tsx",
      "features/**/*.php",
      "features/**/*.py",
      "tests/**/*.py",
      "tests/**/*.rs",
      "features/**/*.rs",
      "features/**/*.rb",
      "*specs*/**/.cs"
    ]
  }
  ```

#### Jetbrains WebStorm

:::warning Limitations
This solution basically applies to other applications of JetBrains based on IDEA, too. However, it seems to fail occasionally without regard to the application used.
:::

In WebStorm the **node_modules** folder is excluded from indexing for good reason. However, this prevents the available Cucumber plugin from finding steps defined by **contextual-gherkin**. To fix this, an exclusion from that exclusion is required per project.

* Make sure the bundled plugins **Cucumber.js** and **Gherkin** have been installed and enabled.
* In your project's explorer, 
  * expand **node_modules** folder,
  * then expand **@cepharum** folder,
  * then expand **contextual-gherkin** folder,
  * right-click on that most recently expanded folder and choose **Mark directory as** -> **Not Excluded**,
  * then repeat the last step on the sub-folders **steps** and **lib**.

:::tip Input is welcome!
You know a better solution to this? Feel free to open [an issue at our repository](https://gitlab.com/cepharum-foss/contextual-gherkin/-/issues).
:::
