# Glossary

This page is explaining a few terms essential to understanding this library.

## Cardinality

Any Gherkin phrase referring to items matching a previous phrase relies on either phrase's _cardinality_ to match. A phrase's cardinality is

* `singular` when it clearly addresses a single item,
* `plural` when it clearly addresses multiple items or
* unknown, thus _nullish_, when it fails to fit into either category.

::: warning Cardinality isn't quantity
The term `buttons` has a plural cardinality. However, this doesn't mean it actually matches multiple buttons. The following sequence also works if a single button is matching, only:

```gherkin
Given there are buttons
And they are enabled
```

Cardinality is solely based on assumptions made due to the phrases' language.
:::

The cardinality is used in tracking matches in a history of matches to establish [contextuality](#contextuality):

* If a phrase is referring to a previous phrase's subject with singular cardinality, it is focusing on phrases tracked there with singular cardinality, only. 
* Same applies to phrases referring to a previous one using plural cardinality accordingly. 
* If a phrase's cardinality is unknown, any previous phrase is considered no matter its cardinality when looking up history.

:::info Example  
Consider this sequence of phrases:

```gherkin
Given there are three buttons
And the first of these buttons reads "Mark complete"
And this button is enabled
```

* The cardinality of first phrase is `plural`. 
* Second phrase is a contextual one. It is looking for previously searched "buttons", thus has a `plural` _source cardinality_. However, it is looking into one of those buttons, only, so its _target cardinality_ is `singular`. That becomes essential in third phrase.
* Third one is another contextual phrase looking for a single button, thus having a `singular` source cardinality. Because of that it is considered expressing something in context of second phrase instead of first one.

  Changing this phrase to

  ```gherkin
  And these buttons are enabled
  ```
  
  its source cardinality changes to `plural` and so the test is addressing buttons matching first phrase instead of second one.
:::

## Contextuality

This library implements two kinds of phrases for common use: global and contextual ones.

Contextual phrases pick up one or more items addressed in a previous phrase to apply another test or start another search in their context. Because of that, any such previous phrase is considered to provide a _context_.

In opposition to that, global phrases don't work in context of a previous one but address one or more items somewhere in the current view of your application. However, they usually provide a context as well.

Multiple phrases searching in context of a previous phrase generate a hierarchy of contexts:

```gherkin
Given there is a table
And it has items
And the first of them has buttons
When I click on the second button with label "Remove"
Then the item does not exist anymore
```

The clicked button is in context of first item, which is in context of all items, which are in context of a table.

By using contextual phrases a huge number of test cases can be expressed with a limited set of generic phrases.

:::info Example
The phrase

```gherkin
Given there is a button labelled with "Add to cart"
```

is a global phrase as it does not refer to any previous phrase.

In opposition to that, the phrase

```gherkin
Given it has a button labelled with "Add to cart"
```

is referring to some single-item context in which the searched button has to exist.
:::

## Cucumber expression

There is a dedicated [page introducing custom Cucumber expressions](api/expressions.md).

## Normalized string

When searching elements by label or text content, either candidate's label or text content is normalized prior to testing it like this:

* Any leading or trailing whitespace characters are removed.
* Any (sequence of) in-between whitespace characters are replaced with a single [space](https://en.wikipedia.org/wiki/Space_(punctuation)) character.

This way, testing for labels and text content is possible even though they might have been injected into tested pages in multiple different ways.

## Selector

There is a dedicated [page introducing selectors](api/selectors.md).

## Phrases

Phrases are used in Gherkin statements. Based on embedded [Cucumber expressions](api/expressions.md), multiple phrases usually match a certain phrasing pattern.

There is a [reference of phrasing patterns](api/phrases.md) provided by **contextual-gherkin**.
