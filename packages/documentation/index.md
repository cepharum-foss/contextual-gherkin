---
layout: home

hero:
  name: Contextual Gherkin
  tagline: Natural language E2E testing
  actions:
  - theme: brand
    text: Get started!
    link: /tutorial/quickstart.html

features:
  - title: Closer to natural language
    details: Using short generic phrases in Gherkin enables easy-to-read user stories. Less involvement of developers improves efficiency of writing tests.
    link: /about.html
    linkText: Read more
  - title: Contextual phrasing
    details: Supported phrases can relate to each other to describe preconditions, actions and assertions in context of subjects of previous phrases.
    link: /api/phrases.html
    linkText: Phrases reference
  - title: Extensible
    details: This library is integrating with a project's E2E testing setup based on TestCafé and Cucumber. This way you can add project-specific phrases at any time.
    link: /tutorial/custom-steps.html
    linkText: Developer information
  - title: Custom Expressions
    details: Provided phrases rely on several custom Cucumber expressions which you can use in project-specific phrases, too.
    link: /api/expressions.html
    linkText: Expressions reference
---
