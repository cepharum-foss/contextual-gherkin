---
outline: [2,3]
---

# Development API

**contextual-gherkin** includes an API for implementing step definitions that deal with contextual relationships between each other by means of enabling one step to act on those elements of your web application that another step has looked up and tested before. This API is used to implement the existing phrases that are included with **contextual-gherkin** out-of-the-box. It is made available to you as well to simplify the [implementation of additional step definitions](../tutorial/custom-steps.md). This page is providing a brief overview of this _development API_.

## Technical background

If you invoke the tests on your device, they will be performed in a local process. In a CI pipeline, tests are performed on a CI runner which is often a headless runtime environment based on Linux. You can run tests in a Docker container as well, and they will be performed in the main process of that container.

In either case, this process is based on [TestCafe](https://testcafe.io/) which is providing its own API for controlling one or more web browsers through a _test controller_. When implementing step definitions, you are basically writing code snippets for TestCafe each performing the particular intentions of either Gherkin-based step.

When interacting with a web browser controlled via TestCafe, some code has to be run in context of that browser from time to time. That code is _never_ run in the same process as the one your tests are running in. TestCafe is serializing functions and simple data they might need to access prior to transferring everything to the attached browser for deserializing and running there. Some [limitations apply](https://testcafe.io/documentation/402832/guides/basic-guides/client-functions#limitations), though. 

Transfer and remote execution happen behind the scenes, and you might not be aware of it most of the time. As it takes a moment to complete, you have to use `await` on functions that rely on code run in remote browser.

**Keep this in mind as it is a very common cause for heaving issues with your custom implementations.**

## Integration basics 

This library provides a single helper function `ContextualGherkin()` for integrating itself with a test runner based on TestCafe. It takes the runner's test controller instance as mandatory argument and either injects a customized instance of the library into the controller's context or reads it back from there.

### Configure and inject

The first part has been illustrated as part of the tutorial to get you started before. It usually resembles this template:

```javascript
const { Before } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Before( testController => {
	ContextualGherkin( testController, {
		// TODO put your configuration here
	} );
} );
```

The hook `Before()` is used to gain access at the test controller prior to running any test. It is then used to invoke the helper function together with [your particular runtime configuration](configuration.md). On providing a configuration, another instance of this development API is created and injected into the provided test controller replacing any previously injected one.

### Access

After that, gaining access on injected development API is as simple as invoking the helper function on the same test controller again, this time without providing a configuration:

```javascript{7}
const { Given } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Given( testController => {
	const developmentApi = ContextualGherkin( testController );
	
	// TODO use development API
} );
```

During a single test run, all hooks of Cucumber such as `Given()` and `Before()` are invoked with the same test controller instance.

:::tip Keep as template  
This code example above provides the context for examples given on this page most of the time.  
:::


## API methods

The API itself provides methods for globally inspecting the document which is currently presented by the browser and for recovering the result of previous inspections as _context_ of follow-up inspections and interactions. 

### api.find()

```javascript
const context = await api.find( typeName, defaultSelector );
```

This method is looking for elements of a named type in whole browser document.

It's using a fixed [context](#context-basics) representing the document itself to use its [`find()` method](#context-find).

:::info Example
```javascript
const context = await api.find( "button" );
```

This provides a context referring to all elements of browser document that are considered _buttons_.
:::

:::tip  
Use this method in step definitions for global queries, actions or inspections.  
:::

### api.getContextFor()

```javascript
const recovered = await api.getContextFor( contextualWord );
```

This method is fetching a previously tracked context from the history of contexts. The delivered entry is addressed by provided information which is expected to result from matching a Cucumber expression of type [`{contextual-word}`](expressions.md#contextual-word).

:::tip  
Use this method in step definitions for local queries, actions or inspections in context of a previous step.  
:::

## Context basics

Contexts are resulting from searching the document in browser for elements matching certain criteria. They can be tracked in multiple sequences commonly referred to as the _history_ of contexts.

Every such sequence is related to a particular type of elements that has been named originally to find either set of matching elements for inspection, interaction and tracking. In addition, a separate sequence is tracking all contexts independent of the type of elements that has been queried.

## Context properties

### context.cardinality

This method provides the cardinality of current context which is based on a query for searching elements. A query can be assumed to address a single element resulting in a `singular` cardinality or multiple elements resulting in a `plural` cardinality. The cardinality is falsy if no assumption is possible.

:::warning  
Do not use this cardinality information to assume a given number of elements actually matching that original query. See the [glossary](../glossary.md#cardinality) for additional information.
:::

### context.matches

This property exposes a [Selector](https://testcafe.io/documentation/402666/reference/test-api/selector) referring to all elements matching current context. It is used on interacting with the selected elements by invoking methods of test controller.

:::info Example  
If context is resulting from processing a Gherkin step like

```gherkin
Given I click on the button
```

then this property provides a `Selector` referring to that button in browser document. You can e.g. use it to click the button

```javascript
await testController.click( context.matches );
```

or to inspect its [state](https://testcafe.io/documentation/402670/reference/test-api/domnodestate):

```javascript
if ( await context.focused ) {
	// ...
}
```
:::

:::warning Prefer context for querying  
The provided selector is basically capable of querying additional elements that are related to those it is selecting originally. See the [TestCafe documentation](https://testcafe.io/documentation/402666/reference/test-api/selector) for additional information.

You can basically use those methods for custom querying and inspections. But if you intend to track results in history for future reference, you should stick to available methods of context as often as possible instead.  
:::

### context.parent

This property is exposing the context from which the current one has been derived e.g. by searching elements subordinated to those in that context.

### context.type

This is the singular form of the name that has been used e.g. in a Gherkin step to address a type of elements to be inspected or to act on.

:::info Example  
When processing a Gherkin step like

```gherkin
Given there are three buttons
```

a context representing those buttons is created and its `type` property holds the string `button`.  
:::

## Context methods

### context.checkCardinalWord()

```javascript
await context.checkCardinalWord( cardinalWord, track );
```

This convenience helper implements a common task of several step definitions that query browser document for elements matching a certain term in a Gherkin step's phrase. It

* checks if elements of current context are actually satisfying quantity expressed by data resulting from a matching Cucumber expression of type [`{cardinal-word}`](expressions.md#cardinal-word) and
* tracks current context in history on demand after passing that check.

The second argument `track` is optional. It is `true` by default, thus demanding to track a satisfying context in history.

:::info Example
```javascript{8}
const { Then } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Then( "there is/are {cardinal-word}", async ( testController, [query] ) => {
	const lib = ContextualGherkin( testController );
	const candidates = await lib.find( query.word );

	await matches.checkCardinalWord( query );
} );
```

In this example, the invocation of `checkCardinalWord()` is asserting that e.g. there are exactly 3 elements in current context if `{cardinal-word}` is representing the term `3 buttons` or at least 2 matching elements if it is representing the term `at least two icons`. 

If the test succeeds, `matches` is tracked in history automatically so that it can be picked up in another step definition via terms such as `these buttons` or `the first icon`.
:::

### context.checkFilteredInContext()

```javascript
await context.checkCardinalWord( filteredContext, contextualWord, requireAll, track );
```

This convenience helper results from another common task found in a few step definitions. 

It is relying on the very particular precondition, that a step definition is fetching some previously tracked context as current one from history to derive another context which is reducing the set of elements based on additional constraints. This method is asserting in such cases, that (some or all) elements of current context are listed in derived one, too. 

On success, the current context gets tracked in history on demand **and** if provided information representing a Cucumber expression of type [`{contextual-word}`](expressions.md#contextual-word) is indicating that current context is a refined version of what has been found in history, only.

* Argument `contextualWord` is expected to be the one, that has been used to fetch the original context from history.
* Argument `requireAll` is optional. It is `true` by default, thus requiring all elements of current context to be part of filtered context, too.
* Argument `track` is optional. It is `true` by default, thus demanding current context to be tracked in history, if it is satisfying described assertion and has been refined from a more complex context fetched from history before.

:::warning Use with caution!  
This method heavily relies on some assumptions to apply. It may have unexpected results if those assumptions aren't met. Thus, don't use this method unless you are sure, that your code is in compliance with those assumptions as accurately as possible, e.g. by sticking to the template given by example below.  
:::

:::info Example
```javascript{8}
const { Then } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Then( "{contextual-word} is/are named 'foo'", async ( t, [ contextualWord ] ) => {
	const context = await Api.access( t ).getContextFor( contextualWord );
	const filtered = await context.withAttribute( "name", "foo" );

	await context.checkFilteredInContext( filtered, contextualWord );
}
```

The step definition is picking up some previous context, then applies a filter to that context to see how many elements of it satisfy a certain constraint. After that, the filtered context is tested to contain all elements of current context. This is identical to the fact, that the constraint applied to the filtered context is satisfied by all elements of current context.

If that's the case **and** the term represented by `contextualWord` indicates a refinement of provided context, then `context` is tracked in history. Refinement is indicated for terms like `the first of these icons` as the fetched context is a reduced version of what has been found in history before.
:::

### context.filter()

```javascript
const newContext = await context.filter( typeName, defaultSelector );
```

This method picks all those matching elements in current context that match a selector for named type as well and provides them in another context. 

The type name is expected to be given in singular form. It is mapped into a selector using [runtime configuration](configuration.md#selectors). If no selector is found there, some optionally provided default selector is used as fallback.

The selector is tested on every matching element of current context. The resulting context is listing those elements matching the selector, only.

### context.filterFn()

```javascript
const newContext = await context.filterFn( callback, closureScope );
```

This method is invoking provided callback function for every matching element of current context. It returns another context that's representing only those elements for which the callback has returned a truthy result. 

:::warning Possible error cause  
The callback is invoked in browser process, thus some limitations apply. A set of named values can be provided as object in second argument to serve as closure scope of invoked function.  
:::

:::info Example
```javascript
const newContext = await context.filterFn( 
	node => node.classList.contains( tag ), 
	{ tag: "active" } 
);
```

The resulting context covers all those elements matching current one that also have a class `active`.  
:::

### context.filterBySubsWithFn()

```javascript
const newContext = await context.filterBySubsWithFn( typeName, defaultSelector, callback, closureScope );
```

This method is a convenience helper basically combining methods `find()` and `filterFn()` to create a context containing those elements of current context which have a subordinated element satisfying a provided callback.

Arguments `typeName` and `defaultSelector` are used to find elements subordinated to those elements of current context. Arguments `callback` and `closureScope` are then used to test every found subordinated element. Every element of current context, which has at least one subordinated element for which the callback returned a truthy result, is eventually covered by returned context, too.

:::warning Possible error cause  
The callback is invoked in browser process, thus some limitations apply. A set of named values can be provided as object in second argument to serve as closure scope of invoked function.  
:::

:::info Example
```javascript
const newContext = await context.filterBySubsWithFn(
	"$tag", false,
	node => node.classList.contains( tag ), 
	{ tag: "active" } 
);
```

The resulting context represents all those elements matching current one that have a constituent matching some custom [part selector](selectors.md#part-selectors) with class `active` being set. If no matching part selector has been configured, those elements of current context are tested by default.
:::

### context.filterBySubsWithText()

```javascript
const newContext = await context.filterBySubsWithText( typeName, defaultSelector, text, partial );
```

This method is another convenience helper based on `filterBySubsWithFn()` for limiting elements of current context to those having a constituent with textual content (completely or partially) matching provided text. Either tested element's textual content gets [normalized](../glossary.md#normalized-string) before testing.

* The provided `text` is either a string or a regular expression which has to match.
* Argument `partial` is optional and 'false' by default. If true, a string or regular expression given as `text` does not have to match the whole value but only a part of it.

:::tip  
This function is used internally to find elements that have a given text or label.  
:::

:::info Example
```javascript
const newContext = await context.filterBySubsWithText( "$text", false, "Hello!" );
```

The resulting context represents all those elements matching current one that have textual content `Hello!`.  
:::

### context.find()

```javascript
const newContext = await context.find( typeName, defaultSelector );
```

This method queries the browser document for elements subordinated to the ones matching current context looking for those subordinated elements that share the type named in first argument. 

The type name is expected to be given in singular form. It is mapped into a selector using [runtime configuration](configuration.md#selectors). If no selector is found there, some optionally provided default selector is used as fallback. 

The selector is used to pick elements of the browser document and return another context describing those elements.

:::info Example  
```javascript
const options = await list.find( "option" );
```

Assuming that `list` is a context representing a single list element, this method is providing another context representing all available elements considered _options_ of that list. Runtime configuration is required to provide information on how to identify a list's options. 
:::

### context.getSelector()

```javascript
const { query, selectors, dependencies } = await context.getSelector( typeName, fallbackSelector );
```

Queries configured map of selectors for the selector matching named type of elements.

Mandatory argument `typeName` is a string naming a type of element e.g. requested by a term matching a Cucumber expression of type [`{cardinal-word}`](expressions.md#cardinal-word). It may be a [virtual selector](selectors.md#virtual-selectors), too.

Argument `fallbackSelector` is optional. It provides a selector to use if none has been configured for named type. If omitted, an exception is thrown if no selector has been found. Provide `false` to request nullish result returned instead of throwing exception.

The method returns found selector and related information as an object consisting of these properties:

* `query` provides the selector to use for querying browser document for elements of named type. It is either a string, a callback function to invoke in browser runtime or `false` to request current elements being used instead of some subordinated ones.
* `selectors` optionally delivers a subset of selectors to prefer on looking up further selectors in context of elements picked by provided query (see [selector hierarchies](selectors.md#selector-hierarchies)).
* `dependencies` is optionally available if `query` is a function to invoke in runtime of browser for picking matching elements. In this case, `dependencies` might be an object containing a custom closure scope for running that function in browser.

:::tip Use with care  
This function is useful in custom step definitions intending to support [virtual selectors](selectors.md#virtual-selectors) on inspecting constituents, attributes or DOM properties. In those cases, properties `selectors` and `dependencies` of returned object can be ignored if available.

In all other cases, you should stick to methods of this API provided to properly interact with the configured hierarchy of selectors based on intentions to find subordinated elements or to reduce set of elements in current context. 
:::
:::info Example  
```javascript
const { query } = await context.getSelector( "#style", "style" );
```

This example checks runtime configuration for a [virtual property selector](selectors.md#property-selectors) to consider on accessing a matching element's local CSS style definitions. The follow-up inspection should use DOM property named in `query` to actually read the local style per element in current context.
:::

### context.hasMatches()

```javascript
const hasMatches = await context.hasMatches();
```

This method is a trivial convenience helper checking number of elements tracked in current context that actually exist in browser document. It returns true if at least one matching element exists and false otherwise.

### context.nth()

```javascript
const newContext = await context.nth( index );
```

Use this method to pick a single element in a set of matching elements. It returns another context representing that selected element, only. The provided index is zero-based, thus first item is available at index 0.

:::info Example
```javascript
const thirdButton = await buttons.nth( 2 );
```
:::

### context.track()

```javascript
await context.track( typeName, cardinality );
```

This method puts current context on top of history of contexts.

It is tracking the context in history sequence for name type of elements and in the separate sequence tracking all contexts independently of their type. It associates either history entry with provided cardinality. 

Arguments are optional. [`context.type`](#context-type) and [`context.cardinality`](#context-cardinality) are used by default.

:::warning Prevent multiple tracking  
This method is tracking current context unconditionally. Thus, only use it unless the context has been tracked before.  
:::

### context.withAttribute()

```javascript
const newContext = await context.withAttribute( name, value );
```

The method creates another context covering only those elements of current context that have named attribute with given value.

The provided name of attribute is automatically subject to optional mapping via [attribute selector](selectors.md#attribute-selectors). The value to check is either 

* a string that has to match whole value found in attribute or 
* a regular expression to be matched by value of attribute.

:::info Example  
```javascript
const usernameInput = await inputs.withAttribute( "name", "username" );
```

This example is creating a context which is consisting of those elements in context `inputs` which have an attribute `name` with value `username`.  
:::

### context.withProperty()

```javascript
const newContext = await context.withProperty( name, value );
```

The method creates another context covering only those elements of current context that have named DOM property with given value.

The provided name of DOM property is automatically subject to optional mapping via [property selector](selectors.md#property-selectors). The value to check is either 

* a string that has to match whole value found in DOM property, 
* a regular expression to be matched by value of DOM property or
* a callback invoked with found value of named DOM property expected to return truthy result if provided value is considered matching expectations and falsy otherwise.

:::warning Possible error cause  
A provided callback is invoked in browser process, thus some limitations apply.

In opposition to other use cases, no set of data can be provided here to serve as closure scope to provided function. Thus, any provided callback should be kept as simple and pure as possible.
:::

:::info Example  
```javascript
const enabledOptions = await allOptions.withProperty( "disabled", v => !v );
```

This example is creating a context which is consisting of those elements in context `allOptions` which have a falsy DOM property `disabled`, which is: they are enabled.  
:::
