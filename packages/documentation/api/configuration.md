# Configuration

**contextual-gherkin** gets basically integrated with E2E testing using a setup hook like this:

```javascript
const { Before } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Before( testController => {
	ContextualGherkin( testController, {
		// TODO add your configuration for contextual-gherkin here
	} );
} );
```

In this, providing a custom configuration is essential. Without it, step implementations of **contextual-gherkin** fail to properly pick elements of your application for assessment.

## selectors

This mandatory option is a map of names into selectors for identifying elements of your web application or website addressed by either name. All names are provided in their singular form independent of the way they are used in test phrases.

The option is given as an object, e.g.

```javascript
ContextualGherkin( testController, {
	selectors: { // [!code focus:7]
		"list item": "li",
		"shop cart": {
			"": "#cart",
			"list item": "span.item",
		},
	},
} );
```

Please, see the [dedicated page on selectors](selectors.md) for additional information.

## aliases

Aliases establish support for different names used in [phrases](../glossary.md#phrases) to address the same kind of elements. Defining them is optional.

They are provided as a regular object mapping either alias into one or more names it is equally aliasing. 

```javascript
ContextualGherkin( testController, {
	selectors: {
		"item": "li",
	},
	aliases: { // [!code focus:3]
		item: [ "list item" ],
	},
} );
```

In this particular case, the sole aliased name can be given as string, too:

```javascript
ContextualGherkin( testController, {
	selectors: {
		"item": "li",
	},
	aliases: { // [!code focus:3]
		item: "list item",
	},
} );
```

Without declaring this alias the following test won't work:

```gherkin
Given there is a set of "list items"
And the first of these items is checked
```

The second step is referring to **items** which have been called **list items** in first step. Using aliases isn't necessary. You could also rephrase your tests, instead.

:::tip Singular aliases
When looking up aliases, the singular form is used. Thus, you should use singular form on declaring aliases, only.
:::

Circular and recursive definitions are supported, too.

```javascript
ContextualGherkin( testController, {
	selectors: {
		"item": "li",
	},
	aliases: { // [!code focus:4]
		item: [ "list item", "bullet point" ],
		"list item": "item",
	},
} );
```

In this example, names **item** and **list item** are aliasing each other. Aliases for **bullet point** are **item** and **list item**.
