---
outline: [2,3,4]
---

# Custom Cucumber expressions

**contextual-gherkin** implements some custom [Cucumber expressions](https://github.com/cucumber/cucumber-expressions#readme) to help implementing step definitions based on partial expressions closer to English language. Support for slightly different phrases meant to describe the same is essential for achieving that. By using custom Cucumber expressions, the number of alternative phrases per step definition can be significantly reduced.

After integrating **contextual-gherkin** with your application, you can use those Cucumber expressions in [your own step definitions](../tutorial/custom-steps.md), too.

## \{count}

This expression matches sequences of digits or any of the following numerals expressing a cardinal amount:

`no`, `zero`, `one`, `two`, `three`, `four`, `five`, `six`, `seven`, `eight`, `nine`, `ten`

:::info Example
The pattern

```gherkin
Given there is/are {count} button(s)
```

matches any of these phrases:

* Given there is **no** button
* Given there are **5** buttons
* Given there is **one** button
:::

::: details For developers
Using `{count}` in a step definition, the resulting argument is a `Number` matching expressed amount.
:::

## \{string-or-word}

This expression is a combination of `{string}` and `{word}` as [supported by original Cucumber expressions library](https://github.com/cucumber/cucumber-expressions#readme). You should use it as a replacement for `{word}` and `{string}` to support simple keywords and complex strings at the same time.

It matches 

* a sequence of characters including whitespace that is wrapped in double quotes or
* any other sequence of characters excluding whitespace.

:::info Example
The phrase pattern

```gherkin
Given there is/are a {string-or-word}
```

matches any of these phrases:

* Given there is a **button**
* Given there is a **"house full of nothing"**
:::

::: details For developers
Using `{string-or-word}` in a step definition, the resulting argument is a `String` containing the actually provided word or string with any enclosing double quotes being stripped off.
:::

## \{word-or-string}

This expression is an alias for `{string-or-word}`. It is provided to support a relaxed Gherkin authoring.

## \{text-or-regexp}

This expression is basically another combination of `{string}` and `{word}`. But in addition to [`{string-or-word}`](#string-or-word) regular expressions are accepted, too, and returned as such.

It matches

* a sequence of characters including whitespace that is wrapped in double quotes,
* a sequence of characters including whitespace that is wrapped in a pair of forward slashes or
* any other sequence of characters excluding whitespace.

When matching a pair of forward slashes, a single `i` immediately following the closing forward slash is accepted as a modifier for resulting regular expression. A literal forward slash in a regular expression is supported, but _always_ requires a preceding backslash for escaping.

:::info Example
The phrase pattern

```gherkin
Given there is/are a {text-or-regexp}
```

matches any of these phrases:

* Given there is a **button**
* Given there is a **"house full of nothing"**
* Given there is a **/house full of nothing|everything/**
* Given there is a **/HOUSE FULL OF NOTHING|EVERYTHING/i**  
* Given there is a **/HOUSE FULL OF NOTHING|EVERY[\\/]?THING/i**

Last example is illustrating how a literal forward slash needs to be escaped even when used in situations where this isn't usually required.
:::

::: details For developers
Using `{text-or-regexp}` in a step definition, the resulting argument is either a `String` containing the actually provided word or string with any enclosing double quotes being stripped off or an instance of `RegExp`.
:::

## \{cardinal-word}

Though implemented independently, this expression can be thought of as an _optional_ concatenation of `{count}` and `{string-or-word}` optionally prefixed by another modifier controlling how to compare given count in assertions. It is available to commonly describe a homogenous set of elements to be found, to act on or to be tested.

Supported values include simple combinations such as

* `a button`, `the button`, `no house` or `six "red flags"` and
* such phrases prefixed with one out of the modifiers `at least`, `at most` or `up to`.

As a special case, a word or string in plural form without any quantity or modifier is valid, too, e.g. `buttons` or `"red flags"`.

:::info Example
The phrase pattern

```gherkin
Given there is/are {cardinal-word}
```

matches any of these phrases:

* Given there is **a button**
* Given there is **the button**
* Given there are **the buttons**
* Given there is **one button**
* Given there are **at least three buttons**
* Given there are **up to 20 icons**
* Given there are **64 "photo thumbnails"**
:::

:::warning Limited support for `the`  
Modifiers can't be combined with quantifier `the` due to bad phrasing.  
:::

:::: details For developers
When using `{cardinal-word}` in a step definition, the extracted information is represented by an object providing

* the cardinal amount in property `number`,
* the word (or string) in property `word` with any double quotes stripped off, representing the singular form of actually found word whenever possible,
* the desired operator for comparing some quantity with this expression in property `mode` and
* the expression's [cardinality](../glossary.md#cardinality) in property `cardinality`.

The mode is one out of the strings `"=="`, `">="` or `"<="`. It can be read like

`some-quantity` `obj.mode` `obj.number`

e.g. `some quantity` `<=` `3`

To conclude, the fourth example given above would result in argument similar to this object:

```javascript
{
	word: "icon", // obey the singular form!
	number: 20,
	mode: "<=",
	cardinality: "plural",
}
```

:::warning Possible collisions  
Expression `{cardinal-word}` might collide with `{contextual-word}` due to supporting quantifier `the`, too. Keep this in mind when writing custom phrases based on either expression.
:::
::::

## \{ordinal-word}

This type of expression is similar to a `{cardinal-word}`. It is combining an ordinal number with a word or string. The ordinal number may be given as a sequence of digits resembling a positive integer and a final period or by using one of the following literal keywords:

`first`, `second`, `third`, `fourth`, `fifth`, `sixth`, `seventh`, `eighth`, `ninth`, `tenth`, `1st`, `2nd`, `3rd`, `4th`, `5th`, `6th`, `7th`, `8th`, `9th`, `10th`

In opposition to `{cardinal-word}`, the ordinal number is mandatory.

:::info Example
The pattern

```gherkin
When I click on the {ordinal-word}
```

matches any of these phrases:

* When I click on the **1. button**
* When I click on the **first button**
* When I click on the **1st button**
:::

::: details For developers
When using `{ordinal-word}` in a step definition, the extracted information is represented by an object providing

* the ordinal number as zero-based index in property `number`,
* the word (or string) in property `word` accepting a singular form, only,
* the expression's fixed mode of comparison in property `mode` which is always `"=="` and
* the expression's [cardinality](../glossary.md#cardinality) in property `cardinality` which is always `singular`.

The resulting argument for any of the examples given above would be an object like this one:

```javascript
{
	word: "button", // obey the singular form!
	number: 0,      // zero-based variant of "first"
	mode: "==",
	cardinality: "singular",
}
```
:::

## \{contextual-word}

This expression is most essential in phrasing complex relationships between Gherkin phrases. Whatever one phrase was addressing e.g. using a `{cardinal-word}` expression, another phrase can refer to using a `{contextual-word}`.

Phrases matching `{contextual-word}` can be very simple as well as very complex. They always include some keyword expressing the reference to some particular element or to a set of elements that has/have been found or tested before.

All matching phrases fall in one of two sections: non-refining and refining ones.

### Non-refining contextual words

Let's start with the simpler ones. Non-refining phrases are addressing a set of elements as processed in a previous Gherkin phrase and selects it for re-processing as a whole.

#### it

This is the simplest contextual word. It is referring to the most recently processed single element that has a [singular cardinality](../glossary.md#cardinality).

:::info Example  
```Gherkin{2}
Given there is a button
And I click on it
```
:::

Using `it` helps to simplify phrases. However, there is a risk involved in getting mixed contexts:

```gherkin
Given there is a map
And it has pins
And the first pin has "icon buttons" // [!code ++]
And it has a "routing path"
```

In second phrase, `it` is referring to the map found in first phrase. In fourth phrase, `it` is referring to that pin searched in third phrase, though it might be intended to refer to the map once again, e.g. in case that third phrase has been inserted later. Thus, using `the map` instead of `it` is more reliable and beneficial for long-term maintenance of tests.

#### them / they

These variants are plural counterparts to `it`. They refer to the most recently processed set of elements that has a [plural cardinality](../glossary.md#cardinality).

:::info Example
```Gherkin{2}
Given there are at least three buttons
And they are enabled
```
:::

Just like `it` before, `them` and `they` may cause issues when used in unintentionally mixed contexts.

#### the latter / the former

These terms can be used to address the most recently processed set of elements in case of `the latter` or its immediate predecessor in case of `the former`.

:::info Example
```Gherkin{3,4}
Given there is a button
And there are four icons
And the former is enabled
And the latter are visible
```
:::

Neither term implies any [cardinality](../glossary.md#cardinality), so they apply to the history of searches without any additional constraints. That's why using them may cause confusion in mixed contexts just like `it` or `them` and `they` before.

#### the latter X / the former X

These variants combine the generic `the latter` and `the former` with a word or string describing a type of element. They basically work like those generic ones, but based on the words plural or singular form an according [cardinality](../glossary.md#cardinality) is assumed and thus considered on looking up history.

:::info Example
````gherkin{4,5}
Given there is a button with label "Send"
And there is a button with label "Cancel"
And there are "thumbnail icons"
When I click the former button
Then the latter button is disabled 
````

The inserted rule addressing some thumbnail icons is ignored due to focusing on buttons.  
:::

#### this X / that X

These variants work similar to `the latter X` and `the former X`. However, here a singular [cardinality](../glossary.md#cardinality) is expressed explicitly.

:::info Example
```gherkin{5}
Given there is a button with label "Send"
And there is a button with label "Cancel"
And there are "thumbnail icons"
When I click on that button
Then this button is disabled 
```

The inserted rule addressing some thumbnail icons is ignored due to focusing on buttons.  
:::

:::warning Ambivalence   
When combining either term with a plural form of a word or string, a warning is logged and no [cardinality](../glossary.md#cardinality) is assumed on looking up the history.
:::

#### these X / those X

These variants are the plural counterparts to `this X` and `that X`. Similar to those, they enforce a plural [cardinality](../glossary.md#cardinality).

:::info Example
```gherkin{2}
Given there are buttons with label "Send"
And these buttons are enabled
And there is a button with label "Cancel"
And there are "thumbnail icons"
When I click on that button
Then this button is disabled 
```
:::

:::warning Ambivalence   
When combining either term with a singular form of a word or string, a warning is logged and no [cardinality](../glossary.md#cardinality) is assumed on looking up the history.
:::

#### the X

This is a variant for `this X` when using a singular name as `X` or a variant for `these X` when using a plural name.

:::info Example
```gherkin{3,5}
Given there are three buttons
And there is a label reading "ready"
And the buttons are enabled
When I click on the first button
Then the label reads "done"
```
:::
	
### Refining contextual words

Now, let's look into that other section of contextual word phrases: the refining ones.

A refining phrase is referring to a set of elements processed in a previous Gherkin phrase, but requests to work on a subset of those elements, only. As of now, this refinement is capable of addressing a single element per referenced set of elements, only.

A refining phrase has two different [cardinalities](../glossary.md#cardinality): its _source cardinality_ is `plural` as a refining phrase refers to some set of elements processed before and its _target cardinality_ is `singular` as the phrase requests to focus on a single element for tracking it in history after processing it individually.

#### the nth X

This simpler pattern of a refining contextual word is combining an ordinal number expression with a string or word referring to a most recently processed type of elements.

The ordinal part always starts with the literal keyword `the` either followed by a sequence of digits and a final period or by one of the following keywords:

`first`, `second`, `third`, `fourth`, `fifth`, `sixth`, `seventh`, `eighth`, `ninth`, `tenth`, `1st`, `2nd`, `3rd`, `4th`, `5th`, `6th`, `7th`, `8th`, `9th`, `10th`, `last`, `former`, `latter`

:::info Example
```gherkin
Given there are three buttons
And the first button is enabled
And the 3. button is disabled
And the second button is enabled
When I click on it
Then the last button is enabled
And the 1st button is disabled
```

This example includes an illustration of how to use the different cardinalities of refining contextual words. In 5th phrase the `it` is referring to the second button selected in 4th phrase.
:::

##### Extended language pattern support <Badge type="tip" text="&gt;=1.1.0"/>

In example given before, the expressions in steps 2, 3 and 4 refer to the set of buttons looked up in first line which is having a [non-singular cardinality](../glossary.md#cardinality). 

Starting with version 1.1.0, support for a different natural language pattern is available:

```gherkin
Given there is a cell reading "Cell 1.1 of panel #2"
And there is a cell reading "Cell 1.1 of panel #3"
And there is a cell reading "Cell 1.1 of panel #1"

Then the second cell reads "Cell 1.1 of panel #3"
Then the third cell is reading "Cell 1.1 of panel #1"
Then the first cell reads "Cell 1.1 of panel #2"
```

Here, steps 1, 2 and 3 are addressing a single element each. But all three singular lookups are considered a single plural lookup when it comes to processing steps 4, 5 and 6. 

This particular pattern works _unless you mix singular-cardinality queries with non-singular queries like here:_

```gherkin
Given there is a cell reading "Cell 1.1 of panel #2"
And there is a cell reading "Cell 1.1 of panel #3" // [!code --]
And there is at least one cell reading "Cell 1.1 of panel #3" // [!code ++]
And there is a cell reading "Cell 1.1 of panel #1"

# This time these steps are failing for the ones above 
# aren't all addressing single "cell" elements.
Then the second cell reads "Cell 1.1 of panel #3"
Then the third cell is reading "Cell 1.1 of panel #1"
Then the first cell reads "Cell 1.1 of panel #2"
```


#### the nth of Y

This is a slightly more complex version enabling refining contextual words in context of two different sets of items with the same type. Like before, there is an ordinal expression extended with the literal keyword `of` followed by a non-refining contextual word with [non-singular cardinality](../glossary.md#cardinality). 

Supported suffixes are

* [them](#them-they)
* [the latter X / the former X](#the-latter-x-the-former-x) with plural cardinality
* [these X / those X](#these-x-those-x)

:::warning Limitations  
In ordinal part, the keywords `former` and `latter` are not supported to prevent ambiguous phrases.  
:::

:::info Examples  
```gherkin
Given there are three buttons with partial label "Increase"
Given there are three buttons with partial label "Decrease"
And the first of the former buttons is enabled
And the 1st of these buttons is disabled
When I click on the 1st of those buttons
Then the 1st of the latter buttons is enabled
And the first of those button is disabled
```

Phrases 3, 5 and 7 each refer to one of the buttons found in first step whereas phrases 4 and 6 refer to one of the buttons found in second step.

Of course, mixing the styles of phrasing is confusing and should be prevented. It's used here for demonstration, only. Sticking to certain pattern helps a bit with understanding such complex scenarios:

```gherkin
Given there are three buttons with partial label "Increase"
Given there are three buttons with partial label "Decrease"
And the first of the former buttons is enabled
And the first of the latter buttons is disabled
When I click on the first of the former buttons
Then the first of the latter buttons is enabled
And the first of the former button is disabled
```
:::

:::: details For developers
When using `{contextual-word}` in a step definition, the resulting argument is an object providing:

* the `word` or string that is selecting a type of elements to be looked up in history,
* the desired record's `index` into history considering tracks matching given type of elements and source cardinality, only,
* the optional `subIndex` of a single item in desired record to focus on as part of a refining contextual word,
* the boolean information whether the contextual word `isRefining` or not,
* the `sourceCardinality` for looking up records in history and
* the `targetCardinality` of a refining contextual word for creating another record in history after successfully processing selected item.

:::info Example
The phrase

* **the 5th of those buttons** is enabled

would result in an object similar to this one:

```javascript
{
	word: "button", // always in singular form
	index: 1,       // addressing second-to-last record in history on a set of buttons
	subIndex: 4,    // zero-based variant of "5th"
	isRefining: true,
	sourceCardinality: "plural",
	targetCardinality: "singular",
}
```
:::
::::
