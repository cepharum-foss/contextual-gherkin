---
outline: [2,3,4]
---

# Selectors

## Introduction

**contextual-gherkin** offers a collection of step definitions. Every step definition is associated with a phrasing pattern that you as a test author can use in your Gherkin-based test descriptions to trigger either defined step. As patterns, they all include [Cucumber expressions](expressions.md) that match variable parts of phrases actually used in your tests. Cucumber expressions match, extract and represent those variable parts in a computable way.

:::info Example  
Supported phrasing patterns are given in our [phrases reference](phrases.md). They look like this:

```
C has/have X with label Y
```

In this pattern, `C`, `X` and `Y` indicate positions of [Cucumber expressions](expressions.md) that cover different terms found in an actual phrase of your test, e.g.

```gherkin
Given this toolbar has a button with label "Clear all"
```

`C` is representing `this toolbar`, `X` is representing `a button` and `Y` is representing `"Clear all"` (implicitly reduced to `Clear All`, though).
:::

All those step definitions associated with phrasing patterns fail to instantly connect terms matched by Cucumber expressions with elements of your web application or website. 

:::info Example  
Consider this Gherkin snippet for illustration:

```gherkin
Given there are three buttons
And there is a "status indicator"
When I click on the second button
Then the indicator is missing
```

Here, the variable parts are `three buttons`, `a "status indicator"`, `the second button` as well as `the indicator`. 

**contextual-gherkin** knows how to pick e.g. the second out of three buttons, but it doesn't know how to identify those three buttons in the first place. It does not know what the status indicator is. It has no idea that the term "indicator" in fourth line is referring to the same element (see [aliases](configuration.md#aliases)).  
:::

A _selectors map_ associates the singular form of names used in such terms with CSS selectors or selector functions to eventually find those elements of your web application that you intend to address with those names. 

This map **must be** provided as part of [runtime configuration](configuration.md#selectors).

## Regular selectors

A regular selector is selecting elements associated with a particular name used in a Cucumber expression's term. If your tests talk about _panels_, a regular selector is associating the name _panel_ with elements matched by that selector.

Regular selectors are either CSS selectors picking elements in the same way as CSS or callback functions invoked in browser runtime per DOM node to decide if either node is matching the selector or not.

```
"panel": "#view > .panel"
```

This regular selector is defining, that direct child elements with class `panel` of an element with ID `view` are considered _panels_.

```javascript
"panel": node => node?.querySelectorAll( ".sections" )?.length > 1
```

This example uses a selector callback matching any provided node that has at least two subordinated elements with class `panelSection`. For a single selection of elements, this callback may be invoked several times, once for every DOM node that's part of a context. Such a context is the whole document or the set of matching elements of a previous selection.

:::warning Callback arguments  
Usually, there is a second argument provided in such a callback. This is some index into the list of nodes provided successively. However, since nodes include text nodes, comments, CDATA section etc. relying on that index might be misleading and adding a linebreak in the wrong spot of your web application can break such tests quite easily. 

Thus, you should inspect the node, only.  
:::

## Virtual selectors

Virtual selectors are supported to customize existing step definitions on inspecting parts and properties of some identified elements of your web application. They are associated with particular names that start with a special character. Following that special character, there is a name that's related to the part or property of a matching element the step definition intends to inspect in detail.

As another major difference to regular selectors, using a virtual selector does not change the focus of what is being tested. For example, a button may be a complex component consisting of a label and a tooltip both found in its HTML code. When checking the label of a button, you can either narrow down your search to address the label part of that button or use a virtual selector on the button to look into that label part without loosing track of the button as a whole.

For example, the following scenario is narrowing down its search to the button's part representing its label. The marked line is addressing the button's label, only:

::: code-group

```gherkin{4} [test.feature]
Given there is a button
And it has a label
Then the label reads "Foo!"
When I click on it
Then something happens
```

```js [setup.js]
ContextualGherkin( testController, {
	selectors: {
		button: {
			"": "my-button",
			label: ".my-button__label"
		}
	}
} )
```

:::

In opposition to that, the following example is finding the same button, but the marked line keeps referring to the button as a whole:

::: code-group

```gherkin{2} [test.feature]
Given there is a button labelled with "Foo!"
When I click on it
Then something happens
```

```js [setup.js]
ContextualGherkin( testController, {
	selectors: {
		button: {
			"": "my-button",
			$label: ".my-button__label"
		}
	}
} )
```

:::


:::info Optional  
All virtual selectors have defaults, thus customizing them is optional.  
:::

### Part selectors

Part selectors descend into an inspected element's structure. They are e.g. used to pick the part of an element that's representing its label or its textual content on screen for comparing it as part of a constraint for either element to match. 

A dollar sign is used as special character to indicate a part selector. 

Currently supported part selectors are:

#### $click

This virtual selector is used to address the element to click on when using according action steps like [`I click on C`](phrases.md#i-click-on-c). When testing complex web components, using this virtual selector might be necessary to make sure the click is performed in bounds of an intended trigger zone.

It is either

* `false` to select the inspected element itself (which is the default),
* a CSS selector describing an embedded element or
* a callback function invoked on the inspected element and all its descendant nodes.

A provided callback function is invoked on the inspected element and all its descendant nodes to return `true` on the matching node. Steps supporting this virtual selector require it to address exactly one matching node.

#### $enter

This virtual selector is used to address the element to type text into when using according action steps like [`I enter T into C`](phrases.md#i-enter-t-into-c). When testing complex web components, using this virtual selector might be necessary so the text is properly entered into the input control which is embedded in that component.

It is either

* `false` to select the inspected element itself (which is the default),
* a CSS selector describing an embedded element or
* a callback function invoked on the inspected element and all its descendant nodes.

A provided callback function is invoked on the inspected element and all its descendant nodes to return `true` on the matching node. Steps supporting this virtual selector require it to address exactly one matching node.

#### $hover

This virtual selector is used to address the element to actually hover when using according action steps like [`I hover over C`](phrases.md#i-hover-over-c). When testing complex web components, using this virtual selector might be necessary to make sure the pointer is properly hovering bounds of an intended trigger zone.

It is either

* `false` to select the inspected element itself (which is the default),
* a CSS selector describing an embedded element or
* a callback function invoked on the inspected element and all its descendant nodes.

A provided callback function is invoked on the inspected element and all its descendant nodes to return `true` on the matching node. Steps supporting this virtual selector require it to address exactly one matching node.

#### $checked <Badge type="tip" text="since v1.2.3"/>

When searching elements that are either checked or not, this virtual part selector may choose one or more related elements as inspection replacements. If an element chosen as replacement here is considered checked, the original element is considered checked, too. In this case, the search is collecting that replaced element.

This virtual part selector is either

* `false` to select the inspected element itself (default),
* a CSS selector describing one or more embedded elements or
* a callback function mapping a given element into one or more elements to inspect.

Callback functions are invoked on every element to be inspected originally. It is expected to return one or more elements as replacements. Similarly, a CSS selector is used in context of every element to be inspected originally for finding descendants as its replacements.

:::info Example  
Consider a phrase like

```gherkin
When I click on the checked item
```

with `item` addressing some complex component like this one:

```html
<div class="item">
	<label>item's label</label>
	<input type="checkbox" class="toggle">
	<button class="action">Trigger</button>
</div>
```

In this case, the information of being checked or not is not a property of the wrapping `div` element but found on one of its constituents, the `input`. The `$checked` selector for the `item` could be 

```javascript
"$checked": "input[type=checkbox]"
```

or

```javascript
"$checked": node => node.querySelector( "input[type=checkbox]" )
```

Like any virtual part selector, `$checked` is not affecting the element considered as checked. It's still the wrapping `div` whenever the addressed `input` is checked.
:::

:::tip Processing order  
This virtual part selector is processed prior to some optional [property selector](#property-selectors) `#checked`. Any property selected by `#checked` is eventually looked up on elements addressed by `$checked`.
:::

#### $classList <Badge type="tip" text="since v1.2.3"/>

This virtual part selector is considered when searching elements that are required to (not) have a certain tag or mark. 

It mostly works like `$checked`, thus see the [description there](#checked) for additional information. The related [property selector](#property-selectors) is `#classList`.

#### $disabled <Badge type="tip" text="since v1.2.3"/>

This virtual part selector is considered when searching elements that are either disabled or enabled (a.k.a. not disabled). 

It mostly works like `$checked`, thus see the [description there](#checked) for additional information. The related [property selector](#property-selectors) is `#disabled`.

#### $id <Badge type="tip" text="since v1.2.3"/>

This virtual part selector is considered when searching elements with a particular ID.

It mostly works like `$checked`, thus see the [description there](#checked) for additional information. The related [attribute selector](#attribute-selectors) is `@id`.

#### $label

This virtual part selector is addressing an element's label. It's used by step definitions checking the label of match candidates such as [`C has/have X with label Y`](phrases.md#c-has-have-x-with-label-y).

It is either 

* `false` to select the inspected element itself,
* a CSS selector describing an embedded element or
* a callback function invoked on match candidate. 

In opposition to regular selectors, the callback function 

* is invoked once per element matching name-based selector instead of every DOM node that's subordinated to it and
* is meant to return the DOM node actually providing the label to inspect as its textual content or node value instead of deciding whether provided node is matching or not. 

The default is `"label"` which is searching for a `<label>` element subordinated to the inspected element.

:::info Example  
Imagine a phrase like

```gherkin
Given there is an input with label "Your name:"
```

is basically matching an `<input>` element. Given that, the related label isn't part of that `<input>` element, thus a callback can be used to identify it properly:

```javascript
"$label": node => node?.parentNode?.querySelector( ".label" )
```
:::

#### $name <Badge type="tip" text="since v1.2.3"/>

This virtual part selector is considered when searching elements with a particular name.

It mostly works like `$checked`, thus see the [description there](#checked) for additional information. The related [attribute selector](#attribute-selectors) is `@name`.

#### $selected <Badge type="tip" text="since v1.2.3"/>

This virtual part selector is considered when searching elements that are selected or not.

It mostly works like `$checked`, thus see the [description there](#checked) for additional information. The related [attribute selector](#attribute-selectors) is `@selected`.

#### $text

This virtual part selector is addressing an element's textual content.

It is either 

* `false` to select the inspected element itself, 
* a CSS selector describing an embedded element or 
* a callback function. 
 
In opposition to regular selectors, the callback function is meant to return the DOM node with the textual content or node value to inspect.

The default is boolean `false` which is addressing the originally matching element itself.

:::info Example  
Consider a phrase like

```gherkin
Given there is an "editor card" with text "related topics"
```

which is matching a custom web component with all its content wrapped in a shadow DOM. 

In this case, the related textual content to compare with the given string isn't associated with that web component, thus a callback is required to fetch the text container properly:

```javascript
"$text": node => node?.shadowRoot?.querySelector( ".label" )
```
:::

#### $value <Badge type="tip" text="since v1.2.3"/>

This virtual part selector is considered when searching elements with a particular value.

It mostly works like `$checked`, thus see the [description there](#checked) for additional information. The related [property selector](#property-selectors) is `#value`.


### Attribute selectors

Attribute selectors can be used to redirect inspections for a named attribute to actually read some other attribute's value of same element instead.

They consist of `@` and the originally addressed attribute's name. 

In opposition to regular selector or virtual part selectors, attribute selectors are limited to mapping to the name of a different attribute, only.

The default is the original attribute name itself.

:::info Example
Step definitions testing elements' IDs and names are inspecting attributes `id` and `name` accordingly. Definitions testing elements being selected or not inspect boolean value of attribute `selected`.

Attribute selectors redirecting those inspections to other attributes could look like this:

```javascript
"@id": "data-uuid",
"@name": "for",
"@selected": "picked",
```
:::

### Property selectors

Property selectors are very similar to attribute selectors. They can be used to redirect inspections for a particular DOM property of a matching element to actually read another DOM property of that same element instead.

They consist of `#` and the name of originally addressed DOM property.

In opposition to regular selectors or virtual part selectors, property selectors are limited to mapping to the name of a different property, only.

The default is the name of the original DOM property itself.

:::info Example
Step definitions can test elements' values or whether they are enabled or checked. Those steps inspect DOM properties `value`, `disabled` or `checked` accordingly. 

In addition, step definitions testing elements for being _tagged_ or _marked_ actually inspect their list of classes using special property `classList` per element.

Property selectors redirecting those inspections to other DOM properties could look like this:

```javascript
"#value": "textContent",
"#disabled": "editorIsDisabled",
"#checked": "deletionRequested",
"#classList": "tags",
```
:::

In case of `#classList`, the resulting property must provide an API similar to the one provided by DOM elements' property [`classList`](https://developer.mozilla.org/en-US/docs/Web/API/Element/classList), which is used by default.

## Selector hierarchies

The phrase "a button" can describe very different things in different contexts. Selector hierarchies are supported by **contextual-gherkin** to deal with such cases.

Let's start with an example:

```gherkin
Given there is a menu
And it has 5 items
When I click on the first item
Then there is an overview
And it has 20 items
```

The rather generic term `item` is used in different situations to probably address different kinds of elements. Let's assume that the menu is just a list of `<a>` elements, while the overview consists of `<li>` elements instead. For the menu, the selector would be

```javascript
item: "a"
```

and for the overview the selector would be

```javascript
item: "li"
```

But how do you support both cases in one [configuration](configuration.md#selectors) properly? This does not work:

```javascript
ContextualGherkin( testController, {
	selectors: {
		menu: "#menu",
		overview: ".overview",
		item: "a", // [!code focus:2]
		item: "li",
	},
} );
```

You could use a combining CSS selector:

```javascript
ContextualGherkin( testController, {
	selectors: {
		menu: "#menu",
		overview: ".overview",
		item: "a, li", // [!code focus]
	},
} );
```

However, this could unintentionally match more elements than expected.

You could use a complex callback function that's testing the parent of any item, too:

```javascript
ContextualGherkin( testController, {
	selectors: {
		menu: "#menu",
		overview: ".overview",
		item: node => node?.closest( "#menu, .overview" )?.id === "menu" ? node?.nodeName === "A" : node?.nodeName === "LI" // [!code focus]
	},
} );
```

But let's talk about this in a few months when you have rearranged your application, something is broken and you haven't touched tests for a while. You will struggle to remember why this selector has been made this way.

As a more convenient solution, **contextual-gherkin** supports a whole hierarchy of selectors:

```javascript
ContextualGherkin( testController, {
	selectors: {
		menu: { // [!code focus:8]
			"": "#menu",
			item: "a",
		},
		overview: {
			"": ".overview",
			item: "li",
		},
	},
} );
```

Any simple selector - those mapping a name into a CSS selector, a callback function or `false` - can be replaced with an object of subordinated selectors with the originally provided one now given for the special name `""`. Next to it, you can add selectors for specific use in context of elements that have matched that outer selector before.

This nesting can be repeated as many times as desired. You can use all kinds of selectors in any level of such a hierarchy, but starting another level is limited to [regular selectors](#regular-selectors):

```javascript
ContextualGherkin( testController, {
	selectors: {
		menu: { // [!code focus:12]
			"": "#menu",
			item: {
				"": "a",
				"#disabled": "isDeadLink"
			},
		},
		overview: {
			"": ".overview",
			item: "li",
			$label: "h2",
		},
	},
} );
```

### Fallback strategy

When picking a selector matching a given name:

1. the current context's set of selectors is inspected first,
2. the level of current context's parent context is inspected as fallback,
3. this second step is repeated as long as there is a parent context but no matching selector and
4. eventually the top level given in configuration is inspected prior to using some default optionally implemented in **contextual-gherkin**.

:::warning Possible confusion  
This fallback strategy is focusing on a hierarchy of [contexts](../glossary.md#contextuality). It may significantly deviate from the hierarchy of selectors found in configuration.

Currently, preventing any selector fallbacks is the only option to prevent this deviation and any confusion resulting from it. However, this leads to a much more complex configuration of selectors that could be hard to maintain, too.
:::

```javascript
ContextualGherkin( testController, {
	selectors: {
		menu: { // [!code focus:10]
			"": "#menu",
			item: "a",
		},
		overview: {
			"": ".overview",
			$label: ".label",
		},
		$label: false,
		item: "li",
	},
} );
```

This time, there is a global fallback for searching items which is used unless items are searched in context of a `menu`. 

In addition, [part selector](#part-selectors) `$label` is customized in different ways:

* An element is its own label by default, now, because of `$label: false` at top level.
* An `overview` contains another element with class `label`, instead.
* Items of an `overview` use the same approach. 

  They are found using default selector, but still inherit the [context](../glossary.md#contextuality) of `overview` in which they have been searched. That's one of the deviations mentioned in warning box above.
* For the menu or its items, no such selector has been given specifically. Thus, the default customized at top level applies.

## Callback function constraints

As described before, selectors can be callback functions. However, some limitations apply mostly because those functions run in a different process and in a different runtime environment which can be as remote as a browser on a device connected via network.

Because of that, all callback functions should be [pure](https://en.wikipedia.org/wiki/Pure_function).  They can not use any closure scope or `this` by default. This example does not work:

```javascript
const lookup = "foo";

ContextualGherkin( testController, {
	selectors: {
		marker: node => node?.textContent ==== lookup, // throws exception !!
	},
} );
```

The variable `lookup` is defined in the test runner's process. But the callback will be invoked in the browser's process. Thus referring to `lookup` does not work as usual.

**contextual-gherkin** is providing a way of explicitly forwarding variables to the browser process so that callback functions can use them as expected:

```javascript
const lookup = "foo";

ContextualGherkin( testController, {
	selectors: {
		marker: [
			node => node?.textContent ==== lookup,
			{ lookup }
		]
	},
} );
```

Instead of providing a function directly, you provide an array consisting of two items which are that function and an object of named values to pass as custom closure scope. 

With this approach, you can e.g. reuse functions more easily:

```javascript
const tester = node => node?.nodeName?.match( lookup );  

ContextualGherkin( testController, {
	selectors: {
		station: [ tester, { lookup: /-station$/i } ],
		terminal: [ tester, { lookup: /-terminal$/i } ],
	},
} );
```
