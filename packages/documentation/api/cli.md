---
outline: [2,3]
---
# Command line tools

## contextual-gherkin

When installing this library, **contextual-gherkin**, as a dependency of your project, a new CLI tool is available named **contextual-gherkin**, too. You can invoke it using **npx** which is a tool of **npm** for executing tools of dependencies.

```bash
npx contextual-gherkin
```

This tool is a convenience helper for integrating the library with your project. It basically serves two purposes:

* Make sure all dependencies have been installed and are up-to-date.
* Run Gherkin-based tests based on **contextual-gherkin** in a browser via TestCafe.

On every invocation, dependencies are checked. Thus, when invoked without any arguments, the tool either fails due to lack of proper dependencies or due to lacking information on test files to run.

In the former case, a prompt for running a certain **npm** command is displayed:

```
@cepharum/contextual-gherkin 0.1.0-alpha.12
run

    npm i -D @cucumber/cucumber@^8.10.0 @cucumber/cucumber-expressions@^16.1.2 gherkin-testcafe@^5.6.0 testcafe@~1.20.0
```

:::warning  
Don't use this particular output given here to set up **contextual-gherkin**, but run the tool as described yourself.  
:::

If dependencies are fine, an error like this one is displayed when invoking without any arguments:

```
@cepharum/contextual-gherkin 0.1.0-alpha.12
provide patterns selecting your test files
```

### Run tests

Running tests requires some actual files in your local project that describe test cases and set up runtime configuration of **contextual-gherkin**. Names of all those files have to be provided as arguments to this tool. Though, based on the assumption of having many files to provide, the tool is expecting you to use patterns for selecting multiple files at once:

```bash
npx contextual-gherkin tests/e2e/features/**/*.feature tests/e2e/steps/**/*.js
```

In this example, two separate patterns are provided. The first is selecting all files with extension **.feature** anywhere in folder **tests/e2e/features** of your project. The second pattern is selecting all files with extension **.js** anywhere in folder **tests/e2e/steps** of your project.

You can basically provide as many patterns as required to select all the files that have to be considered. All selected files with extension **.feature** are parsed for test case descriptions. Unless additional constraints have been provided, all parsed test cases are run afterwards. The files with extension **.js** are parsed and executed. They are expected to contain custom step definitions and hooks for configuring **contextual-gherkin**.

:::tip Typescript support  
Files with extension **.ts** can be used, too. They are transpiled from TypeScript to JavaScript automatically.
:::

### Supported options

**contextual-gherkin** supports a few command line options for customizing its behavior. To safely distinguish those from [options to be passed](#passed-options), `--` can be used as usual.

#### --folder <Badge type="tip" text="since v1.1.0"/>

The example given above for selecting features and step definitions in separate subfolders **features** and **steps** of common folder **tests/e2e** can be simplified using the option `--folder` like this:

```bash
npx contextual-gherkin --folder=tests/e2e
```

It is mapped into the same patterns as given before. In addition, a similar pattern for matching any **.ts** file in **steps** folder is added, too. Thus, using `--folder` is eventually identical to this invocation:

```bash
npx contextual-gherkin tests/e2e/features/**/*.feature \
                       tests/e2e/steps/**/*.js \
                       tests/e2e/steps/**/*.ts
```

You can combine `--folder` with custom patterns you like to provide nonetheless:

```bash
npx contextual-gherkin --folder=tests/e2e tests/lib/**/*.js
```

If your files don't reside in separate subfolders **features** and **steps**, you can customize them with additional options `--features` and `--steps`. They get appended to the folder given in `--folder` and default to `features` and `steps`. 

Drop those defaults by assigning empty strings to have all files in a common base folder:

```bash
npx contextual-gherkin --folder=tests/e2e --features= --steps=
```

Finally, you can use those two options _instead_ of `--folder`, too. In this case, either option is selecting a common base folder for containing feature files or step definition files:

```bash
npx contextual-gherkin --features=tests/e2e --steps=tests/lib
```

#### -E / --env <Badge type="tip" text="since v1.2.3"/>

Environment variables may affect the way **contextual-gherkin** is invoking **gherkin-testcafe**. In addition, custom implementations of Gherkin steps may rely on environment variables for customized behavior in different runtime environments. This option provides an easy way to add or replace environment variables on the fly. Due to its integration with **contextual-gherkin** the definition of environment variables works cross-platform without any additional tool as dependency.

```bash
npx contextual-gherkin --folder tests/e2e -E SERVER=http://127.0.0.1:4200 --env E2E_BROWSER=edge
```

Environment variables defined on command line have precedence over variables defined in an [env-file](#env-file). Environment variables defined on command line are not interpolated, but used as given.

#### --env-file <Badge type="tip" text="since v1.2.3"/>

When defining sets of environment variables for customizing a test run, using the CLI may become tedious. Option `--env-file` supports the selection of a file to read definitions of environment variables from. By default, file **.env** in current folder is read unless missing.

The file is expected to be a regular text file. Empty lines and lines starting with `#` or `;` are ignored. All other lines are expected to contain an assignment in one of these forms:

```dotenv
name=value
name="value"
name='value'
```

Any whitespace is ignored

- before and after the name, 
- before and after the assignment operator,
- before and after the unquoted value,
- before the opening quotes and after the closing quotes

A matching pair of quotes surrounding the value is removed. 

Values enclosed by single quotes are used as given. Other values are subject to interpolation: occurrences like `$NAME_OF_VALUE` or `${NAME_OF_VALUE}` are replaced with value of named environment variable. Interpolation has access to variables 

- in pre-existing environment of **contextual-gherkin** process,
- [defined on command line](#e-env),
- that have been read from file already.

:::warning Note on precedence  
Even after reading the file, **contextual-gherkin** re-processes variables defined on command line to make sure they have precedence over variables read from file here.

Because of that, in favor of more flexible definitions of contexts, variables in the file may address variables defined on command line for interpolation, but they can't replace them.
:::

### Passed options

Any argument either not recognized by **contextual-gherkin** or following first occurrence of `--` is collected and forwarded to [gherkin-testcafe](https://www.npmjs.com/package/gherkin-testcafe), which is a 3rd party dependency of **contextual-gherkin** and used to invoke [TestCafé](https://testcafe.io/documentation/402639/reference/command-line-interface) eventually. 

Use any command line switch supported by **gherkin-testcafe** or **Testcafé** e.g. to limit the tests actually run by choosing tags to match.

```bash
npx contextual-gherkin --folder=tests/e2e --tags @PRIO_1
```

### Browser selection

The tool is reading environment variable `E2E_BROWSER` for the selection of one or more browsers to use on running the tests. See [TestCafé documentation](https://testcafe.io/documentation/402639/reference/command-line-interface#browser-list) for a list of supported browser aliases.

When omitted, `chrome` is used on Windows and `chromium:headless` is used in any other context by default. This is intentional to simplify integration of **contextual-gherkin** with CI-based test runners.

:::tip Include browser arguments here
The name of a browser may include browser-specific command line arguments TestCafé is considering when invoking the browser.  

```bash
npx contextual-gherkin --folder=tests/e2e \
         -E E2E_BROWSER="chrome --no-default-browser-check"
```
:::

### Environment variables

Some additional features can be controlled by environment variables.

#### ACTION_WAIT

Steps of **contextual-gherkin** acting on elements of tested web application implicitly wait about 100ms after performing either action. This variable can be used to provide a different wait time in milliseconds. Set it to `0` to disable any post-action delay.

```bash
npx contextual-gherkin --folder=tests/e2e -E ACTION_WAIT=200
```

#### E2E_BROWSER

This variable is checked for selecting a browser used to run the tests in.

[See above](#browser-selection) for additional information.

#### STEP_BY_STEP

Setting this environment variable to a non-empty string causes every step of **contextual-gherkin** to instantly trigger the TestCafé debugger pausing execution. This improves step-by-step processing of Gherkin-based scenarios in opposition to the more detailed step-by-step processing on an action level as provided by TestCafé itself. This is due to **contextual-gherkin** step implementations often consisting of multiple TestCafé actions.

When set, every Gherkin step implemented by **contextual-gherkin** will unconditionally pause execution using another `debug` action. TestCafè's "Resume" button can be used to execute the step as a whole. Its "Next action" button is still available to continue execution until reaching the next TestCafé action in code.

```bash
npx contextual-gherkin --folder=tests/e2e --env STEP_BY_STEP=1
```
