---
outline: [2,3]
---
# Phrases Reference

All steps provided by **contextual-gherkin** can be divided into five sections:

* [global queries](#global-queries) look for elements somewhere on the current page
* [global actions](#global-actions) perform actions on elements found somewhere on the current page
* [contextual queries](#contextual-queries) look for elements in context of other elements
* [contextual actions](#contextual-actions) perform actions on previously found elements
* [contextual inspections](#contextual-inspections) assert current state of previously found elements

:::warning Have you heard of our custom Cucumber expressions?  
This page is listing patterns for phrases supported by **contextual-gherkin** in a concise way. Understanding provided information depends on understanding the [custom Cucumber expressions](expressions.md) additionally provided by **contextual-gherkin**.
:::

All phrases listed below are given without any Gherkin-specific keyword. Due to the behavior of underlying test runner every listed phrase can be combined with every supported Gherkin keyword. But for the sake of readable tests, you should use 

* queries with `Given` or `Then`,
* actions with `When` and
* inspections with `Given` or `Then`.

In this, `Then` should be used for all queries or inspections but the first one in a homogenic sequence of either, only.

:::info Example  
The global query

```
there is/are X
```

should be used with `Given` or `Then` like this:

```gherkin
Given there is a button
```
:::

:::tip Missing a phrase?  
If you are missing a sufficiently generic phrase here, see [our list of pending issues](https://gitlab.com/cepharum-foss/contextual-gherkin/-/issues) for possible feature requests to add them or create a feature request yourself there.
:::

## Global queries

### there is/are X

Searches unconditionally one or more elements of same type somewhere on the current page.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Aliases** are `there is a list of X` and `there is a set of X`.

### there is/are X with ID Y

Searches one or more elements of same type somewhere on the current page that share a given ID.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the desired ID as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `there is/are X with id Y`.
* Inspected element for having desired ID can be replaced using [virtual part selector `$id`](./selectors.md#id). The Attribute is selected via [virtual selector](./selectors.md#attribute-selectors) `@id` using `"id"` by default.

### there is/are X with name Y

Searches one or more elements of same type somewhere on the current page that share a given value for their `name` attribute.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the desired name as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `there is/are X named Y`.
* Inspected element for having desired name can be replaced using [virtual part selector `$name`](./selectors.md#name). The Attribute is selected via [virtual selector](./selectors.md#attribute-selectors) `@name` using `"name"` by default.

### there is/are X with value Y

Searches one or more elements of same type somewhere on the current page that share a given value looked up in either element's DOM property `value`.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the desired value as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** don't exist.
* Inspected element for having desired value can be replaced using [virtual part selector `$value`](./selectors.md#value). Property is selected via [virtual selector](./selectors.md#property-selectors) `#value` using `"value"` by default.

### there is/are X with label Y

Searches one or more elements of same type somewhere on the current page that have a [normalized](../glossary.md#normalized-string) label with given text.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the label's text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `there is/are X labelled with Y`.
* Either element's label is selected via [virtual selector](./selectors.md#label) `$label` looking for an embedded `<label>` element by default.

### there is/are X with partial label Y

Searches one or more elements of same type somewhere on the current page that have a [normalized](../glossary.md#normalized-string) label _containing_ given text at least. 

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the label's text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `there is/are X partially labelled with Y`.
* Either element's label is selected via [virtual selector](./selectors.md#label) `$label` looking for an embedded `<label>` element by default.

### there is/are X with text Y

Searches one or more elements of same type somewhere on the current page that have a [normalized](../glossary.md#normalized-string) textual content matching given text.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the element's expected text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `there is/are X reading Y`.
* Either element's textual content is selected via [virtual selector](./selectors.md#text) `$text` using the element itself by default.

### there is/are X with partial text Y

Searches one or more elements of same type somewhere on the current page that have a [normalized](../glossary.md#normalized-string) textual content _containing_ given text at least. 

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the element's expected text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `there is/are X partially reading Y`.
* Either element's textual content is selected via [virtual selector](./selectors.md#text) `$text` using the element itself by default.

### there is/are X which is/are enabled

Searches one or more elements of same type somewhere on the current page that are enabled.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Aliases** don't exist.
* An element is enabled if its boolean DOM property `disabled` is falsy. [Virtual part selector `$disabled`](./selectors.md#disabled) can be used to inspect different DOM elements per candidate matching **X**. [Virtual property selector](./selectors.md#property-selectors) `#disabled` can be used to inspect a different DOM property.

### there is/are X which is/are disabled

Searches one or more elements of same type somewhere on the current page that are disabled.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Aliases** don't exist.
* An element is enabled if its boolean DOM property `disabled` is truthy. [Virtual part selector `$disabled`](./selectors.md#disabled) can be used to inspect different DOM elements per candidate matching **X**. [Virtual property selector](./selectors.md#property-selectors) `#disabled` can be used to inspect a different DOM property.

### there is/are X which is/are checked

Searches one or more elements of same type somewhere on the current page that are checked.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Aliases** don't exist.
* An element is checked if its boolean DOM property `checked` is truthy. [Virtual part selector `$checked`](./selectors.md#checked) can be used to inspect different DOM elements per candidate matching **X**. [Virtual property selector](./selectors.md#property-selectors) `#checked` can be used to inspect a different DOM property.

### there is/are X which is/are unchecked

Searches one or more elements of same type somewhere on the current page that are not checked.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Alias** is `there is/are X which is/are not checked`.
* An element is unchecked if its boolean DOM property `checked` is falsy. [Virtual part selector `$checked`](./selectors.md#checked) can be used to inspect different DOM elements per candidate matching **X**. [Virtual property selector](./selectors.md#property-selectors) `#checked` can be used to inspect a different DOM property.

### there is/are X which is/are selected

Searches one or more elements of same type somewhere on the current page that are selected.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Aliases** don't exist.
* An element is selected if its boolean attribute `selected` has been set. [Virtual part selector `$selected`](./selectors.md#selected) can be used to inspect different DOM elements per candidate matching **X**. [Virtual selector](./selectors.md#attribute-selectors) `@selected` can be used to inspect a different boolean attribute.

### there is/are X which is/are unselected

Searches one or more elements of same type somewhere on the current page that are not selected.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Alias** is `there is/are X which is/are not selected`.
* An element is unselected if its boolean attribute `selected` has not been set. [Virtual part selector `$selected`](./selectors.md#selected) can be used to inspect different DOM elements per candidate matching **X**. [Virtual selector](./selectors.md#attribute-selectors) `@selected` can be used to inspect a different boolean attribute.

### there is/are X marked/tagged as Y

Searches one or more elements of same type somewhere on the current page that have a class as additional constraint.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is a single class name as a word (without whitespace or double quotes).
* **Aliases** don't exist.
* An element has given mark or tag if its DOM property `classList` contains named class. [Virtual part selector `$classList`](./selectors.md#classlist) can be used to inspect that DOM property on different elements. [Virtual selector](./selectors.md#property-selectors) `#classList` can be used to inspect a different DOM property of same type.

### there is/are X not marked/tagged as Y

Searches one or more elements of same type somewhere on the current page that don't have a class as additional constraint.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is a single class name as a word (without whitespace or double quotes).
* **Aliases** don't exist.
* An element doesn't have given mark or tag if its DOM property `classList` does not contain named class. [Virtual part selector `$classList`](./selectors.md#classlist) can be used to inspect that DOM property on different elements. [Virtual selector](./selectors.md#property-selectors) `#classList` can be used to inspect a different DOM property of same type.

## Global actions

### I click on X with ID Y

Clicks on a single element found somewhere on the current page that has a given ID.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the desired ID as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** are `I click X with ID Y`, `I click on X with id Y` and `I click X with id Y`.
* Inspected element for having desired ID can be replaced using [virtual part selector `$id`](./selectors.md#id). The Attribute is selected via [virtual selector](./selectors.md#attribute-selectors) `@id` using `"id"` by default.
* The eventually clicked element may be customized using [virtual selector](selectors.md#click) `$click` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I click on X with name Y

Clicks on a single element found somewhere on the current page that has a given value for its `name` attribute.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the desired name as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** are `I click X with name Y`, `I click on X named Y` and `I click X named Y`.
* Inspected element for having desired name can be replaced using [virtual part selector `$name`](./selectors.md#name). The Attribute is selected via [virtual selector](./selectors.md#attribute-selectors) `@name` using `"name"` by default.
* The eventually clicked element may be customized using [virtual selector](selectors.md#click) `$click` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I click on X with value Y

Clicks on a single element found somewhere on the current page that has a given value in its DOM property `value`.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the desired value as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `I click X with value Y`.
* Inspected element for having desired value can be replaced using [virtual part selector `$value`](./selectors.md#value). Property is selected via [virtual selector](./selectors.md#property-selectors) `#value` using `"value"` by default.
* The eventually clicked element may be customized using [virtual selector](selectors.md#click) `$click` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I click on X with label Y

Clicks on a single element found somewhere on the current page that has a [normalized](../glossary.md#normalized-string) label with given text.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the label's text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** are `I click X with label Y`, `I click on X labelled with Y` and `I click X labelled with Y`.
* A matching element's label is selected via [virtual selector](./selectors.md#label) `$label` looking for an embedded `<label>` element by default.
* The eventually clicked element may be customized using [virtual selector](selectors.md#click) `$click` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I click on X with partial label Y

Clicks on a single element found somewhere on the current page that has a [normalized](../glossary.md#normalized-string) label _containing_ given text at least.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the label's text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** are `I click X with partial label Y`, `I click on X partially labelled with Y` and `I click X partially labelled with Y`.
* A matching element's label is selected via [virtual selector](./selectors.md#label) `$label` looking for an embedded `<label>` element by default.
* The eventually clicked element may be customized using [virtual selector](selectors.md#click) `$click` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I click on X with text Y

Clicks on a single element found somewhere on the current page that has a [normalized](../glossary.md#normalized-string) textual content matching given text.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the element's expected text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** are `I click X with text Y`, `I click on X reading Y` and `I click X reading Y`.
* A matching element's textual content is selected via [virtual selector](./selectors.md#text) `$text` using the element itself by default.
* The eventually clicked element may be customized using [virtual selector](selectors.md#click) `$click` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I click on X with partial text Y

Clicks on a single element found somewhere on the current page that has a [normalized](../glossary.md#normalized-string) textual content _containing_ given text at least.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the element's expected text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** are `I click X with partial text Y`, `I click on X partially reading Y` and `I click X partially reading Y`.
* A matching element's textual content is selected via [virtual selector](./selectors.md#text) `$text` using the element itself by default.
* The eventually clicked element may be customized using [virtual selector](selectors.md#click) `$click` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I click on X marked/tagged as Y

Searches single element of named type somewhere on the current page that has a class as additional constraint and clicks it.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is a single class name as a word (without whitespace or double quotes).
* **Alias** is `I click X marked/tagged as Y`.
* An element has given mark or tag if its DOM property `classList` contains named class. [Virtual part selector `$classList`](./selectors.md#classlist) can be used to inspect that DOM property on different elements. [Virtual selector](./selectors.md#property-selectors) `#classList` can be used to inspect a different DOM property of same type.
* The eventually clicked element may be customized using [virtual selector](selectors.md#click) `$click` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I click on X not marked/tagged as Y

Searches single element of named type somewhere on the current page that doesn't have a class as additional constraint and clicks it.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is a single class name as a word (without whitespace or double quotes).
* **Alias** is `I click X not marked/tagged as Y`.
* An element doesn't have given mark or tag if its DOM property `classList` does not contain named class. [Virtual part selector `$classList`](./selectors.md#classlist) can be used to inspect that DOM property on different elements. [Virtual selector](./selectors.md#property-selectors) `#classList` can be used to inspect a different DOM property of same type.
* The eventually clicked element may be customized using [virtual selector](selectors.md#click) `$click` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I hover over X with ID Y

Moves pointer to a single element found somewhere on the current page that has a given ID.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the desired ID as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** are `I hover X with ID Y`, `I hover over X with id Y` and `I hover X with id Y`.
* Inspected element for having desired ID can be replaced using [virtual part selector `$id`](./selectors.md#id). The Attribute is selected via [virtual selector](./selectors.md#attribute-selectors) `@id` using `"id"` by default.
* The eventually hovered element may be customized using [virtual selector](selectors.md#hover) `$hover` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I hover over X with name Y

Moves pointer to a single element found somewhere on the current page that has a given value for its `name` attribute.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the desired name as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** are `I hover X with name Y`, `I hover over X named Y` and `I hover X named Y`.
* Inspected element for having desired name can be replaced using [virtual part selector `$name`](./selectors.md#name). The Attribute is selected via [virtual selector](./selectors.md#attribute-selectors) `@name` using `"name"` by default.
* The eventually hovered element may be customized using [virtual selector](selectors.md#hover) `$hover` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I hover over X with value Y

Moves pointer to a single element found somewhere on the current page that has a given value in its DOM property `value`.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the desired value as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `I hover X with value Y`.
* Inspected element for having desired value can be replaced using [virtual part selector `$value`](./selectors.md#value). Property is selected via [virtual selector](./selectors.md#property-selectors) `#value` using `"value"` by default.
* The eventually hovered element may be customized using [virtual selector](selectors.md#hover) `$hover` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I hover over X with label Y

Moves pointer to a single element found somewhere on the current page that has a [normalized](../glossary.md#normalized-string) label with given text.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the label's text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** are `I hover X with label Y`, `I hover over X labelled with Y` and `I hover X labelled with Y`.
* A matching element's label is selected via [virtual selector](./selectors.md#label) `$label` looking for an embedded `<label>` element by default.
* The eventually hovered element may be customized using [virtual selector](selectors.md#hover) `$hover` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I hover over X with partial label Y

Moves pointer to a single element found somewhere on the current page that has a [normalized](../glossary.md#normalized-string) label _containing_ given text at least.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the label's text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** are `I hover X with partial label Y`, `I hover over X partially labelled with Y` and `I hover X partially labelled with Y`.
* A matching element's label is selected via [virtual selector](./selectors.md#label) `$label` looking for an embedded `<label>` element by default.
* The eventually hovered element may be customized using [virtual selector](selectors.md#hover) `$hover` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I hover over X with text Y

Moves pointer to a single element found somewhere on the current page that has a [normalized](../glossary.md#normalized-string) textual content matching given text.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the element's expected text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** are `I hover X with text Y`, `I hover over X reading Y` and `I hover X reading Y`.
* A matching element's textual content is selected via [virtual selector](./selectors.md#text) `$text` using the element itself by default.
* The eventually hovered element may be customized using [virtual selector](selectors.md#hover) `$hover` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I hover over X with partial text Y

Moves pointer to a single element found somewhere on the current page that has a [normalized](../glossary.md#normalized-string) textual content _containing_ given text at least.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the element's expected text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** are `I hover X with partial text Y`, `I hover over X partially reading Y` and `I hover X partially reading Y`.
* A matching element's textual content is selected via [virtual selector](./selectors.md#text) `$text` using the element itself by default.
* The eventually hovered element may be customized using [virtual selector](selectors.md#hover) `$hover` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I hover over X marked/tagged as Y

Searches single element of named type somewhere on the current page that has a class as additional constraint and moves pointer there without clicking it.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is a single class name as a word (without whitespace or double quotes).
* **Alias** is `I hover X marked/tagged as Y`.
* An element has given mark or tag if its DOM property `classList` contains named class. [Virtual part selector `$classList`](./selectors.md#classlist) can be used to inspect that DOM property on different elements. [Virtual selector](./selectors.md#property-selectors) `#classList` can be used to inspect a different DOM property of same type.
* The eventually hovered element may be customized using [virtual selector](selectors.md#hover) `$hover` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I hover over X not marked/tagged as Y

Searches single element of named type somewhere on the current page that doesn't have a class as additional constraint and moves pointer there without clicking it.

* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is a single class name as a word (without whitespace or double quotes).
* **Alias** is `I hover X not marked/tagged as Y`.
* An element doesn't have given mark or tag if its DOM property `classList` does not contain named class. [Virtual part selector `$classList`](./selectors.md#classlist) can be used to inspect that DOM property on different elements. [Virtual selector](./selectors.md#property-selectors) `#classList` can be used to inspect a different DOM property of same type.
* The eventually hovered element may be customized using [virtual selector](selectors.md#hover) `$hover` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I enter T into X with ID Y

Searches a single element found somewhere on the current page that has a given ID and types given text into it.

* **T** is the text to be typed as a [single word or quoted string](expressions.md#string-or-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the desired ID as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `I enter T into X with id Y`.
* Inspected element for having desired ID can be replaced using [virtual part selector `$id`](./selectors.md#id). The Attribute is selected via [virtual selector](./selectors.md#attribute-selectors) `@id` using `"id"` by default.
* The element text **T** is entered into eventually may be customized using [virtual selector](selectors.md#enter) `$enter` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I enter T into X with name Y

Searches a single element found somewhere on the current page that has a given value for its `name` attribute and types given text into it.

* **T** is the text to be typed as a [single word or quoted string](expressions.md#string-or-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the desired name as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `I enter T into X named Y`.
* Inspected element for having desired name can be replaced using [virtual part selector `$name`](./selectors.md#name). The Attribute is selected via [virtual selector](./selectors.md#attribute-selectors) `@name` using `"name"` by default.
* The element text **T** is entered into eventually may be customized using [virtual selector](selectors.md#enter) `$enter` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I enter T into X with value Y

Searches a single element found somewhere on the current page that has a given value in its DOM property `value` and types given text into it.

* **T** is the text to be typed as a [single word or quoted string](expressions.md#string-or-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the desired value as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** don't exist.
* Inspected element for having desired value can be replaced using [virtual part selector `$value`](./selectors.md#value). Property is selected via [virtual selector](./selectors.md#property-selectors) `#value` using `"value"` by default.
* The element text **T** is entered into eventually may be customized using [virtual selector](selectors.md#enter) `$enter` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I enter T into X with label Y

Searches a single element found somewhere on the current page that has a [normalized](../glossary.md#normalized-string) label with given text and types given text into it.

* **T** is the text to be typed as a [single word or quoted string](expressions.md#string-or-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the label's text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `I enter T into X labelled with Y`.
* A matching element's label is selected via [virtual selector](./selectors.md#label) `$label` looking for an embedded `<label>` element by default.
* The element text **T** is entered into eventually may be customized using [virtual selector](selectors.md#enter) `$enter` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I enter T into X with partial label Y

Searches a single element found somewhere on the current page that has a [normalized](../glossary.md#normalized-string) label _containing_ given text at least and types given text into it.

* **T** is the text to be typed as a [single word or quoted string](expressions.md#string-or-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the label's text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `I enter T into X partially labelled with Y`.
* A matching element's label is selected via [virtual selector](./selectors.md#label) `$label` looking for an embedded `<label>` element by default.
* The element text **T** is entered into eventually may be customized using [virtual selector](selectors.md#enter) `$enter` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I enter T into X with text Y

Searches a single element found somewhere on the current page that has a [normalized](../glossary.md#normalized-string) textual content matching given text and types some text into it.

* **T** is the text to be typed as a [single word or quoted string](expressions.md#string-or-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the element's expected text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `I enter T into X reading Y`.
* A matching element's textual content is selected via [virtual selector](./selectors.md#text) `$text` using the element itself by default.
* The element text **T** is entered into eventually may be customized using [virtual selector](selectors.md#enter) `$enter` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I enter T into X with partial text Y

Searches a single element found somewhere on the current page that has a [normalized](../glossary.md#normalized-string) textual content _containing_ given text at least and types some text into it.

* **T** is the text to be typed as a [single word or quoted string](expressions.md#string-or-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the element's expected text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `I enter T into X partially reading Y`.
* A matching element's textual content is selected via [virtual selector](./selectors.md#text) `$text` using the element itself by default.
* The element text **T** is entered into eventually may be customized using [virtual selector](selectors.md#enter) `$enter` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I enter T into X marked/tagged as Y

Searches a single element of named type somewhere on the current page that has a class as additional constraint and types some text into it.

* **T** is the text to be typed as a [single word or quoted string](expressions.md#string-or-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is a single class name as a word (without whitespace or double quotes).
* **Aliases** don't exist.
* An element has given mark or tag if its DOM property `classList` contains named class. [Virtual part selector `$classList`](./selectors.md#classlist) can be used to inspect that DOM property on different elements. [Virtual selector](./selectors.md#property-selectors) `#classList` can be used to inspect a different DOM property of same type.
* The element text **T** is entered into eventually may be customized using [virtual selector](selectors.md#enter) `$enter` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I enter T into X not marked/tagged as Y

Searches a single element of named type somewhere on the current page that doesn't have a class as additional constraint and types some text into it.

* **T** is the text to be typed as a [single word or quoted string](expressions.md#string-or-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is a single class name as a word (without whitespace or double quotes).
* **Aliases** don't exist.
* An element doesn't have given mark or tag if its DOM property `classList` does not contain named class. [Virtual part selector `$classList`](./selectors.md#classlist) can be used to inspect that DOM property on different elements. [Virtual selector](./selectors.md#property-selectors) `#classList` can be used to inspect a different DOM property of same type.
* The element text **T** is entered into eventually may be customized using [virtual selector](selectors.md#enter) `$enter` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::


## Contextual queries

### C has/have X

Searches unconditionally one or more elements of same type in context of a referenced set of elements.

* **C** is a [contextual word](expressions.md#contextual-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Aliases** are `C has/have a list of X` and `there is a set of X`.

### C has/have X with ID Y

Searches in context of a referenced set of elements one or more elements of same type that share a given ID.

* **C** is a [contextual word](expressions.md#contextual-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the desired ID as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `C has/have X with id Y`.
* Inspected element for having desired ID can be replaced using [virtual part selector `$id`](./selectors.md#id). The Attribute is selected via [virtual selector](./selectors.md#attribute-selectors) `@id` using `"id"` by default.

### C has/have X with name Y

Searches in context of a referenced set of elements one or more elements of same type that share a given value for their `name` attribute.

* **C** is a [contextual word](expressions.md#contextual-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the desired name as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `C has/have X named Y`.
* Inspected element for having desired name can be replaced using [virtual part selector `$name`](./selectors.md#name). The Attribute is selected via [virtual selector](./selectors.md#attribute-selectors) `@name` using `"name"` by default.

### C has/have X with value Y

Searches in context of a referenced set of elements one or more elements of same type that share a given value looked up in either element's DOM property `value`.

* **C** is a [contextual word](expressions.md#contextual-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the desired value as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** don't exist.
* Inspected element for having desired value can be replaced using [virtual part selector `$value`](./selectors.md#value). Property is selected via [virtual selector](./selectors.md#property-selectors) `#value` using `"value"` by default.

### C has/have X with label Y

Searches in context of a referenced set of elements one or more elements of same type that have a [normalized](../glossary.md#normalized-string) label with given text.

* **C** is a [contextual word](expressions.md#contextual-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the label's text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `C has/have X labelled with Y`.
* Either element's label is selected via [virtual selector](./selectors.md#label) `$label` looking for an embedded `<label>` element by default.

### C has/have X with partial label Y

Searches in context of a referenced set of elements one or more elements of same type that have a [normalized](../glossary.md#normalized-string) label _containing_ given text at least.

* **C** is a [contextual word](expressions.md#contextual-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the label's text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `C has/have X partially labelled with Y`.
* Either element's textual content is selected via [virtual selector](./selectors.md#text) `$text` using the element itself by default.

### C has/have X with text Y

Searches in context of a referenced set of elements one or more elements of same type that have a [normalized](../glossary.md#normalized-string) textual content matching given text.

* **C** is a [contextual word](expressions.md#contextual-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the element's expected text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `C has/have X reading Y`.
* Either element's textual content is selected via [virtual selector](./selectors.md#text) `$text` using the element itself by default.

### C has/have X with partial text Y

Searches in context of a referenced set of elements one or more elements of same type that have a [normalized](../glossary.md#normalized-string) textual content _containing_ given text at least.

* **C** is a [contextual word](expressions.md#contextual-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Y** is the element's expected text as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `C has/have X partially reading Y`.
* Either element's textual content is selected via [virtual selector](./selectors.md#text) `$text` using the element itself by default.

### C has/have X which is/are enabled

Searches in context of a referenced set of elements one or more elements of same type that are enabled.

* **C** is a [contextual word](expressions.md#contextual-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Aliases** don't exist.
* An element is enabled if its boolean DOM property `disabled` is falsy. [Virtual part selector `$disabled`](./selectors.md#disabled) can be used to inspect different DOM elements per candidate matching **X**. [Virtual property selector](./selectors.md#property-selectors) `#disabled` can be used to inspect a different DOM property.

### C has/have X which is/are disabled

Searches in context of a referenced set of elements one or more elements of same type  that are disabled.

* **C** is a [contextual word](expressions.md#contextual-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Aliases** don't exist.
* An element is enabled if its boolean DOM property `disabled` is truthy. [Virtual part selector `$disabled`](./selectors.md#disabled) can be used to inspect different DOM elements per candidate matching **X**. [Virtual property selector](./selectors.md#property-selectors) `#disabled` can be used to inspect a different DOM property.

### C has/have X which is/are checked

Searches in context of a referenced set of elements one or more elements of same type that are checked.

* **C** is a [contextual word](expressions.md#contextual-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Aliases** don't exist.
* An element is checked if its boolean DOM property `checked` is truthy. [Virtual part selector `$checked`](./selectors.md#checked) can be used to inspect different DOM elements per candidate matching **X**. [Virtual property selector](./selectors.md#property-selectors) `#checked` can be used to inspect a different DOM property.

### C has/have X which is/are unchecked

Searches in context of a referenced set of elements one or more elements of same type that are checked.

* **C** is a [contextual word](expressions.md#contextual-word).
* **X** is a [cardinal word](expressions.md#cardinal-word).
* **Alias** is `C has/have X which is/are not checked`.
* An element is unchecked if its boolean DOM property `checked` is falsy. [Virtual part selector `$checked`](./selectors.md#checked) can be used to inspect different DOM elements per candidate matching **X**. [Virtual property selector](./selectors.md#property-selectors) `#checked` can be used to inspect a different DOM property.

### C has/have X marked/tagged as Y

Searches in context of a referenced set of elements one or more elements of same type that have a class as additional constraint.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Y** is a single class name as a word (without whitespace or double quotes).
* **Aliases** don't exist.
* An element has given mark or tag if its DOM property `classList` contains named class. [Virtual part selector `$classList`](./selectors.md#classlist) can be used to inspect that DOM property on different elements. [Virtual selector](./selectors.md#property-selectors) `#classList` can be used to inspect a different DOM property of same type.

### C has/have X not marked/tagged as Y

Searches in context of a referenced set of elements one or more elements of same type that don't have a class as additional constraint.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Y** is a single class name as a word (without whitespace or double quotes).
* **Aliases** don't exist.
* An element doesn't have given mark or tag if its DOM property `classList` does not contain named class. [Virtual part selector `$classList`](./selectors.md#classlist) can be used to inspect that DOM property on different elements. [Virtual selector](./selectors.md#property-selectors) `#classList` can be used to inspect a different DOM property of same type.

## Contextual actions

### I click on C

Clicks on a single element found in a previous step.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the element to click.
* **Alias** is `I click C`.
* The eventually clicked element may be customized using [virtual selector](selectors.md#click) `$click` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I hover over C

Moves pointer to a single element found in a previous step.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the element to hover.
* **Alias** is `I hover C`.
* The eventually hovered element may be customized using [virtual selector](selectors.md#hover) `$hover` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

### I enter T into C

Focuses single element found in a previous step and types in some given text.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the element to hover.
* **T** is the text to be typed as a [single word or quoted string](expressions.md#string-or-word).
* **Aliases** don't exist.
* The element text **T** is entered into eventually may be customized using [virtual selector](selectors.md#enter) `$enter` in context of matching element.

:::warning Limitations  
This action requires a single matching element to succeed.  
:::

## Contextual inspections

### C exists/exist

Asserts that all elements found in a previous step still exist in the current view.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Alias** is `C still exists/exist`.

### C does/do not exist

Asserts that all elements found in a previous step don't exist in the current view anymore.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Aliases** are `C does/do not exist anymore`, `C doesn't/don't exist`, `C doesn't/don't exist anymore`, `C is/are missing` and `C is/are gone`.

### C has/have the name Y

Asserts that all elements found in a previous step have an attribute `name` with given value.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Y** is expected value of either element's attribute `name` given as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** are `C has/have name Y` and `C is/are named Y`.
* Inspected element for having desired name can be replaced using [virtual part selector `$name`](./selectors.md#name). The Attribute is selected via [virtual selector](./selectors.md#attribute-selectors) `@name` using `"name"` by default.

### C has/have the ID Y

Asserts that all elements found in a previous step share a given ID.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Y** is expected ID given as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** are `C has/have ID Y`, `C has/have the id Y` and `C has/have id Y`.
* Inspected element for having desired ID can be replaced using [virtual part selector `$id`](./selectors.md#id). The Attribute is selected via [virtual selector](./selectors.md#attribute-selectors) `@id` using `"id"` by default.

### C has/have the value Y

Asserts that all elements found in a previous step share a given value.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Y** is expected value given as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** is `C has/have value Y`.
* Inspected element for having desired value can be replaced using [virtual part selector `$value`](./selectors.md#value). Property is selected via [virtual selector](./selectors.md#property-selectors) `#value` using `"value"` by default.

### C is/are enabled

Asserts that all elements found in a previous step are enabled.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Aliases** don't exist.
* An element is enabled if its boolean DOM property `disabled` is falsy. [Virtual part selector `$disabled`](./selectors.md#disabled) can be used to inspect different DOM elements per candidate matching **X**. [Virtual property selector](./selectors.md#property-selectors) `#disabled` can be used to inspect a different DOM property.

### C is/are disabled

Asserts that all elements found in a previous step are disabled.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Aliases** don't exist.
* An element is disabled if its boolean DOM property `disabled` is truthy. [Virtual part selector `$disabled`](./selectors.md#disabled) can be used to inspect different DOM elements per candidate matching **X**. [Virtual property selector](./selectors.md#property-selectors) `#disabled` can be used to inspect a different DOM property.

### C is/are checked

Asserts that all elements found in a previous step are checked.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Aliases** don't exist.
* An element is checked if its boolean DOM property `checked` is truthy. [Virtual part selector `$checked`](./selectors.md#checked) can be used to inspect different DOM elements per candidate matching **X**. [Virtual property selector](./selectors.md#property-selectors) `#checked` can be used to inspect a different DOM property.

### C is/are unchecked

Asserts that all elements found in a previous step are not checked.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Alias** is `C is/are not checked`.
* An element is unchecked if its boolean DOM property `checked` is falsy. [Virtual part selector `$checked`](./selectors.md#checked) can be used to inspect different DOM elements per candidate matching **X**. [Virtual property selector](./selectors.md#property-selectors) `#checked` can be used to inspect a different DOM property.

### C is/are selected

Asserts that all elements found in a previous step are selected.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Aliases** don't exist.
* An element is selected if its boolean attribute `selected` has been set. [Virtual part selector `$selected`](./selectors.md#selected) can be used to inspect different DOM elements per candidate matching **X**. [Virtual selector](./selectors.md#attribute-selectors) `@selected` can be used to inspect a different boolean attribute.

### C is/are unselected

Asserts that all elements found in a previous step are not selected.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Alias** is `C is/are not selected`.
* An element is unselected if its attribute `selected` has not been set. [Virtual part selector `$selected`](./selectors.md#selected) can be used to inspect different DOM elements per candidate matching **X**. [Virtual selector](./selectors.md#attribute-selectors) `@selected` can be used to inspect a different boolean attribute.

### C has/have the label Y  

Asserts that all elements found in a previous step share a [normalized](../glossary.md#normalized-string) label with given text.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Y** is expected label as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** are `C has/have label Y` and `C is/are labelled with Y`.
* Either element's label is selected via [virtual selector](./selectors.md#label) `$label` looking for an embedded `<label>` element by default.

### C has/have the partial label Y  

Asserts for every element found in a previous step that its [normalized](../glossary.md#normalized-string) label _contains_ a given text at least.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Y** is expected label as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Aliases** are `C has/have partial label Y` and `C is/are partially labelled with Y`.
* Either element's label is selected via [virtual selector](./selectors.md#label) `$label` looking for an embedded `<label>` element by default.

### C has/have text Y  

Asserts that all elements found in a previous step share a [normalized](../glossary.md#normalized-string) textual content.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Y** is expected textual content as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** are `C reads/read Y` and `C is reading Y`.
* Either element's textual content is selected via [virtual selector](./selectors.md#text) `$text` using the element itself by default.

### C has/have partial text Y  

Asserts for every element found in a previous step that its [normalized](../glossary.md#normalized-string) textual content _includes_ a given text at least.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Y** is expected textual content as a [single word, quoted string or regular expression](expressions.md#text-or-regexp).
* **Alias** are `C partially reads/read Y` and `C is partially reading Y`.
* Either element's textual content is selected via [virtual selector](./selectors.md#text) `$text` using the element itself by default.

### C is/are marked/tagged as Y

Asserts for every element found in a previous step that it has a class assigned.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Y** is a single class name as a word (without whitespace or double quotes).
* **Aliases** don't exist.
* An element has given mark or tag if its DOM property `classList` contains named class. [Virtual part selector `$classList`](./selectors.md#classlist) can be used to inspect that DOM property on different elements. [Virtual selector](./selectors.md#property-selectors) `#classList` can be used to inspect a different DOM property of same type.

### C is/are not marked/tagged as Y

Asserts for every element found in a previous step that it doesn't have a class assigned.

* **C** is a [contextual word](expressions.md#contextual-word) selecting the elements to inspect.
* **Y** is a single class name as a word (without whitespace or double quotes).
* **Aliases** don't exist.
* An element doesn't have given mark or tag if its DOM property `classList` does not contain named class. [Virtual part selector `$classList`](./selectors.md#classlist) can be used to inspect that DOM property on different elements. [Virtual selector](./selectors.md#property-selectors) `#classList` can be used to inspect a different DOM property of same type.
