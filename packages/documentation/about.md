# A brief introduction

[Gherkin](https://cucumber.io/docs/gherkin/) is a domain-specific language for expressing test cases. It's similar to writing user stories and looks like this:

```gherkin
Feature: Browse shop
	Scenario: add item to cart
		Given the shop's homepage has been opened
		And there are a few products provided as a list
		When I click on a listed product
		And click on the button with label "Add to cart"
		Then the number of products in cart is "1"
```

Every line starting with one of the keywords `Given`, `When`, `Then`, `And` and `But` is a test step. Every step has an arbitrary phrase following either keyword. For every possible phrase a custom step must be defined by implementing a code performing either query, action or assertion expressed in the phrase. Those implementations are relying on a framework called [Cucumber](https://cucumber.io/docs/cucumber/) which is available for different languages and coding ecosystems. 

This highly opinionated library - **contextual-gherkin** - is designed to work with web applications and websites. It is based on [gherkin-testcafe](https://www.npmjs.com/package/gherkin-testcafe) and [TestCafe](https://testcafe.io/) for interacting with a web application running in an attached browser. The library provides several generic step definitions as well as tools to easily implement additional step definitions yourself. 

All its steps are kept as generic as possible to be useful in many projects. They are all associated with _patterns_ of phrases rather than individual phrases. Phrases matched by those patterns are close to natural (English) language to keep the test cases as readable as possible. They are kept short and simple. And they are designed to refer to each other, which is why they are called [_contextual_](glossary.md#contextuality). This way, the steps supported out-of-the-box are suitable for describing a lot of use cases with a rather limited set of step definitions. 

Given the example above, using **contextual-gherkin** requires some rephrasing of that test case which might look like this:

```gherkin
Feature: Browse shop
	Scenario: add item to cart
		Given the shop's homepage has been opened
		And there is a list of products
		And there is a button with label "Add to cart"
		And there is an indicator with label "Items in cart"
		When I click on the first product
		And I click on the button
		Then the indicator reads "1"
```

[Using the CLI helper](tutorial/quickstart.md) provided by **contextual-gherkin**, setting up E2E testing for your project takes just a few minutes before you are ready to write and run your first test cases.
