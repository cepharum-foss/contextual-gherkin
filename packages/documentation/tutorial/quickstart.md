# Getting started

**contextual-gherkin** can be used as part of a project for developing some web application or website. Accordingly, it also works in dedicated projects set up to test some separately existing web application or website.

This tutorial is about setting up a project of the latter kind for simplified demonstration.

## Create demo project

For illustration purposes, let's create a project testing an existing website such as https://duckduckgo.com. **contextual-gherkin** is part of [npm](https://npmjs.com) ecosystem, thus the project needs to be set up accordingly. 

Create an empty project folder and run this command to prepare for using npm packages:

```bash
npm init -y
```

:::info  
Apart from this step, all following steps apply to any other use case adding **contextual-gherkin** to an existing project, too.  
:::

## Install dependencies

Next, add **contextual-gherkin** as a dependency to this project or to any other project you'd like to use it with for E2E testing. Usually, tests are run in context of development, only, thus adding it as a development dependency should be preferred:

```bash
npm install -D @cepharum/contextual-gherkin
```

**contextual-gherkin** relies on additional dependencies. There is a convenience script helping you with discovering those missing dependencies in your project. Run it like this:

```bash
npx contextual-gherkin
```

On first run in a project, this is going to display something like this:

```
@cepharum/contextual-gherkin 1.0.2
run

    npm i -D @cucumber/cucumber@^10.1.0 @cucumber/cucumber-expressions@^17.0.1 gherkin-testcafe@^7.2.0 testcafe@~3.5.0
```

It's listing missing or mismatching peer dependencies of **contextual-gherkin**. Run the given command to fix dependencies:

```bash
npm i -D @cucumber/cucumber@^10.1.0 @cucumber/cucumber-expressions@^17.0.1 gherkin-testcafe@^7.2.0 testcafe@~3.5.0
```

:::info What if different versions are given in your case?  
The list of dependencies or their versions may change over time, and we might not update this manual every time the dependencies are updated. 

Always stick to the list of dependencies as presented on your device.
:::

## Create test environment

Add some folders to your project to contain your test files. We suggest one folder **test/e2e/features** to contain all Gherkin files and another folder **test/e2e/steps** to contain files for implementations of custom steps.

```bash
mkdir -p test/e2e/features
mkdir test/e2e/steps
```

Define another npm script for running those tests accordingly. Open the **package.json** file and add this line in its `scripts` section:

```json
"test:e2e": "contextual-gherkin test/e2e/**/*.feature test/e2e/**/*.js"
```

By using the convenience script as before, some required integration tasks are covered automatically. You just have to provide glob patterns selecting Gherkin files and custom step definitions in those folders created before.

:::info  
Gherkin files usually use extension **.feature**. The step definition files are regular Javascript files.  
:::

Running E2E tests is now as easy as invoking that script:

```bash
npm run test:e2e
```

If you invoke that script now, it will take a moment, briefly open a browser window most probably before displaying an error message like this one eventually:

```
@cepharum/contextual-gherkin 0.1.0-alpha.12
ERROR TypeError: Reduce of empty array with no initial value
    at Array.reduce (<anonymous>)
    at GherkinTestcafeCompiler.getTests (C:\Users\foo\project\node_modules\gherkin-testcafe\src\compiler.js:249:19)
    at C:\Users\foo\project\node_modules\testcafe\src\runner\bootstrapper.ts:267:25
    at Object.guardTimeExecution [as default] (C:\Users\foo\project\node_modules\testcafe\src\utils\guard-time-execution.ts:6:31)
    at Bootstrapper._getTests (C:\Users\foo\project\node_modules\testcafe\src\runner\bootstrapper.ts:266:21)
    at async Promise.all (index 1)
    at Bootstrapper._bootstrapParallel (C:\Users\foo\project\node_modules\testcafe\src\runner\bootstrapper.ts:366:38)
    at Bootstrapper.createRunnableConfiguration (C:\Users\foo\project\node_modules\testcafe\src\runner\bootstrapper.ts:390:25)
```

That's fine for now. The error is basically due to those patterns not matching any existing Gherkin file describing a test to run.

## Create a test

So, let's create a test file **test/e2e/features/search.feature** with following content:

```gherkin
Feature: duckduckgo.com
	Scenario: list matches

		Given I am on the DuckDuckGo homepage
		And there is an "input field"
		And there is a button
		And there are no matches
		When I enter "gherkin" into the field
		And I click the button
		Then there are matches
```

This is a list of seven steps describing how to perform the actual test. 

Next, you would have to implement all those steps yourself. But this is where **contextual-gherkin** starts to support you as it is covering all steps but the first one automatically. All it takes is some setup to customize **contextual-gherkin** for the project.

## Create configuration

Let's create another file **test/e2e/steps/setup.js** with the following content:

```javascript
const { Before } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Before( testController => {
	ContextualGherkin( testController, {
		selectors: {
			field: "#search_form_input_homepage",
			button: "#search_button_homepage",
			match: "#links > .nrn-react-div",
		},
		aliases: {
			field: "input field"
		},
	} );
} );
```

This file is implementing a hook run prior to testing. It is injecting a configured instance of **contextual-gherkin** into the context of provided test controller of TestCafe. 

The [configuration](../api/configuration.md) is mapping names used in the Gherkin file into CSS selectors for picking either element. Aliases are defined to have **field** and **input field** address the same thing.

## Implement custom steps

As mentioned before, one of the steps isn't supported by **contextual-gherkin** and we have to [implement it ourselves](custom-steps.md) as it is too specific to this project.

Create a file **test/e2e/steps/custom.js** with following content:

```javascript
const { Given } = require( "@cucumber/cucumber" );

Given( "I am on the DuckDuckGo homepage", async testController => {
	await testController.navigateTo( "https://duckduckgo.com" );
} );
```

## Final run

Now, with all this set up, run E2E tests again:

```bash
npm run test:e2e
```

This will open a browser showing DuckDuckGo, how the term `gherkin` is entered before clicking the button so the matches appear. After successful run, the script has displayed something like this:

```
 Running tests in:
 - Chrome 109.0.0.0 / Windows 11

 Feature: duckduckgo.com
 √ Scenario: list matches


 1 passed (7s)
```

The browser used for testing can be controlled using environment variable `E2E_BROWSER`. If omitted, it defaults to `chrome` on Windows and to `chromium:headless` on other systems. See [TestCafe documentation](https://testcafe.io/documentation/402639/reference/command-line-interface#browser-list) for a list of supported browser aliases.

## Time for a deep dive

You are ready to start writing lots of tests now. However, make yourself familiar with the features of **contextual-gherkin** library first:

* [Glossary](../glossary.md)
* [Configuration](../api/configuration.md)
* [Selectors](../api/selectors.md)
* [Cucumber Expression](../api/expressions.md)
* [Phrases](../api/phrases.md)
