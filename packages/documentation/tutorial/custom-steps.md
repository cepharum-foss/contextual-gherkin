# Implementing custom steps

This tutorial is assuming you've created some test cases written in Gherkin already and succeeded on running them, but now you are stuck because **contextual-gherkin** is missing a phrase that you like to use. In addition, you should have done some coding in JavaScript before, too.

It starts with basics that aren't specific to **contextual-gherkin** at all. But eventually, you'll learn how to gain access to a context that's resulting from a previous step provided by **contextual-gherkin**.

## Create a step definition

Let's assume, that all your custom step definitions and runtime configurations reside in folder **test/e2e/steps**. Create a new file in that folder and name it e.g. **my-custom-step.js** and open it in a text editor. Of course, you can have multiple step definitions in a single file and so you could extend some existing file instead, too.

The absolute minimum without any ties to **contextual-gherkin** for implementing a step definition is this:

```javascript
const { Then } = require( "@cucumber/cucumber" );

Then( "<phrase>", async ( testController ) => {
	// TODO implement what should happen if `<phrase>` is used in a test
} );
```

Put this code in that created file.

`Then` is just one out of several options here, but at least in context of **contextual-gherkin** and its underlying dependency [gherkin-testcafe](https://www.npmjs.com/package/gherkin-testcafe) it does not matter which one you are using. Feel free to always use e.g. `Then` or pick the method matching best Gherkin keyword the phrase is meant to be used with.

`<phrase>` is a placeholder here and has to be replaced with an actual phrase you want to trigger the step definition provided as callback in second argument to `Then()` whenever that phrase is used in a Gherkin-based test description. It is the part succeeding either [Gherkin keyword](https://cucumber.io/docs/gherkin/reference/#keywords).

Let's have a step definition for the following test:

```gherkin{1}
Then the panel has a width of 400px
```

The phrase is `the panel has a width of 400px` and that's what we have to use in that step definition above:

```javascript
const { Then } = require( "@cucumber/cucumber" );

Then( "<phrase>", ( testController ) => { // [!code --]
Then( "the panel has a width of 400px", async ( testController ) => { // [!code ++]
	// TODO implement what should happen if `<phrase>` is used in a test
} );
```

The `testController` provided as first argument to the callback exposes [the API of TestCafe](https://testcafe.io/documentation/402665/reference/test-api/testcontroller) for interacting with the browser, the currently presented page and all its elements. You can use it to assert the [width of an element](https://testcafe.io/documentation/402670/reference/test-api/domnodestate):

```javascript
const { Then } = require( "@cucumber/cucumber" );

Then( "the panel has a width of 400px", async ( testController ) => {
	// TODO implement what should happen if `<phrase>` is used in a test // [!code --]
	await testController.expect( Selector( "#panel" ).offsetWidth ).eql( 400 ); // [!code ++]
} );
```

## Phrasing patterns

As of now, the implemented step definition is matching the phrase `the panel has a width of 400px`, only. What if you want to test a different size? You could create another step definition, but that's becoming a tedious approach pretty soon.

Instead of that, inject [Cucumber expressions](https://github.com/cucumber/cucumber-expressions#readme) into your phrase to convert it into a pattern matching multiple phrases:

```javascript
const { Then } = require( "@cucumber/cucumber" );

Then( "the panel has a width of 400px", async ( testController ) => { // [!code --]
Then( "the panel has a width of {int}px", async ( testController ) => { // [!code ++]
	await testController.expect( Selector( "#panel" ).offsetWidth ).eql( 400 );
} );
```

The literal value `400` has been replaced with the Cucumber expression `{int}` which is matching any reasonable sequence of digits. The resulting integer represented by those digits is extracted and made available to your implementation in a list of variables provided as second argument to your callback:

```javascript
const { Then } = require( "@cucumber/cucumber" );

Then( "the panel has a width of {int}px", async ( testController ) => { // [!code --]
Then( "the panel has a width of {int}px", async ( testController, [ width ] ) => { // [!code ++]
	await testController.expect( Selector( "#panel" ).offsetWidth ).eql( 400 );
} );
```

You can use this information in your implementation now:

```javascript
const { Then } = require( "@cucumber/cucumber" );

Then( "the panel has a width of {int}px", async ( testController, [ width ] ) => {
	await testController.expect( Selector( "#panel" ).offsetWidth ).eql( 400 ); // [!code --]
	await testController.expect( Selector( "#panel" ).offsetWidth ).eql( width ); // [!code ++]
} );
```

At this point you are able to test different widths of your panel.

## Access the context

But wait, are you actually testing the width of a particular panel, only? And are you sure the panel is matching CSS selector `#panel`?

The strength of **contextual-gherkin** is its ability to address elements of your application rather easily. Now it's time to use this power in your custom step definition, too. 

:::info  
Your code starts to depend on **contextual-gherkin** now.  
:::

First, import a helper for simplified access on its API:

```javascript
const { Then } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" ); // [!code ++]

Then( "the panel has a width of {int}px", async ( testController, [ width ] ) => {
	await testController.expect( Selector( "#panel" ).offsetWidth ).eql( width );
} );
```

Invoke that helper in your step definition to gain access on [development API](../api/development.md) of **contextual-gherkin**:

```javascript
const { Then } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Then( "the panel has a width of {int}px", async ( testController, [ width ] ) => {
	const lib = ContextualGherkin( testController ); // [!code ++]
	// [!code ++]
	await testController.expect( Selector( "#panel" ).offsetWidth ).eql( width );
} );
```

The leading part of your phrase - `the panel` - resembles terms matching our custom Cucumber expression [`{contextual-word}`](../api/expressions.md#contextual-word), so replace it accordingly:

```javascript
const { Then } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Then( "the panel has a width of {int}px", async ( testController, [ width ] ) => { // [!code --]
Then( "{contextual-word} has a width of {int}px", async ( testController, [ width ] ) => { // [!code ++]
	const lib = ContextualGherkin( testController );

	await testController.expect( Selector( "#panel" ).offsetWidth ).eql( width );
} );
```

A term matched by `{contextual-word}` can refer to a single element as well as multiple elements. Rules of natural language might be violated when expressing a plural context in combination with `has`. Thus, supporting [alternative texts](https://github.com/cucumber/cucumber-expressions#alternative-text) is preferable here:

```javascript
const { Then } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Then( "{contextual-word} has a width of {int}px", async ( testController, [ width ] ) => { // [!code --]
Then( "{contextual-word} has/have a width of {int}px", async ( testController, [ width ] ) => { // [!code ++]
	const lib = ContextualGherkin( testController );

	await testController.expect( Selector( "#panel" ).offsetWidth ).eql( width );
} );
```

This support for using either `has` or `have` does not prevent test authors from violating rules of natural language, but complying with them is an option now, at least.

:::details A coding style for more complex pattern variants
Using alternative text as illustrated above works on a word-by-word basis, only. If you intend to support alternative phrasings that deviate in a more complex way from each other, it is best to extract the callback function and re-use it in association with either phrasing pattern, e.g.

```javascript
Then( "{contextual-word} has/have a width of {int}px", ( testController, [ width ] ) => myStep( testController, width ) );
Then( "{contextual-word} is {int}px wide", ( testController, [ width ] ) => myStep( testController, width ) );

async function myStep( testController, width ) => {
	...
}
```

Mind the way of forwarding variables. In different phrasing patterns, the embedded Cucumber expressions might be given in different order. Your code is prepared for that by individually mapping them per forwarding callback in lines 1 and 2.

All step definitions of **contextual-gherkin** are implemented like that. In addition, they are all exported so that you can associate them with additional phrasing variants yourself.
:::

By adding a second Cucumber expression, two variables are provided in second argument to your callback for consumption now. Their order is equivalent to the order of Cucumber expressions used in the phrasing pattern.

```javascript
const { Then } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Then( "{contextual-word} has/have a width of {int}px", async ( testController, [ width ] ) => { // [!code --]
Then( "{contextual-word} has/have a width of {int}px", async ( testController, [ term, width ] ) => { // [!code ++]
	const lib = ContextualGherkin( testController );

	await testController.expect( Selector( "#panel" ).offsetWidth ).eql( width );
} );
```

`term` is an object with properties exposing essential parts of whatever term has been matching `{contextual-word}`. Use it with the development API of **contextual-gherkin** to access the element(s) this term is referring to in context of another step:

```javascript
const { Then } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Then( "{contextual-word} has/have a width of {int}px", async ( testController, [ term, width ] ) => {
	const lib = ContextualGherkin( testController );
	const context = await lib.getContextFor( term ); // [!code ++]

	await testController.expect( Selector( "#panel" ).offsetWidth ).eql( width );
} );
```

Finally, inspect provided context instead of the particular element that has been selected before:

```javascript
const { Then } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Then( "{contextual-word} has/have a width of {int}px", async ( testController, [ term, width ] ) => {
	const lib = ContextualGherkin( testController );
	const context = await lib.getContextFor( term );

	await testController.expect( Selector( "#panel" ).offsetWidth ).eql( width ); // [!code --]
	await testController.expect( context.matches.offsetWidth ).eql( width ); // [!code ++]
} );
```

Your step definition is now ready to work in context of other steps that eventually pick an element you like to inspect:

```gherkin{2}
Given there is a panel
And it has a width of 500px
```

As a preliminary conclusion, this example illustrates several things:

* You aren't limited to using `the panel` in your step's phrases anymore.
* However, whatever you are addressing there must be found by a preceding step first.
* And of course, you are still free to check any width you like.

## Find elements

The step definition has been depending on an existing context, which is a set of elements looked up in a previous step. Let's shift the intention of our step definition a bit and make it find elements in context of other elements found before and use the width of looked up elements as an additional constraint. So, the step is now meant to match phrases like

```gherkin{1}
Given the panel has buttons with a width of 64px
```

Adjust our step definition to use a pattern matching that new phrase:

```javascript
const { Then } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Then( "{contextual-word} has/have a width of {int}px", async ( testController, [ term, width ] ) => { // [!code --]
Then( "{contextual-word} has/have a buttons with a width of {int}px", async ( testController, [ term, width ] ) => { // [!code ++]
	const lib = ContextualGherkin( testController );
	const context = await lib.getContextFor( term );

	await testController.expect( context.matches.offsetWidth ).eql( width );
} );
```

Of course, you don't intend to find `buttons`, only, but want to find all kinds of elements. Thus, we use another custom Cucumber expression of **contextual-gherkin** which is named [`{cardinal-word}`](../api/expressions.md#cardinal-word):

```javascript
const { Then } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Then( "{contextual-word} has/have buttons with a width of {int}px", async ( testController, [ term, width ] ) => { // [!code --]
Then( "{contextual-word} has/have {cardinal-word} with a width of {int}px", async ( testController, [ term, query, width ] ) => { // [!code ++]
	const lib = ContextualGherkin( testController );
	const context = await lib.getContextFor( term );

	await testController.expect( context.matches.offsetWidth ).eql( width );
} );
```

:::tip Mind the variables!  
This change is introducing a third expression. The list of provided variables has been extended accordingly.
:::

`{cardinal-word}` is an object describing parts of matching term via several properties such as `number` for the expressed number of elements and `word` for the expressed name. Start with looking up all elements in current context that match named type:

```javascript
const { Then } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Then( "{contextual-word} has/have {cardinal-word} with a width of {int}px", async ( testController, [ term, query, width ] ) => {
	const lib = ContextualGherkin( testController );
	const context = await lib.getContextFor( term );
	const candidates = await context.find( query.word ); // [!code ++]

	await testController.expect( context.matches.offsetWidth ).eql( width );
} );
```

By intention, the step isn't about asserting state of some element anymore. It's about looking for different elements that match an expressed constraint. Thus, the assertion used before must be replaced with a function filtering our candidates:

```javascript
const { Then } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Then( "{contextual-word} has/have {cardinal-word} with a width of {int}px", async ( testController, [ term, query, width ] ) => {
	const lib = ContextualGherkin( testController );
	const context = await lib.getContextFor( term );
	const candidates = await context.find( query.word );

	await testController.expect( context.matches.offsetWidth ).eql( width ); // [!code --]
	const matches = await candidates.filterFn( node => node?.offsetWidth === width, { width } ); // [!code ++]
} );
```

The callback function provided as argument to `candidates.filterFn()` will be invoked in runtime of browser which might be running on a different device. Thus, the variable `width` has to be provided as a dependency of that function in second argument to `candidates.filterFn()` ([read more](../api/selectors.md#callback-function-constraints)).

`matches` is now describing elements of selected type which satisfy the additional constraint of matching a given width. 

But `{cardinal-word}` is expressing an expected amount of matches we haven't checked, yet. And it supports different ways of checking amount of matching elements, too. The term matched by `{cardinal-word}` can select an exact number of elements, a minimum or a maximum. This results in slightly more complex code which has been extracted into a helper function for your convenience:

```javascript
const { Then } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Then( "{contextual-word} has/have {cardinal-word} with a width of {int}px", async ( testController, [term, query, width] ) => {
	const lib = ContextualGherkin( testController );
	const context = await lib.getContextFor( term );
	const candidates = await context.find( query.word );

	const matches = await candidates.filterFn( node => node?.offsetWidth === width, { width } );
	// [!code ++]
	await matches.checkCardinalWord( query ); // [!code ++]
} );
```

That's it already. And there is another benefit of using that helper function: If the number of matching elements is satisfying the requested amount, this helper function is tracking provided matches as another context some follow-up step can refer to. This means, that you can use this step definition in a test like this:

```gherkin{2}
Given there is a panel
And it has at least 3 buttons with a width of 64px
When I click on the first button
Then ...
```

## Find elements globally

Our step definition has been searching elements in context of another set of elements. But what if there is no previous set of elements providing a context? What if we want to find matching elements with a certain width anywhere on the page? Such a _global query_ requires just a few adjustments to our latest version.

First, the phrase must be changed. **contextual-gherkin** is based on certain keywords we suggest to stick to. Using them helps to distinguish the intention of any available step definition. And it prevents multiple implementations from accidentally matching the same phrase. For a global query, the keyword is either `there is` or `there are`.

:::details Beware of phrase matching  
In fact, having two step definitions matching the same phrase can be a common issue.

As mentioned before, you can't have different step definitions matching the same phrase, not even when using different Gherkin keywords. For processing Gherkin steps, their leading keywords such as `Given`, `When` or `Then` don't matter at all. 

That's why having two step definitions matching the same phrase can cause unexpected issues when using that phrase which might be hard to debug.  
:::

Let's use those keywords instead of looking up some context:

```javascript
const { Then } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Then( "{contextual-word} has/have {cardinal-word} with a width of {int}px", async ( testController, [term, query, width] ) => { // [!code --]
	Then( "there is/are {cardinal-word} with a width of {int}px", async ( testController, [query, width] ) => { // [!code ++]
		const lib = ContextualGherkin( testController );
		const context = await lib.getContextFor( term );
		const candidates = await context.find( query.word );

		const matches = await candidates.filterFn( node => node?.offsetWidth === width, { width } );

		await matches.checkCardinalWord( query );
	} );
```

Mind the reduction of provided variables in second argument to our callback, too!

Instead of using the library to find a context for starting another search, we now use the library itself to start that search:

```javascript
const { Then } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Then( "there is/are {cardinal-word} with a width of {int}px", async ( testController, [query, width] ) => {
	const lib = ContextualGherkin( testController );
	const context = await lib.getContextFor( term ); // [!code --]
	const candidates = await context.find( query.word ); // [!code --]
	const candidates = await lib.find( query.word ); // [!code ++]

	const matches = await candidates.filterFn( node => node?.offsetWidth === width, { width } );

	await matches.checkCardinalWord( query );
} );
```

And there we are ... the step definition works without a context:

```gherkin{1}
Given there are at least 3 buttons with a width of 64px
When I click on the first button
Then ...
```
