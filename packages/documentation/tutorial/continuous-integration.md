# Continuous integration

## Script setup example

In your package.json file, a bunch of scripts for running E2E tests might look like this:

```json
{
	...
	"scripts": {
		"e2e": "concurrently -n serve,e2e -s command-e2e -k npm:e2e:serve npm:e2e:run",
		"e2e:serve": "serve -p 3000 --no-request-logging e2e/pages",
		"e2e:run": "wait-on http://localhost:3000 && contextual-gherkin --folder e2e",
		...
	},
	"dependencies": {
		"concurrently": "^8.2.2",
		"serve": "^14.2.3",
		"wait-on": "^7.2.0",
		...
	}
}
```

* A set of static HTML pages in folder **e2e/pages** is presented using a simple HTTP server invoked by script `e2e:serve`. 
* Accordingly, script `e2e:run` is invoking **contextual-gherkin** to process Gherkin-based test scenarios in folder **e2e** as soon as the HTTP server has become available. 
* Both scripts are combined by script `e2e` eventually. This script is suitable for running e.g. in a continuous integration.

## GitLab CI

We favor GitLab and its CI for our projects. Setting up contextual-gherkin for running your E2E tests is pretty straightforward there.

```yaml
variables:
  E2E_BROWSER:
    description: "selects browser for E2E testing"
    value: "chromium:headless"

test:e2e:
  stage: test
  image:
    name: "testcafe/testcafe:3.5.0"
    entrypoint: [ "" ]
    docker:
      user: root
  before_script:
    - npm ci
  script:
    - npm run e2e
```

* The version of the image should match the version of testcafe requested as peer dependency for **contextual-gherkin**. Here it is **3.5.0**.
* The testcafe image runs any provided script as user **user** by default. GitLab CI might still use **root** for setting up the repository, though. In this case you need the `docker.user` option selecting whatever user GitLab CI is running the job as. Otherwise you might get an **EPERM** issue on running npm.
