const { Api } = require( "./lib/api.js" );

module.exports = {
	...require( "./steps/global-find" ),
	...require( "./steps/global-action" ),
	...require( "./steps/local-find" ),
	...require( "./steps/local-action" ),
	...require( "./steps/local-state" ),

	/**
	 * Configures test controller to support phrases of contextual Gherkin.
	 *
	 * @param {TestController} testController test controller to inject contextual-gherkin into
	 * @param {Configuration} config runtime configuration for contextual-gherkin,
	 *        omit for gaining access to existing integration instead of starting integration
	 * @return {Api} integration of contextual-gherkin
	 */
	ContextualGherkin( testController, config = undefined ) {
		if ( typeof config === "undefined" ) {
			return Api.access( testController );
		}

		// eslint-disable-next-line no-param-reassign
		return ( testController.ctx.__contextualGherkinLibrary = new Api( testController, config ) );
	},
};
