/// <reference types="gherkin-testcafe" />

declare module "@cepharum/contextual-gherkin" {
	/**
	 * Inserts or replaces instance of contextual-gherkin to use in context of
	 * provided test controller or returns the most recently instance injected
	 * that way.
	 *
	 * @param testController test controller to be used
	 * @param configuration runtime configuration of instance to create and inject into test controller, omit for reading instance, only
	 * @returns instance of contextual-gherkin injected into test controller most recently (which might be just now)
	 */
	export function ContextualGherkin(testController: TestController, configuration?: Configuration): Api;

	/**
	 * Customizes contextual Gherkin in context of a tested application.
	 */
	interface Configuration {
		/**
		 * Maps type names of UI elements into descriptions on how to select
		 * them in context of a tested application.
		 */
		selectors: Selectors;

		/**
		 * Lists sets of aliases each represented as a list of strings aliasing
		 * each other.
		 */
		aliases: Aliases;
	}

	/**
	 * Maps name of an element's type such as "button" into description of how
	 * to select it in context of a tested application.
	 */
	interface Selectors {
		[type: string]: SelectorVariant;
	}

	/**
	 * Maps name of an element's type such as "item" into description of how
	 * to select it in context of another type of element such as "list".
	 */
	interface SubSelectors extends Selectors {
		/**
		 * Describes how to select type of element this sub-selector is
		 * associated with.
		 */
		'': SelectorVariant;
	}

	/**
	 * Represents predicate suitable for deciding whether some provided node is
	 * matching selector or not.
	 *
	 * @param node refers to node to be tested
	 * @param index 0-based index of node in set of nodes to be tested
	 * @returns true if node is matching expectations of selector, false otherwise
	 */
	type SelectorFn = (node: Node) => boolean;

	/**
	 * Combines a predicate function with custom dependencies to be provided in
	 * function.
	 */
	type SelectorFnWithDependencies = [ SelectorFn, { [key: string]: any } ];

	/**
	 * Describes ways of selecting particular type of element.
	 *
	 * @note `false` can be used to request using context as matches.
	 */
	type SimpleSelector = false | CSSSelector | SelectorFn | SelectorFnWithDependencies;

	/**
	 * Combines options available for describing how to select a particular type
	 * of element and/or other types of elements in context of the former.
	 */
	type SelectorVariant = SimpleSelector | SubSelectors;

	/**
	 * Describes query for selecting elements.
	 *
	 * Query is `false` if found selector is `false`, too. Otherwise, it is an
	 * array of arguments to be used on invoking Selector methods.
	 */
	interface Query {
		query: false | CSSSelector | SelectorFn;
		dependencies?: { [key: string]: any };
		selectors: SubSelectors;
	}

	/**
	 * Represents a single CSS selector as commonly supported by browsers.
	 */
	type CSSSelector = string;

	/**
	 * Maps aliases into set of names either alias is aliasing. Circular and
	 * recursive definitions are possible.
	 */
	type Aliases = { [alias: string]: ( string | string[] ) };

	/**
	 * Combines description of a number and a keyword as resulting from custom
	 * cucumber expressions such as {cardinal-word} and {ordinal-word}.
	 */
	interface NumberAndWord {
		/**
		 * Provides number-part of processed cucumber expression.
		 */
		number: number;

		/**
		 * Provides keyword-part of processed cucumber expression.
		 */
		word: string;

		/**
		 * Provides the assumed cardinality of the described phrase.
		 *
		 * Cardinality is used to interpret indices into the history of tested
		 * nodes in different ways. It becomes important on tagging context
		 * nodes when tracking them in a history of previous nodes.
		 *
		 * @example A phrase like "one button" has "singular" cardinality as it
		 *          is clearly selecting a single button, only. The phrase "at
		 *          least one button" has "singular" cardinality, too, as it
		 *          might match multiple buttons, but follow-up references to
		 *          it will use singular cardinality (such as "this button").
		 *          Phrases "buttons" and "at least two buttons" have "plural"
		 *          cardinality for the same reason as we expect them to be
		 *          picked up by a contextual phrase with clear plural
		 *          cardinality such as "these buttons".
		 */
		cardinality?: Cardinality;
	}

	/**
	 * Describes rough categories of search results based on their (assumed)
	 * number of elements.
	 */
	enum Cardinality {
		/**
		 * Indicates a match is expected to represent a single matching element.
		 */
		SINGULAR = "singular",

		/**
		 * Indicates a match is expected to represent multiple matching elements.
		 */
		PLURAL = "plural",
	}

	/**
	 * Defines accepted values for arguments and properties of type Cardinality.
	 */
	type CardinalityValues = "singular" | "plural";

	/**
	 * Selects mode for comparing two cardinal amounts.
	 */
	enum AmountMode {
		/**
		 * Comparison succeeds if both values are equal.
		 */
		EQUALITY = "==",

		/**
		 * Comparison succeeds if first value is lesser than or equal second one.
		 */
		AT_MOST = "<=",

		/**
		 * Comparison succeeds if first value is greater than or equal second one.
		 */
		AT_LEAST = ">=",
	}

	/**
	 * Defines accepted values for picking a comparison mode of cardinal amounts.
	 */
	type AmountModeValues = "==" | "<=" | ">=";

	/**
	 * Describes data found in custom cucumber expression {contextual-word}.
	 */
	interface ContextualWord {
		/**
		 * Provides zero-based index into history of previous searches.
		 *
		 * 0 is addressing most recent search. 1 is addressing search preceding
		 * most recent one. etc.
		 *
		 * @example Phrases "it" and "the latter icon" result in `index` being 0
		 *          for addressing the matches of most recent search. Phrases
		 *          "those buttons" and "the former" result in `index` being 1.
		 */
		index: number;

		/**
		 * Provides optionally found keyword. It might be empty if no keyword
		 * was included with the phrase matching the expression.
		 *
		 * @example In phrase "the latter buttons" the extracted word is
		 *          "buttons". In phrase "them" there is no word to extract,
		 *          thus `word` is undefined.
		 */
		word?: string;

		/**
		 * Describes cardinality type of addressed context.
		 *
		 * Cardinality is used to interpret indices into the history of tested
		 * nodes in different ways. The source cardinality is important for
		 * looking up history of previously searched elements focusing on those
		 * tagged with matching cardinality.
		 *
		 * @example The phrase "these 5 buttons" has "plural" source
		 *          cardinality. "it" has "singular" source cardinality. The
		 *          phrase "the 5th icon" is implying a set of icons to pick
		 *          from (when qualified as "the 5th of these icons"), thus its
		 *          source cardinality is "plural", too.
		 */
		sourceCardinality?: Cardinality;

		/**
		 * Describes cardinality type of selected part in addressed context.
		 *
		 * Cardinality is used to interpret indices into the history of tested
		 * nodes in different ways. The target cardinality becomes important on
		 * tagging context nodes when tracking them in a history of previous
		 * nodes.
		 *
		 * @example The phrase "these 5 buttons" and "it" select the whole
		 *          context, thus their target cardinality is identical to the
		 *          source cardinality. But a phrase like "the 5th icon" is
		 *          picking a single icon out of several ones, thus its target
		 *          cardinality is "singular" while its source cardinality is
		 *          "plural". That's because when tracking that 5th icon in
		 *          history, a follow-up reference phrase is expected to have
		 *          singular source cardinality.
		 */
		targetCardinality?: Cardinality;

		/**
		 * Optionally provides zero-based sub-index of element in a set of
		 * elements matching a previous search as addressed by other properties.
		 *
		 * @example The phrase "the fifth button" results in subIndex set to 4.
		 */
		subIndex?: number;
	}

	/**
	 * Implements development API exposed in context of TestCafe's test
	 * controller for interacting with contextual-gherkin in a step definition.
	 */
	class Api {
		/**
		 * Finds elements of named type anywhere on current page.
		 *
		 * @param typeName name of type all matching elements of current page have to share
		 * @param defaultSelector selector to use in case none has been configured for the given type
		 * @returns context wrapping matching elements of named type for further processing
		 */
		find( typeName: string, defaultSelector?: SimpleSelector ): Promise<Context>;

		/**
		 * Fetches previously tracked set of matches selected by provided
		 * context identifier.
		 *
		 * @param context identifies context to be fetched
		 * @returns selected context for processing
		 */
		getContextFor( context: ContextualWord ): Promise<Context>;
	}

	/**
	 * Describes a context node trackable in history of previous searches.
	 */
	class Context {
		/**
		 * Provides controller instance of TestCafé used to fetch matches of
		 * this node.
		 */
		testController: TestController;

		/**
		 * Refers to context this node's matches have been searched in.
		 */
		parent?: Context;

		/**
		 * Refers to instance of contextual-gherkin which has been used to get
		 * current context originally. It is usually injected into the test
		 * controller and fetched from there to gain access.
		 */
		api: Api;

		/**
		 * Describes matching elements in DOM of test browser serving as context
		 * for any follow-up action, inspection and query.
		 */
		matches: Selector;

		/**
		 * Lists IDs assigned to previously matching elements in DOM of test
		 * browser for tracking them in history as long as possible.
		 *
		 * These IDs are used to deal with cases where elements have been
		 * changed in browser thus failing to match recently used selector again.
		 */
		ids?: number[];

		/**
		 * Exposes singular form of type name of elements represented by current
		 * context.
		 */
		type: string;

		/**
		 * Provides selectors specific to current node.
		 *
		 * This is a thread of hierarchical selectors map found runtime
		 * configuration.
		 */
		selectors: SubSelectors;

		/**
		 * Represents assumed cardinality of current context. If "singular", the
		 * context is expected to match a single element (at least). If
		 * "plural", the context is expected to match multiple elements.
		 *
		 * This is expressing the expectation based on some query used to find
		 * the context's matching elements, only. It doesn't depend on actual
		 * number of matching elements.
		 *
		 * @see https://cepharum-foss.gitlab.io/contextual-gherkin/glossary.html#cardinality
		 */
		cardinality?: Cardinality;

		/**
		 * Searches document in browser for elements of selected type
		 * subordinated to matching elements of current context.
		 *
		 * @param typeOfElement type of elements to search
		 * @param defaultSelector selector to use in case none has been configured for the given type
		 * @returns another context representing all matching elements found as descendants of elements in current context
		 */
		find( typeOfElement: string, defaultSelector?: SimpleSelector ): Promise<Context>;

		/**
		 * Reduces current set of matching elements to the one at given index.
		 *
		 * @note The provided index can be nullish for convenient use in step
		 *       definitions. In that case, current context is returned without
		 *       applying any reduction.
		 *
		 * @param index non-nullish index into current context's set of matching elements to pick
		 * @returns another context representing that selected element, only, current context if index is nullish
		 */
		nth( index: number|null ): Promise<Context>;

		/**
		 * Reduces current context's set of matching elements to those matching
		 * another selector for given type of element.
		 *
		 * @note In opposition to filter(), this function isn't descending into
		 *       the hierarchy of elements but inspects currently matching
		 *       elements, only.
		 *
		 * @param typeOfElement type of element to reduce matches to
		 * @param defaultSelector selector to use if none has been configured for the named type of elements
		 * @returns another context representing only those matching elements of current context that match selector of given name, too
		 */
		filter( typeOfElement: string, defaultSelector?: SimpleSelector ): Promise<Context>;

		/**
		 * Reduces current context's set of matching elements to those marked as
		 * matching by a custom callback function.
		 *
		 * @param fn callback function to invoke on every matching element of current context
		 * @param closureScope set of simple values to be provided as closure scope to invoked callback
		 * @returns another context representing only those matching elements of current context that cause provided callback to return truthy
		 */
		filterFn( fn: SelectorFn, closureScope?: { [key: string]: any } ): Promise<Context>;

		/**
		 * Reduces current context's set of matching elements to those having
		 * subordinated elements of given type which are marked as matching by a
		 * custom callback function in addition.
		 *
		 * This is basically a convenient combination of filtering a given set
		 * of elements by looking up children first and then apply a filter
		 * function to those children while keep focusing on the elements
		 * matching current context
		 *
		 * @param typeOfSub type of element to search in context of either matching element in current context
		 * @param defaultSelector selector to use if none has been configured for the given type of element
		 * @param fn callback function to invoke on every matching element of current context
		 * @param closureScope set of simple values to be provided as closure scope to invoked callback
		 * @returns another context representing only those matching elements of current context that have children causing provided callback to return truthy
		 */
		filterBySubsWithFn( typeOfSub: string, defaultSelector: SimpleSelector, fn: SelectorFn, closureScope?: { [key: string]: any } ): Promise<Context>;

		/**
		 * Implements a convenient helper to filter current set of matching
		 * elements based on children of either matching element matching some
		 * given text partially or completely.
		 *
		 * @param typeOfSub type of element to find per matching element of current context
		 * @param defaultSelector selector to use if none has been configured for the given type of element
		 * @param text text to compare with normalized textual content of either found child
		 * @param partially if true, given text or regular expression is expected to be a part of a child's normalized textual content, only
		 * @returns another context limited to those matching elements of current context that have children with a given text
		 */
		filterBySubsWithText( typeOfSub: string, defaultSelector: SimpleSelector, text: string|RegExp, partially?: boolean ): Promise<Context>;

		/**
		 * Limits set of matching elements to those with a named attribute
		 * having a certain value.
		 *
		 * @param name name of attribute to read, might be mapped internally according to selectors configuration
		 * @param value value to expect in a matching element's attribute
		 * @returns another context with all those elements of current one which have a matching value in named attribute
		 */
		withAttribute( name: string, value: string|RegExp ): Promise<Context>;

		/**
		 * Limits set of matching elements to those with a named DOM property
		 * having a certain value.
		 *
		 * @param name name of DOM property to inspect, might be mapped internally according to selectors configuration
		 * @param value value to expect in a matching element's DOM property, use callback invoked with found value to return truthy on a match
		 * @returns another context with all those elements of current one which have a matching value in named DOM property
		 */
		withProperty( name: string, value: string|RegExp|SelectorFn ): Promise<Context>;

		/**
		 * Indicates if matches of context exist in browser.
		 *
		 * @return true if context has any matching elements currently existing the browser's document
		 */
		hasMatches(): Promise<boolean>;

		/**
		 * Tracks current context in history of contexts for later retrieval in
		 * a follow-up step.
		 *
		 * @param typeOfElement type of element this context should be tracked as, every type has its individual history
		 * @param cardinality assumed cardinality of current context, if given, fetching context works with same cardinality later, too
		 */
		track( typeOfElement?: string, cardinality?: Cardinality ): Promise<void>;

		/**
		 * Looks up runtime configuration for a selector matching provided
		 * name of element to use for searching such elements in current context.
		 *
		 * @param normalizedSingularTypeName type of element to query
		 * @param defaultSelector fallback selector to use if there is no matching configuration
		 * @returns found selector matching provided type of element or optionally provided fallback if missing in configuration
		 * @throws if fallback is missing and no selector has been found or if multiple selectors have been found in configuration
		 */
		getSelector(normalizedSingularTypeName: string, defaultSelector?: SimpleSelector|false ): Query|null;

		/**
		 * Conveniently checks if current set of matching elements is satisfying
		 * provided data extracted from a {cardinal-word} expression.
		 *
		 * @param cardinal data of an encountered {cardinal-word} expression
		 * @param track true [default] if current context should be tracked in history after passing check
		 * @returns when checks have passed and context has been optionally tracked
		 */
		checkCardinalWord(cardinal: NumberAndWord, track?: boolean ): Promise<void>;

		/**
		 * Conveniently asserts that some or all matching elements of current
		 * context are found as matching elements in another context that's
		 * derived from current one e.g. by filtering.
		 *
		 * On passing that assertion, the current context gets tracked in history
		 * on demand unless it hasn't been reduced after fetching it from
		 * history before based on provided `phrase`.
		 *
		 * @note This function is an actual convenience helper for specific use
		 *       cases, only. It might fail to properly assert in other cases.
		 *
		 * @param filteredContext another context of matching elements to compare with current one
		 * @param contextualWord parsed instance of `{contextual-word}` used to fetch current context from history
		 * @param requireAll if true [default], provided context has to list all elements of current context, otherwise it has to list at least one element
		 * @param track if true [default], this context is tracked if it has been reduced after fetching it from context due to provided `phrase`
		 * @returns when assertion has passed and context has been optionally tracked in history
		 */
		checkFilteredInContext( filteredContext: Context, contextualWord: ContextualWord, requireAll?: boolean, track?: boolean ): Promise<void>;
	}
}
