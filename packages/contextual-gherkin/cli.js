#!/usr/bin/env node

const { spawn } = require( "node:child_process" );
const File = require( "node:fs" );
const Path = require( "node:path" );
const os = require( "node:os" );

const semver = require( "semver" );
const { getShellConfigurationSync, quote } = require( "@cepharum/quoting-db" );

const cgFolder = "./node_modules/@cepharum/contextual-gherkin";
const isWindows = os.platform() === "win32";

( async() => {
	// make sure contextual gherkin is installed locally
	const depStat = await stat( cgFolder );

	if ( !depStat?.isDirectory() ) {
		console.error( "run\n\n    npm i -D @cepharum/contextual-gherkin" );
		process.exit( 1 );
	}


	// display some basic stats
	const cg = require( Path.resolve( cgFolder, "package.json" ) );
	console.log( `@cepharum/contextual-gherkin ${cg.version}` );


	// make sure all peer dependencies of contextual-gherkin have been installed
	// locally
	const required = cg.peerDependencies;

	if ( !required ) {
		console.error( "reading list of required peer dependencies has failed" );
		process.exit( 1 );
	}

	const meta = require( Path.resolve( process.cwd(), "package.json" ) );
	const deps = meta.dependencies || {};
	const devDeps = meta.devDependencies || {};
	const missing = [];

	for ( const name of Object.keys( required ) ) {
		const tracked = devDeps[name] || deps[name];
		const dep = require( Path.resolve( process.cwd(), "node_modules", name, "package.json" ) );

		if ( !tracked || !dep?.version || !semver.satisfies( dep.version, required[name] ) ) {
			missing.push( name + "@" + required[name] );
		}
	}

	if ( missing.length > 0 ) {
		console.error( "run\n\n    npm i -D " + missing.join( " " ) );
		process.exit( 1 );
	}


	// make sure some patterns for selecting step implementations and/or
	// features have been provided
	const patterns = [];
	const options = {};
	const passedOptions = [];
	let expectOptions = true;

	for ( let i = 2; i < process.argv.length; i++ ) {
		const arg = process.argv[i];

		if ( arg === "--" && expectOptions ) {
			expectOptions = false;
			continue;
		}

		if ( arg.startsWith( "-" ) ) {
			let [ option, ...parts ] = arg.split( "=" ); // eslint-disable-line prefer-const

			switch ( expectOptions ? option : null ) {
				case "-E" :
				case "--env" :
					if ( parts.length > 0 ) {
						console.error( `use separate argument to define environment variable with ${option}` );
						process.exit( 1 );
					}

					if ( ++i < process.argv.length ) {
						const value = process.argv[i].trim();
						const split = value.indexOf( "=" );

						if ( split < 0 ) {
							console.error( `missing value for defined environment variable ${value}` );
							process.exit( 1 );
						}

						( options.env ??= {} )[value.substring( 0, split )] = value.substring( split + 1 );
					} else {
						console.error( `found --${name} option without value` );
						process.exit( 1 );
					}

					break;

				case "--env-file" :
				case "--features" :
				case "--folder" :
				case "--steps" : {
					const name = option.slice( 2 );

					if ( parts.length > 0 ) {
						options[name] = parts.join( "=" ).trim();
					} else if ( ++i < process.argv.length ) {
						options[name] = process.argv[i].trim();
					} else {
						console.error( `found --${name} option without value` );
						process.exit( 1 );
					}
					break;
				}

				default :
					passedOptions.push( arg );
			}
		} else if ( Path.normalize( arg ).split( Path.sep ).some( segment => segment.startsWith( "*" ) ) ) {
			patterns.push( arg );
		} else {
			passedOptions.push( arg );
		}
	}

	if ( options.folder ) {
		patterns.unshift( Path.join( options.folder, options.steps ?? "steps", "**/*.ts" ) );
		patterns.unshift( Path.join( options.folder, options.steps ?? "steps", "**/*.js" ) );
		patterns.unshift( Path.join( options.folder, options.features ?? "features", "**/*.feature" ) );
	} else {
		if ( options.steps ) {
			patterns.unshift( Path.join( options.steps ?? "", "**/*.ts" ) );
			patterns.unshift( Path.join( options.steps ?? "", "**/*.js" ) );
		}

		if ( options.features ) {
			patterns.unshift( Path.join( options.features ?? "", "**/*.feature" ) );
		}
	}

	if ( !patterns.length ) {
		console.error( "provide patterns selecting your test files" );
		process.exit( 1 );
	}


	// pre-compile environment
	let env = {
		...process.env,
		...options.env
	};


	// read optionally selected environment file
	let content;

	try {
		content = ( await File.promises.readFile( options["env-file"], { encoding: "utf-8" } ) ).trim();
	} catch ( cause ) {
		if ( cause.code !== "ENOENT" && options["env-file"] != null ) {
			throw cause;
		}
	}

	if ( content ) {
		options["env-file"] = {};

		for ( const line of content.split( /\r?\n/ ) ) {
			const trimmed = line.trim();

			if ( trimmed === "" || trimmed[0] === "#" || trimmed[0] === ";" ) {
				continue;
			}

			let [ name, ...value ] = trimmed.split( "=" );

			name = name.trim();
			value = value.join( "=" ).trim();

			if ( value.startsWith( "'" ) && value.endsWith( "'" ) ) {
				value = value.substring( 1, value.length - 1 );
			} else {
				value = value.replace( /^"(.*)"$/, "$1" )
					.replace( /\$(?:\{\s*([a-z0-9_]+)\s*}|([a-z0-9_]+))/g, ( _, a, b ) => env[a ?? b] ?? "" );
			}

			env[name.trim()] = options["env-file"][name.trim()] = value;
		}
	}


	// recompile environment
	env = {
		...process.env,
		...options["env-file"],
		...options.env
	};

	const proxy = new Proxy( env, {
		get( target, p ) {
			if ( target.hasOwnProperty( p ) ) {
				return target[p];
			}

			for ( const [ key, value ] of Object.entries( target ) ) {
				if ( key.toLowerCase() === p.toLowerCase() ) {
					return value;
				}
			}

			return undefined;
		}
	} );


	// inspect local installation of npm to pick sanitizer for passed parameters
	let npm;

	for ( const path of proxy.PATH.split( Path.delimiter ) ) {
		const candidate = Path.resolve( path, isWindows ? "npm.cmd" : "npm" );
		const npmStat = await stat( candidate ); // eslint-disable-line no-await-in-loop

		if ( npmStat?.isFile() || npmStat?.isSymbolicLink() ) {
			npm = candidate;
			break;
		}
	}

	if ( npm == null ) {
		console.error( "npm not found" );
		process.exit( 1 );
	}


	// choose parameter sanitizer based on found npm's kind of file
	const spawnOptions = {};
	let sanitizer = v => v;

	if ( npm.endsWith( ".cmd" ) ) {
		spawnOptions.shell = true;
	} else {
		const file = await File.promises.readFile( npm, "utf8" );

		if ( file.startsWith( "#!/" ) ) {
			spawnOptions.shell = true;
		}
	}

	if ( spawnOptions.shell ) {
		const configuration = getShellConfigurationSync( spawnOptions.shell );

		sanitizer = argument => quote( argument, configuration );
	}


	// invoke gherkin-testcafe and provide all required information
	const child = spawn( "npm", [
		"exec",
		"gherkin-testcafe",
		"--",
		`${( env.E2E_BROWSER && sanitizer( env.E2E_BROWSER ) ) || ( isWindows ? "chrome" : "chromium:headless" )}`,
		`${cgFolder}/steps/**/*.js`,
		...patterns.map( sanitizer ),
		"--param-type-registry-file",
		`${cgFolder}/steps/types.js`,
		...passedOptions.map( sanitizer )
	], { stdio: "inherit", ...spawnOptions, env } );

	child.once( "error", console.error );
	child.once( "exit", process.exit );
} )();

async function stat( path ) { // eslint-disable-line require-jsdoc
	try {
		return await File.promises.stat( path );
	} catch ( cause ) {
		if ( cause.code === "ENOENT" ) {
			return null;
		}

		throw cause;
	}
}
