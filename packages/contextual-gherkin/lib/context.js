const { ClientFunction, Selector } = require( "gherkin-testcafe" );
const pluralize = require( "pluralize" );

/**
 * Assigns ID unique in context of current document to (first) element matching
 * provided selector.
 *
 * @type {ClientFunction<unknown, any[]>}
 * @returns {number} id of element
 */
const getID = ClientFunction( () => {
	// eslint-disable-next-line no-undef
	const el = selector();

	if ( !el ) {
		console.warn( `lost track of matching element #${index}/${numMatches} of type ${typeOfElement} prior to detaching it`, String( selector ) ); // eslint-disable-line no-undef
		return null;
	}

	if ( el.dataset.contextualGherkinId ) {
		return el.dataset.contextualGherkinId;
	}

	// eslint-disable-next-line no-undef
	const data = document.documentElement.dataset;
	const nextId = Number( data.contextualGherkinLastId || 0 ) + 1;

	data.contextualGherkinLastId = el.dataset.contextualGherkinId = nextId;

	return nextId;
} );

const normalizedString = value => String( value || "" ).trim().replace( /\s+/, " " );

/**
 * Represents single node available for tracking in history describing matches
 * in combination with the context and query used to find them.
 *
 * @type {Context}
 * @property {Context} parent refers to node representing context of this node's matches
 * @property {Api} api refers to API instance this node is managed with
 * @property {string} type names type of elements that has been search for
 * @property {Selector} matches describes matches of previous search
 * @property {number[]} ids list of IDs assigned to previously matching elements for tracking them in history as long as possible
 * @property {SubSelectors} selectors provides set of selectors to use instead of top-level selectors in configuration
 * @property {CardinalityValues?} cardinality indicates what number of elements has been expected in provided set of matches
 */
class Context {
	/**
	 * @param {TestController} testController test controller this context is used with
	 * @param {Context} parent context of current node in history
	 * @param {Api} api API instance exposing history to use
	 * @param {Selector} matches description of matches
	 * @param {string} type names type of elements provided as matches
	 * @param {SubSelectors} selectors selectors to use in follow-up queries via this node
	 * @param {number[]} ids list of IDs assigned to previously matching elements to keep individually track of them as long as possible
	 * @param {CardinalityValues?} cardinality expected number of elements in provided set of matches
	 */
	constructor( testController, parent, api, { matches, type, selectors, ids = null, cardinality = null } = {} ) {
		Object.defineProperties( this, {
			testController: { value: testController },
			parent: { value: parent },
			api: { value: api },
			matches: {
				value: matches ? matches.with( { boundTestRun: testController } ) : null,
				configurable: true
			},
			ids: { value: ids },
			type: { value: type },
			selectors: { value: selectors },
			cardinality: { value: cardinality },
		} );
	}

	/**
	 * Searches elements of named type subordinated to elements matching current
	 * context.
	 *
	 * @param {string} typeOfElement names type of elements to search
	 * @param {SimpleSelector} defaultSelector selector to use as fallback
	 * @returns {Promise<Context>} node describing found elements and their processing context
	 */
	find( typeOfElement, defaultSelector = undefined ) {
		const type = this.api.toNormalizedSingularName( typeOfElement );
		const { query, dependencies, selectors } = this.getSelector( type, defaultSelector ) || { query: defaultSelector };
		let matches;

		if ( this.parent && this.matches ) {
			matches = query === false ? this.matches : Selector( this.matches ).find( ...dependencies ? [ query, dependencies ] : [query] );
		} else if ( query === "" ) {
			throw new TypeError( "invalid use of empty-string selector for a global search" );
		} else {
			matches = Selector( query, dependencies ? { dependencies } : {} );
		}

		return Promise.resolve( new Context( this.testController, this, this.api, { matches, type, selectors } ) );
	}

	/**
	 * Limits current set of matches to the single element at given index.
	 *
	 * @param {number} index index of match
	 * @returns {Context} another context describing selected match, only
	 */
	nth( index ) {
		if ( !this.matches ) {
			throw new TypeError( "there are no matches to filter" );
		}

		if ( index == null ) {
			// convenience helper: no particular item of match has been selected -> deliver all
			return this;
		}

		if ( this.cardinality === "singular" ) {
			console.warn( "useless reduction with nth() in context of a single matching element" );
		}

		let matches;

		if ( this.ids?.length > 0 ) {
			if ( index >= this.ids.length ) {
				throw new TypeError( `index ${index} exceeds range of ${this.ids.length} element(s) matching previous search` );
			}

			matches = Selector( `[data-contextual-gherkin-id="${this.ids[index]}"]` );
		} else {
			matches = this.matches.nth( index );
		}

		return new this.constructor( this.testController, this, this.api, {
			matches,
			type: this.type,
			selectors: this.selectors,
			cardinality: "singular",
		} );
	}

	/**
	 * Limits current set of matches to those matching as a given type of
	 * elements, too.
	 *
	 * @param {string} typeOfElement names type of elements to focus on
	 * @param {SimpleSelector} defaultSelector selector to use as fallback
	 * @returns {Promise<Context>} node describing matches of filtering and their processing context
	 */
	filter( typeOfElement, defaultSelector = undefined ) {
		if ( !this.matches ) {
			throw new TypeError( "there are no matches to filter" );
		}

		const type = this.api.toNormalizedSingularName( typeOfElement );
		const { query, dependencies } = this.getSelector( type, defaultSelector );
		const matches = query === "" ? this.matches : Selector( this.matches ).filter( ...dependencies ? [ query, dependencies ] : [query] );

		return Promise.resolve( new this.constructor( this.testController, this, this.api, {
			matches,
			type: this.type,
			selectors: this.selectors,
			cardinality: this.cardinality
		} ) );
	}

	/**
	 * Limits current set of matches to those picked by a given callback
	 * function invoked in browser.
	 *
	 * @param {function(node: Element, index: number):boolean} fn callback invoked in context of browser
	 * @param {Object<string,any>} dependencies serializable data exposed in browser for use in callback
	 * @returns {Promise<Context>} node describing matches of filtering and their processing context
	 */
	filterFn( fn, dependencies = {} ) {
		if ( !this.matches ) {
			throw new TypeError( "there are no matches to filter" );
		}

		const matches = Selector( this.matches, { boundTestRun: this.testController } )
			.filter( fn, dependencies );

		return Promise.resolve( new this.constructor( this.testController, this, this.api, {
			matches,
			type: this.type,
			selectors: this.selectors,
			cardinality: this.cardinality,
		} ) );
	}

	/**
	 * Limits current set of matches to those matching a given type's query and
	 * picked by a given callback function invoked in browser investigating
	 * either match.
	 *
	 * @param {string} typeOfSub names type of elements to focus on
	 * @param {SimpleSelector} defaultSelector selector to use as fallback on searching sub-elements as candidates
	 * @param {function(node: Element, index: number):boolean} fn callback invoked in context of browser
	 * @param {Object<string,any>} context serializable data provided as context (via `this`) of invoked callback
	 * @returns {Promise<Context>} node describing matches of filtering and their processing context
	 */
	filterBySubsWithFn( typeOfSub, defaultSelector, fn, context = {} ) {
		const { query, dependencies } = this.getSelector( typeOfSub, defaultSelector ) || { query: false };

		return this.filterFn( ( node, index ) => {
			let subs;

			if ( typeof query === "function" ) {
				subs = query( node, index );
			} else if ( query === false ) {
				// there is no query addressing any subordinated element(s)
				// -> inspect the originally matching node itself
				subs = [node];
			} else {
				subs = node.querySelectorAll( query );
			}

			if ( subs?.length == null ) {
				subs = subs ? [subs] : [];
			}

			for ( let i = 0, count = subs.length; i < count; i++ ) {
				if ( fn( subs[i], index ) ) { // eslint-disable-line no-undef
					return true;
				}
			}

			return false;
		}, { context, query, fn: ClientFunction( fn, { dependencies: { ...dependencies, ...context } } ) } );
	}

	/**
	 * Limits current set of matching element to those having a subordinated
	 * element containing provided text.
	 *
	 * @param {string} typeOfSub names type of elements to focus on
	 * @param {SimpleSelector} defaultSelector selector to use as fallback on searching sub-elements as candidates
	 * @param {string|RegExp} text text to be contained in either selected sub-element for matching
	 * @param {boolean} partially if true, provided text or regular expression does not have to match whole textual content of matching subs
	 * @returns {Promise<Context>} promises context describing matches reduced to those having subs with given text
	 */
	filterBySubsWithText( typeOfSub, defaultSelector, text, partially = false ) {
		if ( typeof text === "string" ) {
			text = normalizedString( text );
		}

		const ___text = text;
		const ___partially = partially;
		const ___normalize = normalizedString;

		return this.filterBySubsWithFn( typeOfSub, defaultSelector, node => {
			const data = ___normalize( node.nodeType === Node.ATTRIBUTE_NODE ? node.nodeValue : node.textContent );

			if ( ___partially ) {
				return ___text instanceof RegExp ? ___text.test( data ) : data.indexOf( ___text ) > -1;
			}

			return ___text instanceof RegExp ? ( ___text.exec( data ) || [] )[0] === ( data ?? "" ) : data === ___text;
		}, { ___text , ___partially, ___normalize } );
	}

	/**
	 * Limits current set of matches to those having an attribute with given
	 * value.
	 *
	 * @note This function is looking for elements subordinated to current
	 *       node's matches, only.
	 *
	 * @param {string|RegExp} name name of attribute to look up per matching element
	 * @param {string|RegExp|function(any):boolean} value value expected in named attribute of either element, callback for custom test of attribute value
	 * @returns {Promise<Context>} promises another node listing those matches of current node matching named attribute
	 */
	async withAttribute( name, value ) {
		if ( ! await this.matches.count ) {
			throw new TypeError( "there are no matches to filter" );
		}

		if ( typeof value !== "function" && !( value instanceof RegExp ) ) {
			value = normalizedString( value );
		}

		const { query: nameQuery } = this.getSelector( "@" + name, false ) ?? {};
		const ___name = nameQuery ?? name;
		const ___value = value;
		const ___normalize = normalizedString;

		return this.filterBySubsWithFn( "$" + name, false, node => {
			const expected = ___value;
			const attribute = node.attributes.getNamedItem( ___name );
			const actual = attribute == null ? undefined : attribute.value;

			if ( typeof expected === "function" ) {
				return Boolean( expected( typeof actual === "string" ? ___normalize( actual ) : actual ) );
			}

			if ( expected instanceof RegExp ) {
				return Boolean( expected.test( ___normalize( actual ?? "" ) ) );
			}

			return expected === ___normalize( actual ?? "" );
		}, { ___name, ___value, ___normalize } );
	}

	/**
	 * Limits current set of matches to those having a DOM property with given
	 * value.
	 *
	 * @note This function is looking for elements subordinated to current
	 *       node's matches, only.
	 *
	 * @param {string} name name of DOM property to look up per matching element
	 * @param {string|RegExp|function(any):boolean} value value expected in named DOM property of either element, callback for custom test of property value
	 * @returns {Promise<Context>} promises another node listing those matches of current node matching named property
	 */
	async withProperty( name, value ) {
		if ( ! await this.matches.count ) {
			throw new TypeError( "there are no matches to filter" );
		}

		if ( value === "string" ) {
			value = normalizedString( value );
		}

		const { query: nameQuery } = this.getSelector( "#" + name, false ) ?? {};
		const ___name = nameQuery ?? name;
		const ___value = value;
		const ___normalize = normalizedString;

		return this.filterBySubsWithFn( "$" + name, false, node => {
			const expected = ___value;
			const actual = node[___name];

			if ( typeof expected === "function" ) {
				return Boolean( expected( typeof expected === "string" ? ___normalize( actual ) : actual ) );
			}

			if ( expected instanceof RegExp ) {
				return Boolean( expected.test( ___normalize( actual ) ) );
			}

			return expected === ( typeof expected === "string" ? ___normalize( actual ) : actual );
		}, { ___name, ___value, ___normalize } );
	}

	/**
	 * Indicates if node's search yielded any match.
	 *
	 * @returns {Promise<boolean>} promises true if there is some element matching previous search
	 */
	async hasMatches() {
		if ( this.matches ) {
			return await this.matches.exists;
		}

		return false;
	}

	/**
	 * Conveniently asserts that current set of matches satisfies a required
	 * number and tracks the result accordingly.
	 *
	 * @param {NumberAndWord} cardinal describes a combination of a cardinal number and a word
	 * @param {boolean} track if true, track current set of matching elements in history
	 * @param {string} message custom suffix to the error message on mismatch
	 * @returns {Promise<void>} promises assertions passed and result being tracked
	 */
	async checkCardinalWord( cardinal, track = true, message = "" ) {
		const matchCount = await this.matches.count;
		const { word = "element", number, mode } = cardinal;
		const name = number === 1 ? word : pluralize.plural( word );

		switch ( mode ) {
			case ">=" :
				await this.testController.expect( matchCount ).gte( number, `expecting at least ${number} ${name} ${message}`.trim() );
				break;

			case "<=" :
				await this.testController.expect( matchCount ).lte( number, `expecting at most ${number} ${name} ${message}`.trim() );
				break;

			case "==" :
			default :
				await this.testController.expect( matchCount ).eql( number, `expecting exactly ${number} ${name} ${message}`.trim() );
				break;
		}

		if ( track && matchCount > 0 ) {
			if ( cardinal.cardinality === "singular" && matchCount > 1 ) {
				await ( await this.nth( 0 ) ).track( word, cardinal.cardinality );
			} else {
				await this.track( word, cardinal.cardinality );
			}
		}
	}

	/**
	 * Conveniently asserts that some or all matching elements of current node
	 * exist in a subset of those derived by filtering and tracks the current
	 * set in history if it is a refined version of some context fetched from
	 * history.
	 *
	 * @param {Context} filteredContext a subset of current context's matching elements
	 * @param {ContextualWord} contextualWord describes a combination of a cardinal number and a word
	 * @param {boolean} requireAll if true, all elements of current node must exist in filtered subset
	 * @param {boolean} track if true, track current set of matching elements in history
	 * @returns {Promise<void>} promises assertions passed and current node being tracked
	 */
	async checkFilteredInContext( filteredContext, contextualWord, requireAll = true, track = true ) {
		let iter;

		for ( iter = filteredContext; iter && iter !== this; iter = iter.parent ) {} // eslint-disable-line no-empty

		if ( !iter ) {
			throw new TypeError( "provided context has not been derived from current one" );
		}

		const matchCount = await this.matches.count;

		if ( requireAll ) {
			await this.testController.expect( filteredContext.matches.count ).eql( matchCount );
		} else {
			await this.testController.expect( filteredContext.matches.count ).gt( 0 );
		}

		if ( track && contextualWord.isRefining && matchCount > 0 ) {
			await this.track( contextualWord.word, contextualWord.targetCardinality );
		}
	}

	/**
	 * Detaches current set of matching elements to individually identify them
	 * even after DOM has changed.
	 *
	 * @note An exception is thrown on trying to detach an empty set of matches.
	 *
	 * @returns {Promise<number[]>} list of unique IDs assigned to matching elements for tracking
	 */
	async detach() {
		if ( this.matches.__contextualGherkinDetached ) {
			return this.matches.__contextualGherkinDetached;
		}

		const numMatches = await this.matches.count;

		if ( numMatches > 0 ) {
			// haven't been detached before -> detach now
			const ids = ( await Promise.all( new Array( numMatches ).fill( 0 ).map( ( _, index ) => {
				const selector = this.matches.nth( index );

				return getID.with( {
					boundTestRun: this.testController,
					dependencies: {
						selector,
						typeOfElement: this.type,
						index,
						numMatches
					}
				} )();
			} ) ) )
				.filter( ( id, index ) => {
					if ( !( id > 0 ) ) {
						console.warn( `lost track of matching element #${index}/${numMatches} of type ${this.type} prior to detaching it`, String( selector ) ); // eslint-disable-line max-len
					}

					return id > 0;
				} );

			const selector = Selector( ids.map( id => `[data-contextual-gherkin-id="${id}"]` ).join( "," ) );

			selector.__contextualGherkinDetached = ids;

			Object.defineProperty( this, "matches", {
				value: selector,
				configurable: false,
			} );

			return ids;
		}

		throw new TypeError( `there are no matches of type ${this.type} to be detached` );
	}

	/**
	 * Adds current context node to history .
	 *
	 * @param {string} typeOfElement names type of element to be tracked
	 * @param {Cardinality} cardinality indicates number of elements in current match expected by caller
	 * @returns {Promise<void>} promises item tracked
	 */
	async track( typeOfElement = undefined, cardinality = null ) {
		const { api } = this;
		const ids = await this.detach( true );

		// track copy of current context with detached selector
		const copy = new this.constructor( this.testController, this.parent, this.api, {
			matches: this.matches,
			type: this.type,
			selectors: this.selectors,
			ids,
			cardinality,
		} );

		const aliases = api.getNormalizedAliases( typeOfElement, this.type );
		let hadCommonType = false;

		for ( const alias of aliases ) {
			if ( alias ) {
				( api.history[alias] ??= [] ).unshift( copy );
			}

			hadCommonType |= alias === api.commonType;
		}

		if ( !hadCommonType ) {
			( api.history[api.commonType] ??= [] ).unshift( copy );
		}


		// log in browser for debugging purposes
		await ClientFunction( () => {
			console.debug(
				`[contextual-gherkin] tracked ${cardinality ?? ""} ${typeOfElement} as ${alias}:`, // eslint-disable-line no-undef
				document.querySelectorAll( ids // eslint-disable-line no-undef
					.map( id => `[data-contextual-gherkin-id="${id}"]` )
					.join( "," )
				)
			);
		}, {
			boundTestRun: this.testController,
			dependencies: {
				typeOfElement: typeOfElement || this.type,
				cardinality,
				ids,
				alias: aliases[0],
			},
		} )();
	}

	/**
	 * Fetches query for looking up a given type of elements in context of
	 * current node.
	 *
	 * @param {string} normalizedSingularTypeName names type of elements to be queried
	 * @param {SimpleSelector|false} [defaultSelector] query to return as fallback, if false, return nullish if no query was found
	 * @returns {Query|null} query for looking up named type of elements, nullish if query is missing and defaultQuery is false
	 * @throws TypeError if multiple queries have been found or if no query has been found and defaultQuery is omitted
	 * @protected
	 */
	getSelector( normalizedSingularTypeName, defaultSelector = undefined ) {
		const { api } = this;
		const aliases = api.getAliases( normalizedSingularTypeName );
		const pools = [];

		for ( let level = this; level; level = level.parent ) { // eslint-disable-line consistent-this
			if ( level.selectors && level.selectors !== api.config.selectors ) {
				pools.push( level.selectors );
			}
		}

		pools.push( api.config.selectors );

		let query;

		for ( const pool of pools ) {
			for ( const alias of aliases ) {
				if ( pool[alias] != null ) {
					if ( query == null ) {
						query = pool[alias];
					} else {
						throw new TypeError( `found multiple selectors for querying "${normalizedSingularTypeName}"` );
					}
				}
			}

			if ( query != null ) {
				break;
			}
		}

		if ( query == null ) {
			if ( defaultSelector === false ) {
				// silently report back that no query has been configured for this type
				return null;
			}

			if ( defaultSelector != null ) {
				query = defaultSelector;
			}
		}

		if ( query != null ) {
			if ( typeof query === "object" ) {
				if ( Array.isArray( query ) ) {
					return {
						query: query[0],
						dependencies: query[1] || {},
						selectors: this.selectors,
					};
				}

				if ( query[""] == null ) {
					throw new TypeError( `missing selector for querying "${normalizedSingularTypeName}" itself` );
				}

				if ( query[""] === false ) {
					return {
						query: false,
						selectors: this.selectors,
					};
				}

				if ( Array.isArray( query[""] ) ) {
					return {
						query: query[""][0],
						dependencies: query[""][1],
						selectors: query,
					};
				}

				return {
					query: query[""],
					selectors: query,
				};
			}

			return { query, selectors: this.selectors };
		}

		throw new TypeError( `missing selector for querying "${normalizedSingularTypeName}"` );
	}
}

module.exports.Context = Context;
