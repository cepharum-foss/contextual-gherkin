const pluralize = require( "pluralize" );

const { Context } = require( "./context" );

/**
 * Implements API of contextual Gherkin with support for finding elements,
 * tracking them and reading them to find elements on context of those.
 *
 * @type {Api}
 * @property {TestController} testController test controller this API has been set up for
 * @property {Object<string,Context[]>} history lists previously looked up elements of application grouped by type of element, sorted from newest to oldest
 * @property {Configuration} config customizations for integrating with tested application
 * @property {string} commonType names common type of all elements that can be found and tracked
 */
class Api {
	/**
	 * @param {TestController} testController test controller this API is used with
	 * @param {Configuration} config runtime configuration for customizing the API instance
	 */
	constructor( testController, config ) {
		this.testController = testController;
		this.config = this.normalizeConfiguration( config );
		this.history = {};
		this.commonType = "*";
	}

	/**
	 * Gains access on this API in context of provided test controller.
	 *
	 * @param {TestController} testController current test controller
	 * @returns {Api} API fetched from provided test controller
	 */
	static access( testController ) {
		const { __contextualGherkinLibrary: lib } = testController.ctx;

		if ( !lib ) {
			throw new Error( "missing proper integration with contextual-gherkin library ... see https://cepharum-foss.gitlab.io/contextual-gherkin/api/configuration.html" );
		}

		return lib;
	}

	/**
	 * Searches document for elements matching a configured selector.
	 *
	 * @param {string} typeOfElement names type of element to be looked up
	 * @param {SimpleSelector} defaultSelector selector to use as fallback
	 * @returns {Promise<Context>} promises node describing performed search, its context and any yielded matches
	 * @throws Error if no or multiple selectors have been found for named type and no default has been provided
	 */
	find( typeOfElement, defaultSelector = undefined ) {
		return new Context( this.testController, null, this ).find( typeOfElement, defaultSelector );
	}

	/**
	 * Looks up history of previously found and tracked elements.
	 *
	 * @param {string} typeOfElement names of type of element to look up
	 * @param {string|number} index provided index of match to fetch from type's queue of previous matches
	 * @param {Cardinality} cardinality selects expected number of elements in match to find
	 * @returns {Context} node describing found previous match of elements of named type
	 */
	async get( typeOfElement, index, cardinality = null ) {
		const alias = this.getNormalizedAliases( typeOfElement )[0];
		const queue = this.history[alias] || [];
		const numTracks = queue.length;
		const numericIndex = this.parseIndex( index, queue.length );
		const numberLabel = cardinality === "singular" ? "sole " : cardinality === "plural" ? "set of " : "";
		const wrongCardinality = [];

		// search history for previously addresses element matching by cardinality, too
		for ( let i = 0, wanted = numericIndex; i < numTracks; i++ ) {
			const track = queue[i];

			if ( cardinality != null && track.cardinality !== cardinality ) {
				wrongCardinality.push( cardinality );
				continue;
			}

			if ( wanted-- > 0 ) {
				continue;
			}

			if ( await track.matches.count < 1 ) { // eslint-disable-line no-await-in-loop
				throw new Error( `previous selection of "${alias}" (queried as ${numberLabel}"${typeOfElement}") at index ${index} was not matching anything or all matches have gone` ); // eslint-disable-line max-len
			}

			return track;
		}

		if ( wrongCardinality.length > 0 ) {
			const error = new Error( `history of searched elements lacks record tracking ${numberLabel}"${alias}" (queried as ${numberLabel}"${typeOfElement}") at index ${index}, but has record with mismatching cardinality` ); // eslint-disable-line max-len

			if ( wrongCardinality.length === queue.length && wrongCardinality.every( c => c === "plural" ) && queue.every( t => t.cardinality === "singular" ) ) {
				error.cardinalityMismatch = [...queue].reverse();
			}

			throw error;
		}

		throw new Error( `history of searched elements lacks record tracking ${numberLabel}"${alias}" (queried as ${numberLabel}"${typeOfElement}") at index ${index}` ); // eslint-disable-line max-len
	}

	/**
	 * Conveniently recovers context based on provided description.
	 *
	 * @param {ContextualWord} context describes context to recover
	 * @returns {Promise<Context>} promises tracked node describing result of a previous search
	 */
	async getContextFor( context ) {
		try {
			const recovered = await this.get( context.word, context.index, context.sourceCardinality );

			return recovered.nth( context.subIndex );
		} catch ( cause ) {
			if ( cause.cardinalityMismatch ) {
				if ( context.subIndex < cause.cardinalityMismatch.length ) {
					return cause.cardinalityMismatch[context.subIndex];
				}
			}

			throw cause;
		}
	}

	/**
	 * Normalizes configuration provided user to be safe for processing
	 * internally.
	 *
	 * @param {Configuration} config configuration to normalize
	 * @returns {Configuration} normalized configuration
	 */
	normalizeConfiguration( config ) {
		const normalized = { ...config };

		// convert all sets of aliases to lowercase and sort either one alphabetically
		if ( normalized.aliases && typeof normalized.aliases === "object" ) {
			if ( Array.isArray( normalized.aliases ) ) {
				throw new TypeError( "syntax for defining aliases has changed, see https://cepharum-foss.gitlab.io/contextual-gherkin/api/configuration.html#aliases" );
			}

			const aliases = {};

			for ( const alias of Object.keys( normalized.aliases ) ) {
				let names = normalized.aliases[alias];

				if ( !Array.isArray( names ) ) {
					names = names ? [names] : [];
				}

				aliases[this.normalizeName( alias )] = Object.freeze( names
					.map( name => this.normalizeName( name ) )
					.filter( i => i )
				);
			}

			normalized.aliases = aliases;
		} else {
			normalized.aliases = {};
		}

		Object.freeze( normalized.aliases );

		// normalize selectors tree
		normalized.selectors = Object.freeze( this.normalizeSelectors( normalized.selectors || {} ) );

		return normalized;
	}

	/**
	 * Recursively normalizes selectors provided as part of configuration.
	 *
	 * @param {any} selectors description of one or more selectors to be normalized
	 * @param {number} depth tracks depth of current level in a hierarchy of selectors
	 * @returns {Selectors} normalized selectors
	 */
	normalizeSelectors( selectors, depth = 0 ) {
		switch ( typeof selectors ) {
			case "function" :
			case "string" :
			case "boolean" :
				if ( !depth ) {
					throw new TypeError( `invalid selectors configuration` );
				}

				if ( selectors === true ) {
					throw new TypeError( `invalid boolean true in selectors` );
				}

				if ( selectors === false && depth < 2 ) {
					throw new TypeError( `invalid boolean false in a top-level selector` );
				}

				return selectors;

			case "object" : {
				if ( selectors == null ) {
					return depth > 0 ? null : {};
				}

				if ( Array.isArray( selectors ) ) {
					if ( selectors.length === 2 ) {
						const [ fn, deps ] = selectors;

						if ( typeof fn == "function" && deps && typeof deps === "object" && !Array.isArray( deps ) ) {
							return selectors;
						}
					}

					throw new TypeError( "invalid list of selectors" );
				}

				const normalized = {};

				for ( const name of Object.keys( selectors ) ) {
					const value = this.normalizeSelectors( selectors[name], depth + 1 );

					if ( value != null ) {
						const key = this.normalizeName( name );

						if ( normalized[key] != null ) {
							console.warn( `ambiguous selector for ${name} found` );
						}

						normalized[key] = value;
					}
				}

				if ( depth > 0 && normalized[""] == null ) {
					console.warn( `found selector with subordinated selectors, only` );
				}

				return Object.freeze( normalized );
			}

			default :
				throw new TypeError( `invalid ${typeof selectors} in selectors` );
		}
	}

	/**
	 * Normalizes provided name of an element's type.
	 *
	 * @param {string} typeName type's name assumed to be in plural form
	 * @returns {string} normalized form of provided name
	 */
	normalizeName( typeName ) {
		return String( typeName || "" ).trim().replace( /\s+/g, " " ).toLocaleLowerCase();
	}

	/**
	 * Converts provided name of an element's type into its singular variant.
	 *
	 * @param {string} name type's name assumed to be optionally in plural form (might be singular form, too)
	 * @returns {string} singular form of provided name
	 */
	singularize( name ) {
		return pluralize.singular( String( name || "" ) );
	}

	/**
	 * Gets all aliases of provided type name.
	 *
	 * @param {string} singularName singular type name to look up case-insensitively
	 * @returns {string[]} list of aliases including provided name in alphabetically ascending order
	 */
	getAliases( singularName ) {
		const defined = this.config.aliases;
		const lookup = this.normalizeName( singularName );

		const populate = ( pool, alias ) => {
			pool[alias] = true; // eslint-disable-line no-param-reassign

			for ( const candidate of Object.keys( defined ) ) {
				if ( defined[candidate].includes( alias ) && !pool[candidate] ) {
					populate( pool, candidate );
				}
			}

			return pool;
		};

		if ( lookup ) {
			return Object.keys( populate( {}, lookup ) )
				// sort all found aliases to make sure always same one is
				// considered "primary" alias used for tracking elements
				.sort( ( l, r ) => l.localeCompare( r ) );
		}

		return [];
	}

	/**
	 * Converts named type of elements as provided by a caller into its
	 * normalized singular form.
	 *
	 * @param {string} name named type of elements
	 * @param {string} fallback name of type to use if normalizing provided name is falsy
	 * @returns {string} normalized singular form of provided name
	 */
	toNormalizedSingularName( name, fallback = undefined ) {
		return ( name == null ? null : this.singularize( this.normalizeName( name ) ) ) || fallback || this.commonType;
	}

	/**
	 * Fetches all normalized aliases for named type of element.
	 *
	 * @param {string} name names type of elements
	 * @param {string} fallbackType name of type to use as fallback
	 * @returns {string[]} aliases of named type of elements
	 */
	getNormalizedAliases( name, fallbackType = undefined ) {
		return this.getAliases( this.toNormalizedSingularName( name, fallbackType ) );
	}

	/**
	 * Normalizes provided sort-of-index into list of items into actual index of
	 * an item in that list.
	 *
	 * @param {string|number} index index or index-like value selecting item of a list
	 * @param {number} count number of items existing in that list
	 * @returns {number} parsed numeric index
	 */
	parseIndex( index, count ) {
		let numeric;

		if ( /^\s*\d+\s*$/.test( index ) ) {
			numeric = parseInt( index, 10 );
		} else {
			const normalized = this.normalizeName( index );

			switch ( normalized ) {
				case "it" :
				case "this" :
				case "these" :
				case "latter" :
				case "the latter" :
					numeric = 0;
					break;

				case "that" :
				case "those" :
				case "former" :
				case "the former" :
					numeric = 1;
					break;
			}
		}

		if ( numeric != null ) {
			if ( numeric < count ) {
				return numeric;
			}

			throw new Error( `insufficient previous searches for addressing matches at history #${index}` );
		}

		throw new Error( `invalid index of previous match: ${index}` );
	}
}

module.exports.Api = Api;
