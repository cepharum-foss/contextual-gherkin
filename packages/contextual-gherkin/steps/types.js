/**
 * @note This file isn't actually implementing any steps but resides in this
 *       folder to simplify this package's integration with an application's
 *       E2E testing.
 */

/* eslint-disable max-len */

const { ParameterTypeRegistry, ParameterType } = require( "@cucumber/cucumber-expressions" );
const pluralize = require( "pluralize" );

// Declare this mock function used below so that certain plugins for code
// assisting Gherkin syntax are capable of discovering those definitions.
const defineParameterType = definition => definition;


const registry = new ParameterTypeRegistry();

/**
 * Represents a combination of a number and a word usually parsed from an
 * according cucumber expression such as {cardinal-word} or {ordinal-word}.
 *
 * @property {string} word names type of elements gathered by that previous search, nullish if no word was given
 * @property {CardinalityValues} cardinality provides assumed number of elements processed phrase is trying to identify
 * @property {AmountModeValues} mode selects comparison mode to use
 */
class NumberAndWord {
	/**
	 * @param {number} number some numeric value
	 * @param {string} word some string
	 * @param {AmountModeValues} mode selects comparison mode to use
	 * @param {CardinalityValues} cardinality selects cardinality of expression
	 */
	constructor( number, word, mode = "==", cardinality = null ) {
		this.number = number;
		this.word = word;
		this.mode = mode;
		this.cardinality = cardinality;
	}
}

/**
 * Names an entity with optional context like its position in a set of likewise
 * entities.
 *
 * @property {number} index index into history of previous searches, 0 is for most recent search, -1 is for the search preceding most recent one, etc.
 * @property {string} word names type of elements gathered by that previous search, nullish if no word was given
 * @property {number} subIndex picks one out of multiple matches in selected previous search, if negative, counts from end of list, if nullish, no particular element of that match is picked
 * @property {boolean} isRefining if true, the represented phrase is identifying a reduced set of matches in context of a larger set
 * @property {CardinalityValues} sourceCardinality provides number of elements assumed in source of context (e.g. "icons" in "the fifth of these icons")
 * @property {CardinalityValues} targetCardinality provides number of elements assumed in refined target of context (e.g. "the fifth" in "the fifth of these icons")
 */
class ContextualWord {
	/**
	 * @param {number} index index into history of previous searches, 0 is for most recent search, -1 is for the search preceding most recent one, etc.
	 * @param {string} word names type of elements gathered by that previous search, nullish if no word was given
	 * @param {boolean} plural if true, matched phrase is addressing multiple elements matched in a previous search, otherwise it's referring to a single element matched before
	 * @param {number} subIndex picks one out of multiple matches in selected previous search, if negative, counts from end of list, if nullish, no particular element of that match is picked
	 * @param {boolean} isRefining if true, the phrase is identifying a reduced set of matches in context of a larger set
	 */
	constructor( index, word, plural, subIndex, isRefining ) {
		this.index = index;
		this.word = word;
		this.subIndex = subIndex;
		this.isRefining = isRefining;
		this.sourceCardinality = isRefining ? "plural" : plural === true ? "plural" : plural === false ? "singular" : null;
		this.targetCardinality = isRefining ? "singular" : this.sourceCardinality;
	}

	/**
	 * Provides cardinal word describing item eventually selected by this
	 * contextual word.
	 *
	 * @param {number} amount explicit number of items to describe in resulting cardinal word
	 * @returns {NumberAndWord} cardinal word addressed by current contextual word
	 */
	asCardinalWord( amount = undefined ) {
		const mode = this.targetCardinality === "singular" || this.sourceCardinality === "singular" ? "==" : ">=";

		return new NumberAndWord(
			this.targetCardinality === "singular" ? 1 : amount ?? 1,
			this.word, mode, this.targetCardinality );
	}
}

const Cardinals = {
	a: null,
	an: null,
	the: 1,
	no: 0,
	zero: 0,
	one: 1,
	two: 2,
	three: 3,
	four: 4,
	five: 5,
	six: 6,
	seven: 7,
	eight: 8,
	nine: 9,
	ten: 10,
};

const Ordinals = {
	first: 1,
	second: 2,
	third: 3,
	fourth: 4,
	fifth: 5,
	sixth: 6,
	seventh: 7,
	eighth: 8,
	ninth: 9,
	tenth: 10,
	"1st": 1,
	"2nd": 2,
	"3rd": 3,
	"4th": 4,
	"5th": 5,
	"6th": 6,
	"7th": 7,
	"8th": 8,
	"9th": 9,
	"10th": 10,
};

const isPlural = w => ( pluralize.isPlural( w ) ? true : pluralize.isSingular( w ) ? false : null );

const customTypes = [
	/**
	 * {ordinal-word} - Matches combination of an ordinal with a {word}.
	 *
	 * Ordinals are
	 *
	 * - positive integer followed by full stop or
	 * - ordinal numerals from first to tenth or
	 * - ordinal numerals from 1st to 10th
	 */
	defineParameterType( {
		name: "ordinal-word",
		regexp: /([1-9]\d*\.|first|second|third|fourth|fifth|sixth|seventh|eighth|ninth|tenth|1st|2nd|3rd|4th|5th|6th|7th|8th|9th|10th)\s+([^"\s]+|"[^"]*")/,
		type: NumberAndWord,
		transformer: ( index, word ) => {
			const _word = word[0] === '"' ? word.slice( 1, -1 ) : word;
			const plural = isPlural( _word );

			if ( plural ) {
				throw new TypeError( `invalid ordinal expression with a plural form "${word}"` );
			}

			return new NumberAndWord( ( Ordinals[index] ?? parseInt( index, 10 ) ) - 1, _word, "==", "singular" );
		},
	} ),

	/**
	 * {contextual-word} - Matches common contextual references usually
	 *                     combining a referential phrase with a {word}.
	 *
	 * Examples are
	 *
	 * - it
	 * - them
	 * - the former
	 * - the latter
	 * - the {word}
	 * - this {word}
	 * - that {word}
	 * - these {word}
	 * - those {word}
	 * - the former {word}
	 * - the latter {word}
	 * - the first {word}
	 * - the 6th {word}
	 * - the 20. {word}
	 * - the ninth of these {word}
	 * - the fourth of those {word}
	 * - the 8th of the former {word}
	 * - the 1. of the latter {word}
	 *
	 * @note {word} must not be "the", "it", "former" or "latter".
	 *
	 * smoke tested with https://regex101.com/
	 */
	defineParameterType( {
		name: "contextual-word",
		regexp: /(it|them|they)\b|(?:the(?:\s+([1-9]\d*\.|first|second|third|fourth|fifth|sixth|seventh|eighth|ninth|tenth|1st|2nd|3rd|4th|5th|6th|7th|8th|9th|10th|last|former|latter)(?:\s+of\s+(these|those|the\s+(?:former|latter)))?)?|(this|these|that|those))\s+((?!it\b|of\b|former\b|latter\b)[^"\s]+|"[^"]*")|the\s+(former|latter)\b/,
		type: ContextualWord,
		transformer: ( itRef, ordinal, ordinalThisThat, thisThat, word, formerLatter ) => {
			if ( itRef ) {
				return new ContextualWord( 0, undefined, itRef !== "it", undefined, false );
			}

			if ( formerLatter ) {
				return new ContextualWord( formerLatter === "former" ? 1 : 0, undefined, undefined, undefined, false );
			}

			const originalWord = word[0] === '"' ? word.slice( 1, -1 ) : word;
			let plural = isPlural( originalWord );
			const normalizedWord = plural ? pluralize.singular( originalWord ) : originalWord;

			if ( thisThat != null ) {
				if ( plural != null && plural !== [ "these", "those" ].includes( thisThat ) ) {
					console.warn( `ambiguous cardinality expressed in phrase "${thisThat} ${originalWord}"` );

					plural = undefined;
				}

				return new ContextualWord( [ "this", "these" ].includes( thisThat ) ? 0 : 1, normalizedWord, plural, undefined, false );
			}

			if ( ordinal != null ) {
				if ( ordinal === "former" || ordinal === "latter" ) {
					if ( ordinalThisThat != null ) {
						throw new TypeError( "invalid combination of former/latter with scoping reference" );
					}

					return new ContextualWord( ordinal === "former" ? 1 : 0, normalizedWord, plural, undefined, false );
				}

				const subIndex = ( Ordinals[ordinal] ?? parseInt( ordinal, 10 ) ) - 1;
				let index;

				switch ( ordinalThisThat ?? null ) {
					case "those" :
					case "the former" :
						index = 1;
						break;

					case "these" :
					case "the latter" :
					case null :
						index = 0;
						break;
				}

				return new ContextualWord( index, normalizedWord, plural, subIndex, true );
			}

			return new ContextualWord( 0, normalizedWord, plural, undefined, false );
		},
	} ),

	/**
	 * {cardinal-word} - Matches combination of a {count} with a {word}.
	 */
	defineParameterType( {
		name: "cardinal-word",
		regexp: /(?:(at\s+least|at\s+most|up\s+to)\s+)?(?:(\d+|zero|no|a|an|the|one|two|three|four|five|six|seven|eight|nine|ten)\s+)?([^"\s]+|"[^"]*")/,
		type: NumberAndWord,
		transformer: ( mode, amount, word ) => {
			const _word = word[0] === '"' ? word.slice( 1, -1 ) : word;
			const plural = isPlural( _word );
			const _cardinality = plural === true ? "plural" : plural === false ? "singular" : null;
			const quantifier = ( mode ?? "" ).replace( /\s+/g, " " ).trim();
			let _amount = amount != null && Cardinals.hasOwnProperty( amount ) ? Cardinals[amount] : amount == null ? null : parseInt( amount, 10 );
			let _mode;

			if ( quantifier !== "" ) {
				if ( _amount === 0 ) {
					throw new TypeError( `unsupported combination of "${mode}" with 0 amount` );
				}

				if ( _amount == null ) {
					if ( plural === true ) {
						throw new TypeError( `invalid use of "${mode}" without particular limit` );
					}

					_amount = 1;
				}
			}

			switch ( quantifier ) {
				case "at least" :
					if ( amount === "the" ) {
						throw new TypeError( `unspecific phrase "at least the X" ... use proper quantifier` );
					}

					_mode = ">=";
					break;

				case "up to" :
				case "at most" :
					if ( amount === "the" ) {
						throw new TypeError( `invalid or unspecific phrase "${quantifier} the X" ... use proper quantifier` );
					}

					_mode = "<=";
					break;

				default :
					if ( _amount == null ) {
						_mode = ">=";
						_amount = 1;
					} else if ( amount === "the" && plural ) {
						_mode = ">=";
						_amount = 2;
					} else {
						_mode = "==";
					}
			}

			return new NumberAndWord( _amount, pluralize.singular( _word ), _mode, _cardinality );
		},
	} ),

	/**
	 * {string-or-word} - Matches either {word} or {string} which is either a
	 * sequence of non-whitespace characters or a sequence of characters
	 * including whitespace wrapped in double quotes.
	 */
	defineParameterType( {
		name: "string-or-word",
		regexp: /([^\s"]\S*)|"([^"]*)"/,
		type: String,
		transformer: ( word, string ) => string ?? word,
	} ),

	/**
	 * {word-or-string} - Aliases {string-or-word} for convenience.
	 */
	defineParameterType( {
		name: "word-or-string",
		regexp: /([^\s"]\S*)|"([^"]*)"/,
		type: String,
		transformer: ( word, string ) => string ?? word,
	} ),

	/**
	 * {text-or-regexp} - Matches either {word}, {string} or some regular
	 *                    expression wrapped in a pair of forward slashes.
	 */
	defineParameterType( {
		name: "text-or-regexp",
		regexp: /([^\s"/]\S*)|"([^"]*)"|\/(.+?(?<![^\\](\\\\)*\\))\/(i)?/,
		type: String,
		transformer: ( word, string, re, reModifier ) => ( re ? new RegExp( re, reModifier ) : string ?? word ),
	} ),

	/**
	 * {count} - Matches non-negative integers or one of the numeral from zero to ten.
	 */
	defineParameterType( {
		name: "count",
		regexp: /\d+|no|zero|one|two|three|four|five|six|seven|eight|nine|ten/,
		type: Number,
		transformer: s => Cardinals[s] ?? parseInt( s, 10 ),
	} ),
];

for ( const { name, regexp, type, transformer } of customTypes ) {
	registry.defineParameterType( new ParameterType( name, regexp, type, transformer ) );
}

module.exports = registry;
module.exports.customTypes = customTypes;
