const { Then } = require( "@cucumber/cucumber" );
const { Api } = require( "../lib/api" );

Then( "{contextual-word} has/have (the )ID/id {text-or-regexp}", ( t, [ context, text ] ) => localStateByAttribute( t, context, true, "id", text ) );
Then( "{contextual-word} has/have (the )name {text-or-regexp}", ( t, [ context, text ] ) => localStateByAttribute( t, context, true, "name", text ) );
Then( "{contextual-word} is/are named {text-or-regexp}", ( t, [ context, text ] ) => localStateByAttribute( t, context, true, "name", text ) );
Then( "{contextual-word} is/are selected", ( t, [context] ) => localStateByAttribute( t, context, true, "selected", v => v != null ) );
Then( "{contextual-word} is/are unselected", ( t, [context] ) => localStateByAttribute( t, context, true, "selected", v => v == null ) );
Then( "{contextual-word} is/are not selected", ( t, [context] ) => localStateByAttribute( t, context, true, "selected", v => v == null ) );

/**
 * Checks if some or all elements of an addressed match have a named attribute
 * matching some expected value.
 *
 * @param {TestController} testController exposes test controller
 * @param {ContextualWord} phrase description of previous match to check
 * @param {boolean} all if true, all matching elements have to match named attribute's expected value
 * @param {string} name name of attribute to check with either matching element
 * @param {string} value expected value of named attribute per matching element
 * @returns {Promise<void>} promises check passed
 */
async function localStateByAttribute( testController, phrase, all, name, value ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const context = await Api.access( testController ).getContextFor( phrase );
	const filtered = await context.withAttribute( name, value );

	await context.checkFilteredInContext( filtered, phrase, all );
}

Then( "{contextual-word} has/have (the )value {text-or-regexp}", ( t, [ context, text ] ) => localStateByProperty( t, context, true, "value", text ) );
Then( "{contextual-word} is/are enabled", ( t, [context] ) => localStateByProperty( t, context, true, "disabled", v => !v ) );
Then( "{contextual-word} is/are disabled", ( t, [context] ) => localStateByProperty( t, context, true, "disabled", v => v ) );
Then( "{contextual-word} is/are checked", ( t, [context] ) => localStateByProperty( t, context, true, "checked", v => v ) );
Then( "{contextual-word} is/are unchecked", ( t, [context] ) => localStateByProperty( t, context, true, "checked", v => !v ) );
Then( "{contextual-word} is/are not checked", ( t, [context] ) => localStateByProperty( t, context, true, "checked", v => !v ) );

/**
 * Checks if all elements of an addressed match have a named property matching
 * some expected value.
 *
 * @param {TestController} testController exposes test controller
 * @param {ContextualWord} phrase description of previous match to check
 * @param {boolean} all if true, all matching elements have to match named attribute's expected value
 * @param {string} name name of DOM property to check on either matching element
 * @param {string|RegExp|function(string):boolean} value expected value of named property per matching element
 * @returns {Promise<void>} promises check passed
 */
async function localStateByProperty( testController, phrase, all, name, value ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const context = await Api.access( testController ).getContextFor( phrase );
	const filtered = await context.withProperty( name, value );

	await context.checkFilteredInContext( filtered, phrase, all );
}

Then( "{contextual-word} is/are marked/tagged as {word}", ( t, [ context, className ] ) => localStateByClass( t, context, true, className, false ) );
Then( "{contextual-word} is/are not marked/tagged as {word}", ( t, [ context, className ] ) => localStateByClass( t, context, true, className, true ) );

/**
 * Asserts if selected element(s) has/have given class or not.
 *
 * @param {TestController} testController exposes test controller
 * @param {ContextualWord} phrase describes context of search
 * @param {boolean} all if true, all matching elements have to match named attribute's expected value
 * @param {string} className name of class to check
 * @param {boolean} negated inverts the assertion by means of requiring one or all elements of context not to have selected class
 * @return {Promise<void>} promises step passed
 */
async function localStateByClass( testController, phrase, all, className, negated ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const context = await Api.access( testController ).getContextFor( phrase );
	const { query: property } = context.getSelector( "#classList", "classList" );
	const filtered = await context.filterBySubsWithFn( "$classList", false, node => node?.[property]?.contains( className ) ^ negated, { property, className, negated } );

	await context.checkFilteredInContext( filtered, phrase, all );
}

Then( "{contextual-word} is/are labelled with {text-or-regexp}", ( t, [ context, text ] ) => localStateByLabel( t, context, text, false ) );
Then( "{contextual-word} has/have (the )label {text-or-regexp}", ( t, [ context, text ] ) => localStateByLabel( t, context, text, false ) );
Then( "{contextual-word} is/are partially labelled with {text-or-regexp}", ( t, [ context, text ] ) => localStateByLabel( t, context, text, true ) );
Then( "{contextual-word} has/have (the )partial label {text-or-regexp}", ( t, [ context, text ] ) => localStateByLabel( t, context, text, true ) );

/**
 * Checks if an addressed match of a previously tracked search contains a given
 * text.
 *
 * @param {TestController} testController exposes test controller
 * @param {ContextualWord} phrase description of previous match to check
 * @param {string} text expected text found in elements of selected match
 * @param {boolean} partially if true, provided text does not have to match whole content of matching subs
 * @returns {Promise<void>} promises check passed
 */
async function localStateByLabel( testController, phrase, text, partially ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const context = await Api.access( testController ).getContextFor( phrase );
	const refined = await context.filterBySubsWithText( "$label", "label", text, partially );

	await refined.checkCardinalWord( phrase.asCardinalWord( await context.matches.count ), phrase.isRefining, `labelled with "${text}"` );
}

Then( "{contextual-word} read/reads {text-or-regexp}", ( t, [ context, text ] ) => localStateByTextContent( t, context, text, false ) );
Then( "{contextual-word} is reading {text-or-regexp}", ( t, [ context, text ] ) => localStateByTextContent( t, context, text, false ) );
Then( "{contextual-word} has/have text {text-or-regexp}", ( t, [ context, text ] ) => localStateByTextContent( t, context, text, false ) );
Then( "{contextual-word} partially read/reads {text-or-regexp}", ( t, [ context, text ] ) => localStateByTextContent( t, context, text, true ) );
Then( "{contextual-word} is partially reading {text-or-regexp}", ( t, [ context, text ] ) => localStateByTextContent( t, context, text, true ) );
Then( "{contextual-word} has/have partial text {text-or-regexp}", ( t, [ context, text ] ) => localStateByTextContent( t, context, text, true ) );

/**
 * Checks if an addressed match of a previously tracked search contains a given
 * text.
 *
 * @param {TestController} testController exposes test controller
 * @param {ContextualWord} phrase description of previous match to check
 * @param {string} text expected text found in elements of selected match
 * @param {boolean} partially if true searched elements may partially contain provided text to be considered a match
 * @returns {Promise<void>} promises check passed
 */
async function localStateByTextContent( testController, phrase, text, partially ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const context = await Api.access( testController ).getContextFor( phrase );
	const refined = await context.filterBySubsWithText( "$text", false, text, partially );

	await refined.checkCardinalWord( phrase.asCardinalWord( await context.matches.count ), phrase.isRefining, `${partially ? "partially " : ""}reading "${text}"` );
}

Then( "{contextual-word} (still )exists/exist", ( t, [context] ) => localStateExists( t, context, true ) );
Then( "{contextual-word} doesn't/don't exist( anymore)", ( t, [context] ) => localStateExists( t, context, false ) );
Then( "{contextual-word} does/do not exist( anymore)", ( t, [context] ) => localStateExists( t, context, false ) );
Then( "{contextual-word} is/are missing/gone", ( t, [context] ) => localStateExists( t, context, false ) );

/**
 * Checks if some contextual elements exist or are missing explicitly.
 *
 * @param {TestController} testController exposes test controller
 * @param {ContextualWord} phrase description of previous match to check
 * @param {boolean} exists if true, described contextual element has to exist to pass, otherwise it has to be missing
 * @returns {Promise<void>} promises check passed
 */
async function localStateExists( testController, phrase, exists ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const context = await Api.access( testController ).getContextFor( phrase );
	const matchCount = await context.matches.count;

	if ( exists ) {
		// FIXME might pass unintentionally if just one of several elements to be tested exists
		await testController.expect( matchCount ).gt( 0 );
	} else {
		await testController.expect( matchCount ).eql( 0 );
	}

	if ( phrase.isRefining && matchCount > 0 ) {
		await context.track( phrase.word, phrase.targetCardinality );
	}
}

module.exports = {
	localStateByAttribute,
	localStateByProperty,
	localStateByClass,
	localStateByLabel,
	localStateByTextContent,
	localStateExists,
};
