const { When } = require( "@cucumber/cucumber" );
const { Api } = require( "../lib/api" );

const actAndTrack = async( testController, cardinal, refined, action, input ) => {
	const target = await refined.find( "$" + action, false );
	const targetCount = await target.matches.count;

	await testController.expect( targetCount ).gt( 0, `there is no $${action} target for selected ${cardinal.word} to ${input == null ? action : `enter '${input}' into`}` ); // eslint-disable-line max-len
	await testController.expect( targetCount ).eql( 1, `there is no single $${action} target for selected ${cardinal.word} to ${input == null ? action : `enter '${input}' into`}` ); // eslint-disable-line max-len

	await refined.detach();

	if ( input == null ) {
		await testController[action]( target.matches );
	} else {
		await testController.typeText( target.matches, input );
	}

	await refined.track( cardinal.word, cardinal.cardinality );

	const wait = Number( process.env.ACTION_WAIT ) || 100;
	if ( wait > 0 ) {
		await testController.wait( wait );
	}
};

When( "I click (on ){cardinal-word} with ID/id {text-or-regexp}", ( t, [ cardinal, id ] ) => globalActionByAttribute( t, cardinal, "id", id, "click" ) );
When( "I click (on ){cardinal-word} with name {text-or-regexp}", ( t, [ cardinal, name ] ) => globalActionByAttribute( t, cardinal, "name", name, "click" ) );
When( "I click (on ){cardinal-word} named {text-or-regexp}", ( t, [ cardinal, name ] ) => globalActionByAttribute( t, cardinal, "name", name, "click" ) );

When( "I hover (over ){cardinal-word} with ID/id {text-or-regexp}", ( t, [ cardinal, id ] ) => globalActionByAttribute( t, cardinal, "id", id, "hover" ) );
When( "I hover (over ){cardinal-word} with name {text-or-regexp}", ( t, [ cardinal, name ] ) => globalActionByAttribute( t, cardinal, "name", name, "hover" ) );
When( "I hover (over ){cardinal-word} named {text-or-regexp}", ( t, [ cardinal, name ] ) => globalActionByAttribute( t, cardinal, "name", name, "hover" ) );

When( "I enter {string-or-word} into {cardinal-word} with ID/id {text-or-regexp}", ( t, [ input, cardinal, id ] ) => globalActionByAttribute( t, cardinal, "id", id, "enter", input ) );
When( "I enter {string-or-word} into {cardinal-word} with name {text-or-regexp}", ( t, [ input, cardinal, name ] ) => globalActionByAttribute( t, cardinal, "name", name, "enter", input ) );
When( "I enter {string-or-word} into {cardinal-word} named {text-or-regexp}", ( t, [ input, cardinal, name ] ) => globalActionByAttribute( t, cardinal, "name", name, "enter", input ) );

/**
 * Searches for element matching a named attribute by value and performs
 * selected action on it.
 *
 * @param {TestController} testController exposes test controller
 * @param {NumberAndWord} cardinal describes what to search
 * @param {string|RegExp} name name of attribute to match
 * @param {string|RegExp} value value of named attribute to match
 * @param {'click'|'hover'|'enter'} action action to perform on found element
 * @param {string} input text to enter into selected field (instead of clicking or hovering)
 * @return {Promise<void>} promises step passed
 */
async function globalActionByAttribute( testController, cardinal, name, value, action, input ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const matches = await Api.access( testController ).find( cardinal.word );

	if ( cardinal.cardinality !== "singular" ) {
		await testController.expect( false ).eql( true, `selector is not addressing a single ${cardinal.word} to ${input == null ? action : `enter '${input}' into`}` ); // eslint-disable-line max-len
	}

	const refined = await matches.withAttribute( name, value );
	const matchCount = await refined.matches.count;

	await testController.expect( matchCount ).gt( 0, `there is no matching ${cardinal.word} with attribute ${name} == "${value}" to ${input == null ? action : `enter '${input}' into`}` ); // eslint-disable-line max-len
	await testController.expect( matchCount ).eql( 1, `there is no single matching ${cardinal.word} with attribute ${name} == "${value}" to ${input == null ? action : `enter '${input}' into`}` ); // eslint-disable-line max-len

	await actAndTrack( testController, cardinal, refined, action, input );
}

When( "I click (on ){cardinal-word} marked/tagged as {word}", ( t, [ cardinal, className ] ) => globalActionByClass( t, cardinal, className, "click", false ) );
When( "I click (on ){cardinal-word} not marked/tagged as {word}", ( t, [ cardinal, className ] ) => globalActionByClass( t, cardinal, className, "click", true ) );

When( "I hover (over ){cardinal-word} marked/tagged as {word}", ( t, [ cardinal, className ] ) => globalActionByClass( t, cardinal, className, "hover", false ) );
When( "I hover (over ){cardinal-word} not marked/tagged as {word}", ( t, [ cardinal, className ] ) => globalActionByClass( t, cardinal, className, "hover", true ) );

When( "I enter {string-or-word} into {cardinal-word} marked/tagged as {word}", ( t, [ input, cardinal, className ] ) => globalActionByClass( t, cardinal, className, "enter", false, input ) );
When( "I enter {string-or-word} into {cardinal-word} not marked/tagged as {word}", ( t, [ input, cardinal, className ] ) => globalActionByClass( t, cardinal, className, "enter", true, input ) );

/**
 * Searches for element matching a named attribute by value and performs
 * selected action on it.
 *
 * @param {TestController} testController exposes test controller
 * @param {NumberAndWord} cardinal describes what to search
 * @param {string|RegExp} className class required for match
 * @param {'click'|'hover'|'enter'} action action to perform on found element
 * @param {boolean} negated inverts the assertion by means of requiring one or all elements of context not to have selected class
 * @param {string} input text to enter into selected field (instead of clicking or hovering)
 * @return {Promise<void>} promises step passed
 */
async function globalActionByClass( testController, cardinal, className, action, negated, input ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const matches = await Api.access( testController ).find( cardinal.word );

	if ( cardinal.cardinality !== "singular" ) {
		await testController.expect( false ).eql( true, `selector is not addressing a single ${cardinal.word} ${negated ? "not " : ""}marked as ${className} to ${input == null ? action : `enter '${input}' into`}` ); // eslint-disable-line max-len
	}

	const { query: property } = matches.getSelector( "#classList", "classList" );
	const refined = await matches.filterBySubsWithFn( "$classList", false, node => node?.[property]?.contains( className ) ^ negated, { property, className, negated } );
	const matchCount = await refined.matches.count;

	await testController.expect( matchCount ).gt( 0, `there is no matching ${cardinal.word} ${negated ? "not " : ""}marked as ${className} to ${input == null ? action : `enter '${input}' into`}` );
	await testController.expect( matchCount ).eql( 1, `there is no single matching ${cardinal.word} ${negated ? "not " : ""}marked as ${className} to ${input == null ? action : `enter '${input}' into`}` );

	await actAndTrack( testController, cardinal, refined, action, input );
}

When( "I click (on ){cardinal-word} with value {text-or-regexp}", ( t, [ cardinal, value ] ) => globalActionByProperty( t, cardinal, "value", value, "click" ) );

When( "I hover (over ){cardinal-word} with value {text-or-regexp}", ( t, [ cardinal, value ] ) => globalActionByProperty( t, cardinal, "value", value, "hover" ) );

When( "I enter {string-or-word} into {cardinal-word} with value {text-or-regexp}", ( t, [ input, cardinal, value ] ) => globalActionByProperty( t, cardinal, "value", value, "enter", input ) );

/**
 * Searches for element matching a named DOM property by value and performs
 * selected action on it.
 *
 * @param {TestController} testController exposes test controller
 * @param {NumberAndWord} cardinal describes what to search
 * @param {string|RegExp} name name of attribute to match
 * @param {string|RegExp} value value of named attribute to match
 * @param {'click'|'hover'|'enter'} action action to perform on found element
 * @param {string} input text to enter into selected field (instead of clicking or hovering)
 * @return {Promise<void>} promises step passed
 */
async function globalActionByProperty( testController, cardinal, name, value, action, input ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const matches = await Api.access( testController ).find( cardinal.word );

	if ( cardinal.cardinality !== "singular" ) {
		await testController.expect( false ).eql( true, `selector is not addressing a single ${cardinal.word} with property ${name} == "${value}" to ${input == null ? action : `enter '${input}' into`}` ); // eslint-disable-line max-len
	}

	const refined = await matches.withProperty( name, value );
	const matchCount = await refined.matches.count;

	await testController.expect( matchCount ).gt( 0, `there is no matching ${cardinal.word} with property ${name} == "${value}" to ${input == null ? action : `enter '${input}' into`}` ); // eslint-disable-line max-len
	await testController.expect( matchCount ).eql( 1, `there is no single matching ${cardinal.word} with property ${name} == "${value}" to ${input == null ? action : `enter '${input}' into`}` ); // eslint-disable-line max-len

	await actAndTrack( testController, cardinal, refined, action, input );
}

When( "I click (on ){cardinal-word} with label {text-or-regexp}", ( t, [ cardinal, label ] ) => globalActionByLabel( t, cardinal, label, false, "click" ) );
When( "I click (on ){cardinal-word} with partial label {text-or-regexp}", ( t, [ cardinal, label ] ) => globalActionByLabel( t, cardinal, label, true, "click" ) );
When( "I click (on ){cardinal-word} labelled with {text-or-regexp}", ( t, [ cardinal, label ] ) => globalActionByLabel( t, cardinal, label, false, "click" ) );
When( "I click (on ){cardinal-word} partially labelled with {text-or-regexp}", ( t, [ cardinal, label ] ) => globalActionByLabel( t, cardinal, label, false, "click" ) );

When( "I hover (over ){cardinal-word} with label {text-or-regexp}", ( t, [ cardinal, label ] ) => globalActionByLabel( t, cardinal, label, false, "hover" ) );
When( "I hover (over ){cardinal-word} with partial label {text-or-regexp}", ( t, [ cardinal, label ] ) => globalActionByLabel( t, cardinal, label, true, "hover" ) );
When( "I hover (over ){cardinal-word} labelled with {text-or-regexp}", ( t, [ cardinal, label ] ) => globalActionByLabel( t, cardinal, label, false, "hover" ) );
When( "I hover (over ){cardinal-word} partially labelled with {text-or-regexp}", ( t, [ cardinal, label ] ) => globalActionByLabel( t, cardinal, label, false, "hover" ) );

When( "I enter {string-or-word} into {cardinal-word} with label {text-or-regexp}", ( t, [ input, cardinal, label ] ) => globalActionByLabel( t, cardinal, label, false, "enter", input ) );
When( "I enter {string-or-word} into {cardinal-word} with partial label {text-or-regexp}", ( t, [ input, cardinal, label ] ) => globalActionByLabel( t, cardinal, label, true, "enter", input ) );
When( "I enter {string-or-word} into {cardinal-word} labelled with {text-or-regexp}", ( t, [ input, cardinal, label ] ) => globalActionByLabel( t, cardinal, label, false, "enter", input ) );
When( "I enter {string-or-word} into {cardinal-word} partially labelled with {text-or-regexp}", ( t, [ input, cardinal, label ] ) => globalActionByLabel( t, cardinal, label, false, "enter", input ) );

/**
 * Searches for type of element with a certain label and performs selected
 * action on it.
 *
 * @param {TestController} testController exposes test controller
 * @param {NumberAndWord} cardinal provides cardinal number and word
 * @param {string} label provides required label of matching element(s)
 * @param {boolean} partially if true, provided text does not have to match whole content of matching subs
 * @param {'click'|'hover'|'enter'} action action to perform on found element
 * @param {string} input text to enter into selected field (instead of clicking or hovering)
 * @returns {Promise<void>} promises step passed
 */
async function globalActionByLabel( testController, cardinal, label, partially, action, input ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const matches = await Api.access( testController ).find( cardinal.word );

	if ( cardinal.cardinality !== "singular" ) {
		await testController.expect( false ).eql( true, `selector is not addressing a single ${cardinal.word} with ${partially ? "partial " : ""}label "${label}" to ${input == null ? action : `enter '${input}' into`}` ); // eslint-disable-line max-len
	}

	const refined = await matches.filterBySubsWithText( "$label", "label", label, partially );
	const matchCount = await refined.matches.count;

	await testController.expect( matchCount ).gt( 0, `there is no matching ${cardinal.word} with ${partially ? "partial " : ""}label "${label}" to ${input == null ? action : `enter '${input}' into`}` );
	await testController.expect( matchCount ).eql( 1, `there is no single matching ${cardinal.word} with ${partially ? "partial " : ""}label "${label}" to ${input == null ? action : `enter '${input}' into`}` );

	await actAndTrack( testController, cardinal, refined, action, input );
}

When( "I click (on ){cardinal-word} reading {text-or-regexp}", ( t, [ cardinal, text ] ) => globalActionByTextContent( t, cardinal, text, false, "click" ) );
When( "I click (on ){cardinal-word} with text {text-or-regexp}", ( t, [ cardinal, text ] ) => globalActionByTextContent( t, cardinal, text, false, "click" ) );
When( "I click (on ){cardinal-word} partially reading {text-or-regexp}", ( t, [ cardinal, text ] ) => globalActionByTextContent( t, cardinal, text, true, "click" ) );
When( "I click (on ){cardinal-word} with partial text {text-or-regexp}", ( t, [ cardinal, text ] ) => globalActionByTextContent( t, cardinal, text, true, "click" ) );

When( "I hover (over ){cardinal-word} reading {text-or-regexp}", ( t, [ cardinal, text ] ) => globalActionByTextContent( t, cardinal, text, false, "hover" ) );
When( "I hover (over ){cardinal-word} with text {text-or-regexp}", ( t, [ cardinal, text ] ) => globalActionByTextContent( t, cardinal, text, false, "hover" ) );
When( "I hover (over ){cardinal-word} partially reading {text-or-regexp}", ( t, [ cardinal, text ] ) => globalActionByTextContent( t, cardinal, text, true, "hover" ) );
When( "I hover (over ){cardinal-word} with partial text {text-or-regexp}", ( t, [ cardinal, text ] ) => globalActionByTextContent( t, cardinal, text, true, "hover" ) );

When( "I enter {string-or-word} into {cardinal-word} reading {text-or-regexp}", ( t, [ input, cardinal, text ] ) => globalActionByTextContent( t, cardinal, text, false, "enter", input ) );
When( "I enter {string-or-word} into {cardinal-word} with text {text-or-regexp}", ( t, [ input, cardinal, text ] ) => globalActionByTextContent( t, cardinal, text, false, "enter", input ) );
When( "I enter {string-or-word} into {cardinal-word} partially reading {text-or-regexp}", ( t, [ input, cardinal, text ] ) => globalActionByTextContent( t, cardinal, text, true, "enter", input ) );
When( "I enter {string-or-word} into {cardinal-word} with partial text {text-or-regexp}", ( t, [ input, cardinal, text ] ) => globalActionByTextContent( t, cardinal, text, true, "enter", input ) );

/**
 * Searches for type of element with given text content and performs selected
 * action on it.
 *
 * @param {TestController} testController exposes test controller
 * @param {NumberAndWord} cardinal provides cardinal number and word
 * @param {string} text provides text to be found in eventually matching element(s)
 * @param {boolean} partially if true, provided text does not have to match whole content of matching subs
 * @param {'click'|'hover'|'enter'} action action to perform on found element
 * @param {string} input text to enter into selected field (instead of clicking or hovering)
 * @returns {Promise<void>} promises step passed
 */
async function globalActionByTextContent( testController, cardinal, text, partially, action, input ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const matches = await Api.access( testController ).find( cardinal.word );

	if ( cardinal.cardinality !== "singular" ) {
		await testController.expect( false ).eql( true, `selector is not addressing a single ${cardinal.word} ${partially ? "partially " : ""}reading "${text}" to ${input == null ? action : `enter '${input}' into`}` ); // eslint-disable-line max-len
	}

	const refined = await matches.filterBySubsWithText( "$text", false, text, partially );
	const matchCount = await refined.matches.count;

	await testController.expect( matchCount ).gt( 0, `there is no matching ${cardinal.word} ${partially ? "partially " : ""}reading "${text}" to ${input == null ? action : `enter '${input}' into`}` );
	await testController.expect( matchCount ).eql( 1, `there is no single matching ${cardinal.word} ${partially ? "partially " : ""}reading "${text}" to ${input == null ? action : `enter '${input}' into`}` );

	await actAndTrack( testController, cardinal, refined, action, input );
}

module.exports = {
	globalActionByAttribute,
	globalActionByProperty,
	globalActionByClass,
	globalActionByLabel,
	globalActionByTextContent,
};
