const { Given } = require( "@cucumber/cucumber" );
const { Api } = require( "../lib/api" );

Given( "{contextual-word} has/have {cardinal-word}", ( t, [ context, cardinal ] ) => localFind( t, context, cardinal ) );
Given( "{contextual-word} has/have a list/set of {cardinal-word}", ( t, [ context, cardinal ] ) => localFind( t, context, cardinal ) );

/**
 * Finds elements in context of containing elements.
 *
 * @param {TestController} testController exposes test controller
 * @param {ContextualWord} phrase describes context of search
 * @param {NumberAndWord} cardinal describes what to search
 * @return {Promise<void>} promises step passed
 */
async function localFind( testController, phrase, cardinal ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const context = await Api.access( testController ).getContextFor( phrase );
	const matches = await context.find( cardinal.word );

	await matches.checkCardinalWord( cardinal );
}

Given( "{contextual-word} has/have {cardinal-word} with ID/id {text-or-regexp}", ( t, [ context, cardinal, id ] ) => localFindByAttribute( t, context, cardinal, "id", id ) );
Given( "{contextual-word} has/have {cardinal-word} with name {text-or-regexp}", ( t, [ context, cardinal, name ] ) => localFindByAttribute( t, context, cardinal, "name", name ) );
Given( "{contextual-word} has/have {cardinal-word} named {text-or-regexp}", ( t, [ context, cardinal, name ] ) => localFindByAttribute( t, context, cardinal, "name", name ) );

/**
 * Finds elements in context of containing elements matching a named attribute
 * by value.
 *
 * @param {TestController} testController exposes test controller
 * @param {ContextualWord} phrase describes context of search
 * @param {NumberAndWord} cardinal describes what to search
 * @param {string|RegExp} name name of attribute to match
 * @param {string|RegExp} value value of named attribute to match
 * @return {Promise<void>} promises step passed
 */
async function localFindByAttribute( testController, phrase, cardinal, name, value ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const context = await Api.access( testController ).getContextFor( phrase );
	const matches = await context.find( cardinal.word );
	const refined = await matches.withAttribute( name, value );

	await refined.checkCardinalWord( cardinal, true, `with ${name} ${value instanceof RegExp ? "matching" : "equals"} ${value}` );
}

Given( "{contextual-word} has/have {cardinal-word} with value {text-or-regexp}", ( t, [ context, cardinal, value ] ) => localFindByProperty( t, context, cardinal, "value", value ) );
Given( "{contextual-word} has/have {cardinal-word} which is/are enabled", ( t, [ context, cardinal ] ) => localFindByProperty( t, context, cardinal, "disabled", v => !v ) );
Given( "{contextual-word} has/have {cardinal-word} which is/are disabled", ( t, [ context, cardinal ] ) => localFindByProperty( t, context, cardinal, "disabled", v => v ) );
Given( "{contextual-word} has/have {cardinal-word} which is/are checked", ( t, [ context, cardinal ] ) => localFindByProperty( t, context, cardinal, "checked", v => v ) );
Given( "{contextual-word} has/have {cardinal-word} which is/are not checked", ( t, [ context, cardinal ] ) => localFindByProperty( t, context, cardinal, "checked", v => !v ) );
Given( "{contextual-word} has/have {cardinal-word} which is/are unchecked", ( t, [ context, cardinal ] ) => localFindByProperty( t, context, cardinal, "checked", v => !v ) );

/**
 * Finds elements in context of containing elements matching a named DOM
 * property by value.
 *
 * @param {TestController} testController exposes test controller
 * @param {ContextualWord} phrase describes context of search
 * @param {NumberAndWord} cardinal describes what to search
 * @param {string} name name of DOM property to match
 * @param {string|function(string):boolean} value value of named DOM property to match, or callback deciding if found value matches
 * @return {Promise<void>} promises step passed
 */
async function localFindByProperty( testController, phrase, cardinal, name, value ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const context = await Api.access( testController ).getContextFor( phrase );
	const matches = await context.find( cardinal.word );
	const refined = await matches.withProperty( name, value );

	await refined.checkCardinalWord( cardinal, true, typeof value === "function" ? `${value() ? "not " : ""}${name}` : `with ${name} ${value instanceof RegExp ? "matching" : "equals"} ${value}` );
}

Given( "{contextual-word} has/have {cardinal-word} marked/tagged as {word}", ( t, [ context, cardinal, className ] ) => localFindByClass( t, context, cardinal, className, false ) );
Given( "{contextual-word} has/have {cardinal-word} not marked/tagged as {word}", ( t, [ context, cardinal, className ] ) => localFindByClass( t, context, cardinal, className, true ) );

/**
 * Finds elements in context of containing elements with named class.
 *
 * @param {TestController} testController exposes test controller
 * @param {ContextualWord} phrase describes context of search
 * @param {NumberAndWord} cardinal describes what to search
 * @param {string} className name of class to check
 * @param {boolean} negated inverts the assertion by means of requiring one or all elements of context not to have selected class
 * @return {Promise<void>} promises step passed
 */
async function localFindByClass( testController, phrase, cardinal, className, negated ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const context = await Api.access( testController ).getContextFor( phrase );
	const matches = await context.find( cardinal.word );
	const { query: property } = matches.getSelector( "#classList", "classList" );
	const refined = await matches.filterBySubsWithFn( "$classList", false, node => node?.[property]?.contains( className ) ^ negated, { property, className, negated } );

	await refined.checkCardinalWord( cardinal, true, `${negated ? "not " : ""}marked as "${className}"` );
}

Given( "{contextual-word} has/have {cardinal-word} with label {text-or-regexp}", ( t, [ context, cardinal, label ] ) => localFindByLabel( t, context, cardinal, label, false ) );
Given( "{contextual-word} has/have {cardinal-word} with partial label {text-or-regexp}", ( t, [ context, cardinal, label ] ) => localFindByLabel( t, context, cardinal, label, true ) );
Given( "{contextual-word} has/have {cardinal-word} labelled with {text-or-regexp}", ( t, [ context, cardinal, label ] ) => localFindByLabel( t, context, cardinal, label, false ) );
Given( "{contextual-word} has/have {cardinal-word} partially labelled with {text-or-regexp}", ( t, [ context, cardinal, label ] ) => localFindByLabel( t, context, cardinal, label, true ) );

/**
 * Finds elements in context of containing elements with a given label.
 *
 * @param {TestController} testController exposes test controller
 * @param {ContextualWord} phrase describes context of search
 * @param {NumberAndWord} cardinal describes what to search
 * @param {string} label expected label of item(s) to find
 * @param {boolean} partially if true, provided text does not have to match whole content of matching subs
 * @return {Promise<void>} promises step passed
 */
async function localFindByLabel( testController, phrase, cardinal, label, partially ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const context = await Api.access( testController ).getContextFor( phrase );
	const matches = await context.find( cardinal.word );
	const refined = await matches.filterBySubsWithText( "$label", "label", label, partially );

	await refined.checkCardinalWord( cardinal, true, `${partially ? "partially " : ""}labelled with "${label}"` );
}

Given( "{contextual-word} has/have {cardinal-word} reading {text-or-regexp}", ( t, [ context, cardinal, text ] ) => localFindByTextContent( t, context, cardinal, text, false ) );
Given( "{contextual-word} has/have {cardinal-word} with text {text-or-regexp}", ( t, [ context, cardinal, text ] ) => localFindByTextContent( t, context, cardinal, text, false ) );
Given( "{contextual-word} has/have {cardinal-word} partially reading {text-or-regexp}", ( t, [ context, cardinal, text ] ) => localFindByTextContent( t, context, cardinal, text, true ) );
Given( "{contextual-word} has/have {cardinal-word} with partial text {text-or-regexp}", ( t, [ context, cardinal, text ] ) => localFindByTextContent( t, context, cardinal, text, true ) );

/**
 * Finds elements in context of containing elements containing given text.
 *
 * @param {TestController} testController exposes test controller
 * @param {ContextualWord} phrase describes context of search
 * @param {NumberAndWord} cardinal describes what to search
 * @param {string} text provides expected text content of item(s) to find
 * @param {boolean} partially if true, provided text does not have to match whole content of matching subs
 * @return {Promise<void>} promises step passed
 */
async function localFindByTextContent( testController, phrase, cardinal, text, partially ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const context = await Api.access( testController ).getContextFor( phrase );
	const matches = await context.find( cardinal.word );
	const refined = await matches.filterBySubsWithText( "$text", false, text, partially );

	await refined.checkCardinalWord( cardinal, true, `${partially ? "partially " : ""}reading "${text}"` );
}

module.exports = {
	localFind,
	localFindByAttribute,
	localFindByProperty,
	localFindByClass,
	localFindByLabel,
	localFindByTextContent,
};
