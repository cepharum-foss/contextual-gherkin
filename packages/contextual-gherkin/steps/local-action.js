const { When } = require( "@cucumber/cucumber" );
const { Api } = require( "../lib/api" );

When( "I click (on ){contextual-word}", ( t, [context] ) => localAction( t, context, "click" ) );

When( "I hover (over ){contextual-word}", ( t, [context] ) => localAction( t, context, "hover" ) );

When( "I enter {string-or-word} into {contextual-word}", ( t, [ input, context ] ) => localAction( t, context, "enter", input ) );

/**
 * Searches for type of element in current context and performs selected action
 * on it.
 *
 * @param {TestController} testController exposes test controller
 * @param {ContextualWord} phrase description of element to be clicked.
 * @param {'click'|'hover'|'enter'} action action to perform on found element
 * @param {string} input text to enter into selected field (instead of clicking or hovering)
 * @returns {Promise<void>} promises element clicked
 */
async function localAction( testController, phrase, action, input ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const context = await Api.access( testController ).getContextFor( phrase );
	const matchCount = await context.matches.count;

	await testController.expect( matchCount ).gt( 0, `there is no ${phrase.word} to ${input == null ? action : `enter '${input}' into`}` );
	await testController.expect( matchCount ).eql( 1, `there is no single ${phrase.word} to ${input == null ? action : `enter '${input}' into`}` );

	const target = await context.find( "$" + action, false );
	const targetCount = await target.matches.count;

	await testController.expect( targetCount ).gt( 0, `there is no $${action} target for selected ${phrase.word} to ${input == null ? action : `enter '${input}' into`}` ); // eslint-disable-line max-len
	await testController.expect( targetCount ).eql( 1, `there is no single $${action} target for selected ${phrase.word} to ${input == null ? action : `enter '${input}' into`}` ); // eslint-disable-line max-len

	await context.detach();

	if ( input == null ) {
		await testController[action]( target.matches );
	} else {
		await testController.typeText( target.matches, input );
	}

	if ( phrase.isRefining && matchCount > 0 ) {
		await context.track( phrase.word, phrase.targetCardinality );
	}

	const wait = Number( process.env.ACTION_WAIT ) || 100;
	if ( wait > 0 ) {
		await testController.wait( wait );
	}
}

module.exports = {
	localAction,
};
