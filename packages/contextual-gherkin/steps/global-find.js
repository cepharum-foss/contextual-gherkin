const { Given } = require( "@cucumber/cucumber" );
const { Api } = require( "../lib/api" );

Given( "there is/are {cardinal-word}", ( t, [cardinal] ) => globalFind( t, cardinal ) );
Given( "there is a list/set of {cardinal-word}", ( t, [cardinal] ) => globalFind( t, cardinal ) );

/**
 * Searches for type of elements with a certain label.
 *
 * @param {TestController} testController exposes test controller
 * @param {NumberAndWord} cardinal provides cardinal number and word
 * @returns {Promise<void>} promises step passed
 */
async function globalFind( testController, cardinal ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const found = await Api.access( testController ).find( cardinal.word );

	await found.checkCardinalWord( cardinal );
}

Given( "there is/are {cardinal-word} with ID/id {text-or-regexp}", ( t, [ cardinal, id ] ) => globalFindByAttribute( t, cardinal, "id", id ) );
Given( "there is/are {cardinal-word} with name {text-or-regexp}", ( t, [ cardinal, name ] ) => globalFindByAttribute( t, cardinal, "name", name ) );
Given( "there is/are {cardinal-word} named {text-or-regexp}", ( t, [ cardinal, name ] ) => globalFindByAttribute( t, cardinal, "name", name ) );
Given( "there is/are {cardinal-word} which is/are selected", ( t, [cardinal] ) => globalFindByAttribute( t, cardinal, "selected", v => v != null ) );
Given( "there is/are {cardinal-word} which is/are not selected", ( t, [cardinal] ) => globalFindByAttribute( t, cardinal, "selected", v => v == null ) );
Given( "there is/are {cardinal-word} which is/are unselected", ( t, [cardinal] ) => globalFindByAttribute( t, cardinal, "selected", v => v == null ) );

/**
 * Searches for element(s) matching a named attribute by value.
 *
 * @param {TestController} testController exposes test controller
 * @param {NumberAndWord} cardinal describes what to search
 * @param {string|RegExp} name name of attribute to match
 * @param {string|RegExp} value value of named attribute to match
 * @return {Promise<void>} promises step passed
 */
async function globalFindByAttribute( testController, cardinal, name, value ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const matches = await Api.access( testController ).find( cardinal.word );
	const refined = await matches.withAttribute( name, value );

	await refined.checkCardinalWord( cardinal, true, `with ${name} ${value instanceof RegExp ? "matching" : "equals"} ${value}` );
}

Given( "there is/are {cardinal-word} with value {text-or-regexp}", ( t, [ cardinal, value ] ) => globalFindByProperty( t, cardinal, "value", value ) );
Given( "there is/are {cardinal-word} which is/are enabled", ( t, [cardinal] ) => globalFindByProperty( t, cardinal, "disabled", v => !v ) );
Given( "there is/are {cardinal-word} which is/are disabled", ( t, [cardinal] ) => globalFindByProperty( t, cardinal, "disabled", v => v ) );
Given( "there is/are {cardinal-word} which is/are checked", ( t, [cardinal] ) => globalFindByProperty( t, cardinal, "checked", v => v ) );
Given( "there is/are {cardinal-word} which is/are not checked", ( t, [cardinal] ) => globalFindByProperty( t, cardinal, "checked", v => !v ) );
Given( "there is/are {cardinal-word} which is/are unchecked", ( t, [cardinal] ) => globalFindByProperty( t, cardinal, "checked", v => !v ) );

/**
 * Searches for element(s) matching a named DOM property by value.
 *
 * @param {TestController} testController exposes test controller
 * @param {NumberAndWord} cardinal describes what to search
 * @param {string} name name of DOM property to match
 * @param {string|function(string):boolean} value value of named DOM property to match, or callback deciding if found value matches
 * @return {Promise<void>} promises step passed
 */
async function globalFindByProperty( testController, cardinal, name, value ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const matches = await Api.access( testController ).find( cardinal.word );
	const refined = await matches.withProperty( name, value );

	await refined.checkCardinalWord( cardinal, true, typeof value === "function" ? `${value() ? "not " : ""}${name}` : `with ${name} ${value instanceof RegExp ? "matching" : "equals"} ${value}` );
}

Given( "there is/are {cardinal-word} marked/tagged as {word}", ( t, [ cardinal, mark ] ) => globalFindByClass( t, cardinal, mark, false ) );
Given( "there is/are {cardinal-word} not marked/tagged as {word}", ( t, [ cardinal, mark ] ) => globalFindByClass( t, cardinal, mark, true ) );

/**
 * Searches for element(s) with a named class set.
 *
 * @param {TestController} testController exposes test controller
 * @param {NumberAndWord} cardinal describes what to search
 * @param {string} className name of class to test
 * @param {boolean} negated inverts the assertion by means of requiring one or all elements of context not to have selected class
 * @return {Promise<void>} promises step passed
 */
async function globalFindByClass( testController, cardinal, className, negated ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const matches = await Api.access( testController ).find( cardinal.word );
	const { query: property } = matches.getSelector( "#classList", "classList" );
	const refined = await matches.filterBySubsWithFn( "$classList", false, node => node?.[property]?.contains( className ) ^ negated, { property, className, negated } );

	await refined.checkCardinalWord( cardinal, true, `${negated ? "not " : ""}marked as "${className}"` );
}

Given( "there is/are {cardinal-word} with label {text-or-regexp}", ( t, [ cardinal, label ] ) => globalFindByLabel( t, cardinal, label, false ) );
Given( "there is/are {cardinal-word} with partial label {text-or-regexp}", ( t, [ cardinal, label ] ) => globalFindByLabel( t, cardinal, label, true ) );
Given( "there is/are {cardinal-word} labelled with {text-or-regexp}", ( t, [ cardinal, label ] ) => globalFindByLabel( t, cardinal, label, false ) );
Given( "there is/are {cardinal-word} partially labelled with {text-or-regexp}", ( t, [ cardinal, label ] ) => globalFindByLabel( t, cardinal, label, false ) );

/**
 * Searches for type of elements with a certain label.
 *
 * @param {TestController} testController exposes test controller
 * @param {NumberAndWord} cardinal provides cardinal number and word
 * @param {string} label provides required label of matching element(s)
 * @param {boolean} partially if true, provided text does not have to match whole content of matching subs
 * @returns {Promise<void>} promises step passed
 */
async function globalFindByLabel( testController, cardinal, label, partially ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const matches = await Api.access( testController ).find( cardinal.word );
	const refined = await matches.filterBySubsWithText( "$label", "label", label, partially );

	await refined.checkCardinalWord( cardinal, true, `${partially ? "partially " : ""}labelled with "${label}"` );
}

Given( "there is/are {cardinal-word} reading {text-or-regexp}", ( t, [ cardinal, text ] ) => globalFindByTextContent( t, cardinal, text, false ) );
Given( "there is/are {cardinal-word} with text {text-or-regexp}", ( t, [ cardinal, text ] ) => globalFindByTextContent( t, cardinal, text, false ) );
Given( "there is/are {cardinal-word} partially reading {text-or-regexp}", ( t, [ cardinal, text ] ) => globalFindByTextContent( t, cardinal, text, true ) );
Given( "there is/are {cardinal-word} with partial text {text-or-regexp}", ( t, [ cardinal, text ] ) => globalFindByTextContent( t, cardinal, text, true ) );

/**
 * Searches for type of elements with a certain label.
 *
 * @param {TestController} testController exposes test controller
 * @param {NumberAndWord} cardinal provides cardinal number and word
 * @param {string} text provides text to be found in eventually matching element(s)
 * @param {boolean} partially if true, provided text does not have to match whole content of matching subs
 * @returns {Promise<void>} promises step passed
 */
async function globalFindByTextContent( testController, cardinal, text, partially ) {
	if ( process.env.STEP_BY_STEP ) {
		await testController.debug();
	}

	const matches = await Api.access( testController ).find( cardinal.word );
	const refined = await matches.filterBySubsWithText( "$text", false, text, partially );

	await refined.checkCardinalWord( cardinal, true, `${partially ? "partially " : ""}reading "${text}"` );
}

module.exports = {
	globalFind,
	globalFindByAttribute,
	globalFindByProperty,
	globalFindByClass,
	globalFindByLabel,
	globalFindByTextContent,
};
