const { Given } = require( "@cucumber/cucumber" );

Given( "page {word} is loaded", ( testController, url ) => testController.navigateTo( "http://localhost:3000/" + url + ".html" ) );
