import { Before } from "@cucumber/cucumber";
import { ContextualGherkin } from "@cepharum/contextual-gherkin";

Before( "@alias", testController => {
	ContextualGherkin( testController, {
		selectors: {
			"user list": "ul.users",
			"account list": "ul.users",
			"role list": "ul.roles",
			item: "li",
		},
		aliases: {
			list: [ "account list", "user list", "role list" ],
		},
	} );
} );

Before( "@basic", testController => {
	ContextualGherkin( testController, {
		selectors: {
			paragraph: "p",
			button: {
				"": "button",
				label: {
					"": ".label",
					"@name": "x-name"
				},
				$label: {
					"": ".label",
				},
			},
			"list item": "ul > li.matching",
			changer: "#changer",
			label: "label",
			$label: "label",
			village: {
				"": ".village",
				house: ".house",
				$label: "h3",
				villa: [
					node => node?.classList?.contains( type ) && content.test( node.textContent ), // eslint-disable-line no-undef
					{ type: "house", content: /\bBoo\b/ },
				],
			},
			island: {
				"": ".island",
				$label: "h2",
			},
			hideout: {
				"": "hide-out",
				$text: node => node.shadowRoot?.querySelector( ".inner" ),
			},
			spot: {
				"": "hide-out",
				$text: false,
			},
			map: {
				"": ".map",
				$label: "h1",
				island: {
					"": ".island",
					village: {
						"": ".village",
					},
				},
			},
			organism: {
				"": ".organism",
				output: ".output",
				$enter: ".input",
				$hover: ".hover",
				$click: ".click",
			}
		},
		aliases: {
			item: [ "list item", "bullet point" ],
			organism: "component",
			changer: "activator",
		},
	} );
} );

Before( "@findComplex", testController => {
	ContextualGherkin( testController, {
		selectors: {
			box: {
				"": "span.field",
				$name: node => node.previousElementSibling,
				$value: node => node.previousElementSibling,
				$classList: node => node.parentElement,
			},
			component: {
				"": ".view",
				$name: ".field",
				$value: ".field",
				"@name": "data-bar",
				"#value": "ariaLabel",
			},
			container: {
				"": ".container",
				$name: ".view input",
				$value: ".number",
			},
			control: {
				"": ".field",
				$name: node => node.previousElementSibling,
				$value: node => node.previousElementSibling,
			},
			field: {
				"": ".field",
				"@name": "data-bar",
				"#value": "ariaLabel",
				$label: node => node.parentElement.querySelector( "label" ),
			},
			input: {
				"": "input",
			},
			object: {
				"": ".container",
				$label: ".view label",
				$text: ".field"
			},
		},
	} );
} );

Before( "@form", testController => {
	ContextualGherkin( testController, {
		selectors: {
			form: {
				"": "form",
				field: ".field",
				panel: {
					"": ".control",
					button: {
						"": "button",
						$label: false,
					},
				},
			},
			selector: "select",
			option: "option",
			"text field": "input",
			$label: "label",
		},
	} );
} );

Before( "@phrases", testController => {
	ContextualGherkin( testController, {
		selectors: {
			element: "div, h2",
			panel: ".panel",
			headline: "h2",
			cell: ".cell",
			output: "#output",
		},
	} );
} );

Before( "@regression", testController => {
	ContextualGherkin( testController, {
		selectors: {
			container: {
				"": ".content",
				"locations list": {
					"": "x-locations-table",
					location: "x-locations-table-row",
				},
			}
		},
		aliases: {
			list: "locations list",
			"locations list": "list",
			row: "location",
			location: "row",
		},
	} );
} );
