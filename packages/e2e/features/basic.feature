@basic
Feature: Simple document

	Scenario: check content

		Given page basic is loaded
		And there is one paragraph
		And there is one button with label "Click Me"
		And there is a label reading "Haven't clicked yet"
		When I click on this button
		Then the label reads "Clicked"
		And the button has label "Click Me"

	Scenario: check content in container

		Given page basic is loaded
		And there is a button
		And it has a label
		And it is named primary
		And it is named /^Prr?imary$/i
		Then the label reads "Click Me"

	Scenario: count matching elements

		Given page basic is loaded
		And there is a village
		Then it has houses reading "Foo"
		And they read /Foo/
		And it has a house reading "Foo"
		And it reads /foo/i
		And the village has two houses reading "Foo"
		And the village has at least one house reading "Foo"
		And it reads /^FOO$/i
		And the village has at least two houses reading "Foo"
		And they read /^FOO\/?$/i
		And the village has at most two houses reading "Foo"
		And the village has up to two houses reading "Foo"
		And the village has up to two houses reading "Boo"
		And the village has up to one house reading "Boo"
		And the village has up to three houses reading /^[fB]oo$/i
		And the village has a villa reading "Boo"

	Scenario: filter by shadow DOM

		Given page basic is loaded
		And there is an island
		And it has two hideouts
		And it has one hideout reading "a secret to find"
		And the island has two hideouts partially reading "secret to find"
		And the island has no spot reading "a secret to find"
		And the island has no spot partially reading "secret to find"
		And there is one hideout reading "a secret to find"
		And there are two hideouts partially reading "secret to find"
		And there are two hideouts partially reading /secret\sto\sfind/
		And there is no spot reading "a secret to find"
		And there is no spot partially reading "secret to find"

	Scenario: selector context fallback

		Given page basic is loaded
		And there is a map
		And it has an island
		And it has a village
		Then the map is labelled with "Map of the world"
		And the map is labelled with /^map of\sthe world$/i
		But the map has no island labelled with "A small remote island"
		And the island has no village labelled with "Village"
		But there is an island labelled with "A small remote island"
		And there is a village labelled with "Village"

	Scenario: virtual selector for custom interaction targets

		Given page basic is loaded
		And there is an organism
		And it has an output
		When I click on the organism
		Then the output reads "clicked"
		When I hover the organism
		Then the output reads "hovered"
		When I enter "foo" into the organism
		Then the output reads "foo"
		When I click on the organism marked as ready
		Then the output reads "clicked"
		When I hover the component marked as ready
		Then the output reads "hovered"
		When I enter "bar" into the organism marked as ready
		Then the output reads "foobar"
