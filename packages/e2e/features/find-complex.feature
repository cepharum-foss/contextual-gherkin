@findComplex
Feature: Find elements

	Background:
		Given page find-complex is loaded

	Scenario: by matching attribute

		When there is one input with name surname
		Then it is marked as text

		When there is one field with name surname
		Then it is marked as alt
		Then it is labelled with "Surname"

		When there is one container with name surname
		Then it is marked as first

		When there is one component with name surname
		Then it is marked as sub-a

		When there is one control with name surname
		Then it is marked as alt

		When there is one box with name surname
		Then it is marked as sub-a
		And it reads "foo"
		Then there is one box marked as sub-a
		And it reads "foo"

		When there is one object with label "Surname"
		Then it reads "foo"
		When there is one object with text "foo"
		Then it is labelled with "Surname"

	Scenario: by matching property

		When there is one input with value numeric
		Then it is marked as number

		When there is one field with value numeric
		Then it is marked as custom
		Then it is labelled with "Numeric"

		When there is one container with value numeric
		Then it is marked as second

		When there is one component with value numeric
		Then it is marked as sub-b

		When there is one control with value numeric
		Then it is marked as custom

		When there is one box with value numeric
		Then it is marked as sub-b
		And it reads "bar"
		Then there is one box marked as sub-b
		And it reads "bar"

		When there is one object with label "Numeric"
		Then it reads "bar"
		When there is one object with text "bar"
		Then it is labelled with "Numeric"
