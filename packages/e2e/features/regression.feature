@regression
Feature: Regression testing

	Scenario: issue #8

		Given page regression/issue-8 is loaded
		And there is a container

		Then it has a "locations list"
		And it has two rows

		Then the container has a "list"
		And it has two locations
