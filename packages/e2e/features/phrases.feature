@phrases
Feature: Testing phrases

	Scenario: globally finding

		Given page phrases is loaded
		And there are three panels
		And there are the headlines
		And there are 3 headlines
		And there are 3 headlines reading /Panel #\d/
		And there are 3 headlines partially reading "Panel "
		And there are at least 25 cells
		And there are at most 30 cells
		And there are 27 cells
		And there is a cell
		And there are nine cells with text /.+ #2/
		And there are nine cells marked as middle
		And there are 18 cells not marked as middle
		And there is no cell marked as missing

	Scenario Outline: globally clicking (<label>)

		Given page phrases is loaded
		Then there is at most one element with text "<label>"
		When I click on a headline reading "<label>"
		Then there is an output with text "<label>"
		Then there are two elements with text "<label>"

		Examples:
			| label    |
			| Panel #1 |
			| Panel #2 |
			| Panel #3 |

	Scenario Outline: globally hovering (<label>)

		Given page phrases is loaded
		When I hover over the headline reading "<label>"
		Then there is an element marked as hovered
		And it reads "<label>"
		And there is an element tagged as hovered
		And it has text "<label>"

		Examples:
			| label    |
			| Panel #1 |
			| Panel #2 |
			| Panel #3 |

	Scenario: locally finding

		Given page phrases is loaded
		And there is a panel
		And it has one headline
		And the panel has one headline reading /Panel #\d/
		And the panel has one headline partially reading "Panel "
		And the panel has at least 5 cells
		And the panel has at most 10 cells
		And the panel has 9 cells
		And the panel has a cell
		And the panel has no cell with text /.+ #2/
		But the panel has nine cells with partial text / #1/
		And the panel has three cells marked as middle
		And the panel has 6 cells not marked as middle
		And the panel has no cell marked as missing

	Scenario Outline: locally clicking (<label>)

		Given page phrases is loaded
		And there is a panel with partial text "<label>"
		And it has a headline reading "<label>"
		When I click on it
		Then it is enabled
		Then it is not checked
		Then there is an output with text "<label>"

		Examples:
			| label    |
			| Panel #1 |
			| Panel #2 |
			| Panel #3 |

	Scenario Outline: locally hovering (<label>)

		Given page phrases is loaded
		And there is a panel with partial text "<label>"
		And it has a headline reading "<label>"
		When I hover over it
		Then it is marked as hovered
		Then it is not marked as missing
		Then it is enabled
		Then it is not checked
		And it reads "<label>"
		And it is tagged as hovered
		And it is not tagged as missing
		And it has text "<label>"

		Examples:
			| label    |
			| Panel #1 |
			| Panel #2 |
			| Panel #3 |

	Scenario: regular cardinality matching

		Given page phrases is loaded
		Then there are cells partially reading "of panel #2"
		And the first cell reads "Cell 1.1 of panel #2"
		And the third cell reads "Cell 1.3 of panel #2"
		And the fourth cell reads "Cell 2.1 of panel #2"

		And there are cells partially reading "of panel #3"
		And the first of these cell reads "Cell 1.1 of panel #3"
		And the third of these cell reads "Cell 1.3 of panel #3"
		And the fourth of these cell reads "Cell 2.1 of panel #3"
		And the first of those cell reads "Cell 1.1 of panel #2"
		And the third of those cell reads "Cell 1.3 of panel #2"
		And the fourth of those cell reads "Cell 2.1 of panel #2"
		And the first cell reads "Cell 1.1 of panel #3"
		And the third cell reads "Cell 1.3 of panel #3"
		And the fourth cell reads "Cell 2.1 of panel #3"

	Scenario: extended cardinality matching

		Given page phrases is loaded
		And there is a cell reading "Cell 1.1 of panel #2"
		And there is a cell reading "Cell 1.1 of panel #3"
		And there is a cell reading "Cell 1.1 of panel #1"

		Then the second cell reads "Cell 1.1 of panel #3"
		Then the third cell is reading "Cell 1.1 of panel #1"
		Then the first cell reads "Cell 1.1 of panel #2"
