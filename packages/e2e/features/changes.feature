@changes
Feature: Tracking matches

	Scenario: when changed meanwhile

		Given page changes is loaded
		And there are three "list items"
		And the first of these items reads "one"
		And it reads one
		And the second of these items reads "two"
		And the third of these items reads "three"
		And there is an activator
		When I click it
		Then the first of these items reads one
		And the second of these items is missing
		And the third of these items reads three
		But there are no "list items"
