@alias
Feature: Aliases

	Background:
		Given page alias is loaded

	Scenario: test 1
		When there is a "user list"
		Then this list has two items

	Scenario: test 2
		When there is a "role list"
		Then this list has three items

	Scenario: test 3
		When there is an "account list"
		Then this list has two items
