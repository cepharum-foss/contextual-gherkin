@form
Feature: A form

	Scenario: supports entering text

		Given page form is loaded
		And there is a form
		And it has a "text field" with id "input-text"
		When I enter "foo" into it
		Then it has the value "foo"

		When I enter bar into the "text field" with id input-hidden
		Then it has the value bar

		Given the form has a panel
		And this panel has one button with label "Send"
		When I click the button
		Then the former "text field" has the value "bam!"
		And the latter "text field" has the value "bar"

	Scenario: has a selector with selected options

		Given page form is loaded
		And there is a form
		Then it has a selector
		And it has four options
		And the first option is not selected
		And the second option is selected
		And the third option is unselected
		And there are two options which are selected
		And there are two options which are unselected
