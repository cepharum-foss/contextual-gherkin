const { describe, it } = require( "mocha" );
require( "should" );

const { customTypes } = require( "@cepharum/contextual-gherkin/steps/types" );

const type = customTypes.find( t => t.name === "contextual-word" );

describe( "Custom cucumber expression {contextual-word}", () => {
	it( "is defined", () => {
		type.should.be.Object().which.has.properties( "name", "regexp", "transformer", "type" );
	} );

	it( "supports different phrases for describing previous matches", () => {
		assert( "it", { index: 0, isRefining: false, sourceCardinality: "singular", targetCardinality: "singular" } );
		assert( "them", { index: 0, isRefining: false, sourceCardinality: "plural", targetCardinality: "plural" } );
		assert( "they", { index: 0, isRefining: false, sourceCardinality: "plural", targetCardinality: "plural" } );

		assert( "the latter", { index: 0, isRefining: false } );
		assert( "the former", { index: 1, isRefining: false } );

		assert( "the button", { index: 0, word: "button", isRefining: false, sourceCardinality: "singular", targetCardinality: "singular" } );
		assert( "the buttons", { index: 0, word: "button", isRefining: false, sourceCardinality: "plural", targetCardinality: "plural" } );

		assert( "this button", { index: 0, word: "button", isRefining: false, sourceCardinality: "singular", targetCardinality: "singular" } );
		assert( "that button", { index: 1, word: "button", isRefining: false, sourceCardinality: "singular", targetCardinality: "singular" } );
		assert( "these buttons", { index: 0, word: "button", isRefining: false, sourceCardinality: "plural", targetCardinality: "plural" } );
		assert( "those buttons", { index: 1, word: "button", isRefining: false, sourceCardinality: "plural", targetCardinality: "plural" } );

		assert( "this buttons", { index: 0, word: "button", isRefining: false, sourceCardinality: null, targetCardinality: null } );
		assert( "that buttons", { index: 1, word: "button", isRefining: false, sourceCardinality: null, targetCardinality: null } );
		assert( "these button", { index: 0, word: "button", isRefining: false, sourceCardinality: null, targetCardinality: null } );
		assert( "those button", { index: 1, word: "button", isRefining: false, sourceCardinality: null, targetCardinality: null } );

		assert( "the former icon", { index: 1, word: "icon", isRefining: false, sourceCardinality: "singular", targetCardinality: "singular" } );
		assert( "the latter icon", { index: 0, word: "icon", isRefining: false, sourceCardinality: "singular", targetCardinality: "singular" } );
		assert( "the former icons", { index: 1, word: "icon", isRefining: false, sourceCardinality: "plural", targetCardinality: "plural" } );
		assert( "the latter icons", { index: 0, word: "icon", isRefining: false, sourceCardinality: "plural", targetCardinality: "plural" } );

		assert( "the 1. icon", { index: 0, word: "icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 2. icon", { index: 0, word: "icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 3. icon", { index: 0, word: "icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 4. icon", { index: 0, word: "icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 5. icon", { index: 0, word: "icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 6. icon", { index: 0, word: "icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 7. icon", { index: 0, word: "icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 8. icon", { index: 0, word: "icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 9. icon", { index: 0, word: "icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 10. icon", { index: 0, word: "icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 11. icon", { index: 0, word: "icon", subIndex: 10, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 123. icon", { index: 0, word: "icon", subIndex: 122, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 123456789. icon", { index: 0, word: "icon", subIndex: 123456788, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( "the 1st icon", { index: 0, word: "icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 2nd icon", { index: 0, word: "icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 3rd icon", { index: 0, word: "icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 4th icon", { index: 0, word: "icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 5th icon", { index: 0, word: "icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 6th icon", { index: 0, word: "icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 7th icon", { index: 0, word: "icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 8th icon", { index: 0, word: "icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 9th icon", { index: 0, word: "icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 10th icon", { index: 0, word: "icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( "the first icon", { index: 0, word: "icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the second icon", { index: 0, word: "icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the third icon", { index: 0, word: "icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the fourth icon", { index: 0, word: "icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the fifth icon", { index: 0, word: "icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the sixth icon", { index: 0, word: "icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the seventh icon", { index: 0, word: "icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the eighth icon", { index: 0, word: "icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the ninth icon", { index: 0, word: "icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the tenth icon", { index: 0, word: "icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( "the 1. of these icons", { index: 0, word: "icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 2. of these icons", { index: 0, word: "icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 3. of these icons", { index: 0, word: "icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 4. of these icons", { index: 0, word: "icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 5. of these icons", { index: 0, word: "icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 6. of these icons", { index: 0, word: "icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 7. of these icons", { index: 0, word: "icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 8. of these icons", { index: 0, word: "icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 9. of these icons", { index: 0, word: "icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 10. of these icons", { index: 0, word: "icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 11. of these icons", { index: 0, word: "icon", subIndex: 10, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 123. of these icons", { index: 0, word: "icon", subIndex: 122, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 123456789. of these icons", { index: 0, word: "icon", subIndex: 123456788, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( "the 1st of these icons", { index: 0, word: "icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 2nd of these icons", { index: 0, word: "icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 3rd of these icons", { index: 0, word: "icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 4th of these icons", { index: 0, word: "icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 5th of these icons", { index: 0, word: "icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 6th of these icons", { index: 0, word: "icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 7th of these icons", { index: 0, word: "icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 8th of these icons", { index: 0, word: "icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 9th of these icons", { index: 0, word: "icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 10th of these icons", { index: 0, word: "icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( "the first of these icons", { index: 0, word: "icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the second of these icons", { index: 0, word: "icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the third of these icons", { index: 0, word: "icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the fourth of these icons", { index: 0, word: "icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the fifth of these icons", { index: 0, word: "icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the sixth of these icons", { index: 0, word: "icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the seventh of these icons", { index: 0, word: "icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the eighth of these icons", { index: 0, word: "icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the ninth of these icons", { index: 0, word: "icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the tenth of these icons", { index: 0, word: "icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( "the 1. of those icons", { index: 1, word: "icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 2. of those icons", { index: 1, word: "icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 3. of those icons", { index: 1, word: "icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 4. of those icons", { index: 1, word: "icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 5. of those icons", { index: 1, word: "icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 6. of those icons", { index: 1, word: "icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 7. of those icons", { index: 1, word: "icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 8. of those icons", { index: 1, word: "icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 9. of those icons", { index: 1, word: "icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 10. of those icons", { index: 1, word: "icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 11. of those icons", { index: 1, word: "icon", subIndex: 10, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 123. of those icons", { index: 1, word: "icon", subIndex: 122, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 123456789. of those icons", { index: 1, word: "icon", subIndex: 123456788, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( "the 1st of those icons", { index: 1, word: "icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 2nd of those icons", { index: 1, word: "icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 3rd of those icons", { index: 1, word: "icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 4th of those icons", { index: 1, word: "icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 5th of those icons", { index: 1, word: "icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 6th of those icons", { index: 1, word: "icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 7th of those icons", { index: 1, word: "icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 8th of those icons", { index: 1, word: "icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 9th of those icons", { index: 1, word: "icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 10th of those icons", { index: 1, word: "icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( "the first of those icons", { index: 1, word: "icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the second of those icons", { index: 1, word: "icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the third of those icons", { index: 1, word: "icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the fourth of those icons", { index: 1, word: "icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the fifth of those icons", { index: 1, word: "icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the sixth of those icons", { index: 1, word: "icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the seventh of those icons", { index: 1, word: "icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the eighth of those icons", { index: 1, word: "icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the ninth of those icons", { index: 1, word: "icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the tenth of those icons", { index: 1, word: "icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( "the 1. of the latter icons", { index: 0, word: "icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 2. of the latter icons", { index: 0, word: "icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 3. of the latter icons", { index: 0, word: "icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 4. of the latter icons", { index: 0, word: "icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 5. of the latter icons", { index: 0, word: "icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 6. of the latter icons", { index: 0, word: "icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 7. of the latter icons", { index: 0, word: "icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 8. of the latter icons", { index: 0, word: "icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 9. of the latter icons", { index: 0, word: "icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 10. of the latter icons", { index: 0, word: "icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 11. of the latter icons", { index: 0, word: "icon", subIndex: 10, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 123. of the latter icons", { index: 0, word: "icon", subIndex: 122, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 123456789. of the latter icons", { index: 0, word: "icon", subIndex: 123456788, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( "the 1st of the latter icons", { index: 0, word: "icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 2nd of the latter icons", { index: 0, word: "icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 3rd of the latter icons", { index: 0, word: "icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 4th of the latter icons", { index: 0, word: "icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 5th of the latter icons", { index: 0, word: "icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 6th of the latter icons", { index: 0, word: "icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 7th of the latter icons", { index: 0, word: "icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 8th of the latter icons", { index: 0, word: "icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 9th of the latter icons", { index: 0, word: "icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 10th of the latter icons", { index: 0, word: "icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( "the first of the latter icons", { index: 0, word: "icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the second of the latter icons", { index: 0, word: "icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the third of the latter icons", { index: 0, word: "icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the fourth of the latter icons", { index: 0, word: "icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the fifth of the latter icons", { index: 0, word: "icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the sixth of the latter icons", { index: 0, word: "icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the seventh of the latter icons", { index: 0, word: "icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the eighth of the latter icons", { index: 0, word: "icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the ninth of the latter icons", { index: 0, word: "icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the tenth of the latter icons", { index: 0, word: "icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( "the 1. of the former icons", { index: 1, word: "icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 2. of the former icons", { index: 1, word: "icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 3. of the former icons", { index: 1, word: "icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 4. of the former icons", { index: 1, word: "icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 5. of the former icons", { index: 1, word: "icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 6. of the former icons", { index: 1, word: "icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 7. of the former icons", { index: 1, word: "icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 8. of the former icons", { index: 1, word: "icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 9. of the former icons", { index: 1, word: "icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 10. of the former icons", { index: 1, word: "icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 11. of the former icons", { index: 1, word: "icon", subIndex: 10, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 123. of the former icons", { index: 1, word: "icon", subIndex: 122, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 123456789. of the former icons", { index: 1, word: "icon", subIndex: 123456788, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( "the 1st of the former icons", { index: 1, word: "icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 2nd of the former icons", { index: 1, word: "icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 3rd of the former icons", { index: 1, word: "icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 4th of the former icons", { index: 1, word: "icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 5th of the former icons", { index: 1, word: "icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 6th of the former icons", { index: 1, word: "icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 7th of the former icons", { index: 1, word: "icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 8th of the former icons", { index: 1, word: "icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 9th of the former icons", { index: 1, word: "icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the 10th of the former icons", { index: 1, word: "icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( "the first of the former icons", { index: 1, word: "icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the second of the former icons", { index: 1, word: "icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the third of the former icons", { index: 1, word: "icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the fourth of the former icons", { index: 1, word: "icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the fifth of the former icons", { index: 1, word: "icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the sixth of the former icons", { index: 1, word: "icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the seventh of the former icons", { index: 1, word: "icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the eighth of the former icons", { index: 1, word: "icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the ninth of the former icons", { index: 1, word: "icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( "the tenth of the former icons", { index: 1, word: "icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
	} );

	it( "supports optional quotes for using word with whitespace", () => {
		assert( 'the "fancy button"', { index: 0, word: "fancy button", isRefining: false, sourceCardinality: "singular", targetCardinality: "singular" } );
		assert( 'the "fancy buttons"', { index: 0, word: "fancy button", isRefining: false, sourceCardinality: "plural", targetCardinality: "plural" } );

		assert( 'this "fancy button"', { index: 0, word: "fancy button", isRefining: false, sourceCardinality: "singular", targetCardinality: "singular" } );
		assert( 'that "fancy button"', { index: 1, word: "fancy button", isRefining: false, sourceCardinality: "singular", targetCardinality: "singular" } );
		assert( 'these "fancy buttons"', { index: 0, word: "fancy button", isRefining: false, sourceCardinality: "plural", targetCardinality: "plural" } );
		assert( 'those "fancy buttons"', { index: 1, word: "fancy button", isRefining: false, sourceCardinality: "plural", targetCardinality: "plural" } );

		assert( 'this "fancy buttons"', { index: 0, word: "fancy button", isRefining: false, sourceCardinality: null, targetCardinality: null } );
		assert( 'that "fancy buttons"', { index: 1, word: "fancy button", isRefining: false, sourceCardinality: null, targetCardinality: null } );
		assert( 'these "fancy button"', { index: 0, word: "fancy button", isRefining: false, sourceCardinality: null, targetCardinality: null } );
		assert( 'those "fancy button"', { index: 1, word: "fancy button", isRefining: false, sourceCardinality: null, targetCardinality: null } );

		assert( 'the former "fancy icon"', { index: 1, word: "fancy icon", isRefining: false, sourceCardinality: "singular", targetCardinality: "singular" } );
		assert( 'the latter "fancy icon"', { index: 0, word: "fancy icon", isRefining: false, sourceCardinality: "singular", targetCardinality: "singular" } );
		assert( 'the former "fancy icons"', { index: 1, word: "fancy icon", isRefining: false, sourceCardinality: "plural", targetCardinality: "plural" } );
		assert( 'the latter "fancy icons"', { index: 0, word: "fancy icon", isRefining: false, sourceCardinality: "plural", targetCardinality: "plural" } );

		assert( 'the 1. "fancy icon"', { index: 0, word: "fancy icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 2. "fancy icon"', { index: 0, word: "fancy icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 3. "fancy icon"', { index: 0, word: "fancy icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 4. "fancy icon"', { index: 0, word: "fancy icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 5. "fancy icon"', { index: 0, word: "fancy icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 6. "fancy icon"', { index: 0, word: "fancy icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 7. "fancy icon"', { index: 0, word: "fancy icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 8. "fancy icon"', { index: 0, word: "fancy icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 9. "fancy icon"', { index: 0, word: "fancy icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 10. "fancy icon"', { index: 0, word: "fancy icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 11. "fancy icon"', { index: 0, word: "fancy icon", subIndex: 10, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 123. "fancy icon"', { index: 0, word: "fancy icon", subIndex: 122, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 123456789. "fancy icon"', { index: 0, word: "fancy icon", subIndex: 123456788, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( 'the 1st "fancy icon"', { index: 0, word: "fancy icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 2nd "fancy icon"', { index: 0, word: "fancy icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 3rd "fancy icon"', { index: 0, word: "fancy icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 4th "fancy icon"', { index: 0, word: "fancy icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 5th "fancy icon"', { index: 0, word: "fancy icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 6th "fancy icon"', { index: 0, word: "fancy icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 7th "fancy icon"', { index: 0, word: "fancy icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 8th "fancy icon"', { index: 0, word: "fancy icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 9th "fancy icon"', { index: 0, word: "fancy icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 10th "fancy icon"', { index: 0, word: "fancy icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( 'the first "fancy icon"', { index: 0, word: "fancy icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the second "fancy icon"', { index: 0, word: "fancy icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the third "fancy icon"', { index: 0, word: "fancy icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the fourth "fancy icon"', { index: 0, word: "fancy icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the fifth "fancy icon"', { index: 0, word: "fancy icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the sixth "fancy icon"', { index: 0, word: "fancy icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the seventh "fancy icon"', { index: 0, word: "fancy icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the eighth "fancy icon"', { index: 0, word: "fancy icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the ninth "fancy icon"', { index: 0, word: "fancy icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the tenth "fancy icon"', { index: 0, word: "fancy icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( 'the 1. of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 2. of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 3. of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 4. of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 5. of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 6. of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 7. of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 8. of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 9. of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 10. of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 11. of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 10, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 123. of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 122, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 123456789. of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 123456788, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( 'the 1st of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 2nd of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 3rd of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 4th of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 5th of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 6th of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 7th of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 8th of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 9th of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 10th of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( 'the first of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the second of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the third of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the fourth of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the fifth of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the sixth of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the seventh of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the eighth of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the ninth of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the tenth of these "fancy icons"', { index: 0, word: "fancy icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( 'the 1. of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 2. of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 3. of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 4. of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 5. of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 6. of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 7. of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 8. of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 9. of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 10. of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 11. of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 10, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 123. of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 122, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 123456789. of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 123456788, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( 'the 1st of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 2nd of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 3rd of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 4th of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 5th of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 6th of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 7th of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 8th of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 9th of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 10th of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( 'the first of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the second of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the third of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the fourth of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the fifth of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the sixth of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the seventh of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the eighth of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the ninth of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the tenth of those "fancy icons"', { index: 1, word: "fancy icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( 'the 1. of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 2. of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 3. of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 4. of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 5. of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 6. of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 7. of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 8. of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 9. of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 10. of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 11. of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 10, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 123. of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 122, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 123456789. of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 123456788, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( 'the 1st of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 2nd of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 3rd of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 4th of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 5th of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 6th of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 7th of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 8th of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 9th of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 10th of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( 'the first of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the second of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the third of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the fourth of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the fifth of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the sixth of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the seventh of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the eighth of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the ninth of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the tenth of the latter "fancy icons"', { index: 0, word: "fancy icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( 'the 1. of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 2. of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 3. of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 4. of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 5. of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 6. of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 7. of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 8. of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 9. of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 10. of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 11. of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 10, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 123. of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 122, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 123456789. of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 123456788, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( 'the 1st of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 2nd of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 3rd of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 4th of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 5th of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 6th of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 7th of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 8th of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 9th of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the 10th of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );

		assert( 'the first of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 0, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the second of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 1, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the third of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 2, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the fourth of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 3, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the fifth of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 4, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the sixth of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 5, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the seventh of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 6, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the eighth of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 7, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the ninth of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 8, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
		assert( 'the tenth of the former "fancy icons"', { index: 1, word: "fancy icon", subIndex: 9, isRefining: true, sourceCardinality: "plural", targetCardinality: "singular" } );
	} );
} );

function assert( phrase, data ) {
	type.regexp.exec( phrase )[0].should.be.equal( phrase );

	const object = type.transformer( ...[].slice.call( type.regexp.exec( phrase ), 1 ) );

	for ( const prop of [ "index", "word", "subIndex", "isRefining", "sourceCardinality", "targetCardinality" ] ) {
		( ( object[prop] ?? undefined ) === ( ( data[prop] ?? undefined ) ) )
			.should.be.true( `mismatching ${prop} on phrase "${phrase}", expected: ${data[prop]}, actual: ${object[prop]}` );
	}
}
