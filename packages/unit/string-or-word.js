const { describe, it } = require( "mocha" );
require( "should" );

const { customTypes } = require( "@cepharum/contextual-gherkin/steps/types" );

const type = customTypes.find( t => t.name === "string-or-word" );

describe( "Custom cucumber expression {string-or-word}", () => {
	it( "is defined", () => {
		type.should.be.Object().which.has.properties( "name", "regexp", "transformer", "type" );
	} );

	it( "matches sequence of characters without whitespace or double quotes", () => {
		assert( "button", "button" );
	} );

	it( "matches sequence of characters with whitespace wrapped in double quotes", () => {
		assert( '"fancy button"', "fancy button" );
	} );

	it( "matches empty sequence of characters wrapped in double quotes", () => {
		assert( '""', "" );
	} );
} );

function assert( phrase, expected ) {
	const actual = type.transformer( ...[].slice.call( type.regexp.exec( phrase ), 1 ) );

	actual.should.be.equal( expected );
}
