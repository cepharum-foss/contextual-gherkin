const { describe, it } = require( "mocha" );
require( "should" );
const { Api } = require( "@cepharum/contextual-gherkin/lib/api" );

describe( "contextual-gherkin API", () => {
	it( "is available", () => {
		Api.should.be.Function();
	} );

	it( "can be created", () => {
		( () => new Api( {}, {} ) ).should.not.throw();
	} );

	it( "creates empty set of aliases by default", () => {
		const a = new Api( {}, {} );

		a.config.aliases.should.be.Object().which.is.empty();
	} );

	it( "supports simple definitions of 1:1 aliases in configuration", () => {
		( () => new Api( {}, {
			aliases: {
				alias: "original"
			},
		} ) ).should.not.throw();
	} );

	it( "supports regular definitions of 1:n aliases in configuration", () => {
		( () => new Api( {}, {
			aliases: {
				alias: ["original"],
				altAlias: [ "original", "altOriginal" ]
			},
		} ) ).should.not.throw();
	} );

	it( "always fetches at least the given name as alias of itself", () => {
		const api = new Api( {}, {
			aliases: {
				alias: ["original"],
				altAlias: [ "original", "altOriginal" ]
			},
		} );

		api.getAliases( "alias" ).should.be.deepEqual( ["alias"] );
	} );

	describe( "supports aliasing of element names which", () => {
		it( "fetches a provided name and all its defined aliases", () => {
			const api = new Api( {}, {
				aliases: {
					alias: [ "original", "aaa" ],
					"alt alias": [ "original", "bbb" ]
				},
			} );

			api.getAliases( "original" ).should.be.deepEqual( [ "alias", "alt alias", "original" ] );
		} );

		it( "recursively fetches all aliases for a given name", () => {
			const api = new Api( {}, {
				aliases: {
					alias: ["original"],
					"alt alias": [ "alias", "bbb" ]
				},
			} );

			api.getAliases( "original" ).should.be.deepEqual( [ "alias", "alt alias", "original" ] );
		} );

		it( "always delivers list of found aliases in alphabetical order", () => {
			const api = new Api( {}, {
				aliases: {
					foo: ["bar"],
					bam: ["zzz"]
				},
			} );

			api.getAliases( "bar" ).should.be.deepEqual( [ "bar", "foo" ] );
			api.getAliases( "zzz" ).should.be.deepEqual( [ "bam", "zzz" ] );
		} );

		it( "deals with circular definitions of aliases", () => {
			const api = new Api( {}, {
				aliases: {
					alias: ["original"],
					original: ["alias"]
				},
			} );

			api.getAliases( "original" ).should.be.deepEqual( [ "alias", "original" ] );
			api.getAliases( "alias" ).should.be.deepEqual( [ "alias", "original" ] );
		} );

		it( "fetches some provided name, only, if there are no aliases defined", () => {
			const api = new Api( {}, {
				aliases: {
					alias: [ "original", "aaa" ],
					"alt alias": [ "original", "bbb" ]
				},
			} );

			api.getAliases( "alias" ).should.be.deepEqual( ["alias"] );
			api.getAliases( "alt alias" ).should.be.deepEqual( ["alt alias"] );
			api.getAliases( "foo" ).should.be.deepEqual( ["foo"] );
		} );

		it( "works case-insensitively by converting all names to their lower-case variants", () => {
			const api = new Api( {}, {
				aliases: {
					alias: [ "original", "aaa" ],
					altAlias: [ "original", "altOriginal" ],
					altOriginal: ["altAlias"]
				},
			} );

			api.getAliases( "alias" ).should.be.deepEqual( ["alias"] );
			api.getAliases( "altAlias" ).should.be.deepEqual( [ "altalias", "altoriginal" ] );
			api.getAliases( "foo" ).should.be.deepEqual( ["foo"] );
			api.getAliases( "altOriginal" ).should.be.deepEqual( [ "altalias", "altoriginal" ] );
		} );
	} );
} );
