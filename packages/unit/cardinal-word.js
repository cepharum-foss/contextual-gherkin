const { describe, it } = require( "mocha" );
require( "should" );

const { customTypes } = require( "@cepharum/contextual-gherkin/steps/types" );

const type = customTypes.find( t => t.name === "cardinal-word" );

describe( "Custom cucumber expression {cardinal-word}", () => {
	it( "is defined", () => {
		type.should.be.Object().which.has.properties( "name", "regexp", "transformer", "type" );
	} );

	it( "supports different phrases for describing amounts", () => {
		assert( "a button", { number: 1, word: "button", mode: ">=", cardinality: "singular" } );
		assert( "an icon", { number: 1, word: "icon", mode: ">=", cardinality: "singular" } );
		assert( "the icon", { number: 1, word: "icon", mode: "==", cardinality: "singular" } );
		assert( "the icons", { number: 2, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "icons", { number: 1, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "no icon", { number: 0, word: "icon", mode: "==", cardinality: "singular" } );
		assert( "no icons", { number: 0, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "1 icon", { number: 1, word: "icon", mode: "==", cardinality: "singular" } );
		assert( "2 icons", { number: 2, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "3 icons", { number: 3, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "4 icons", { number: 4, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "5 icons", { number: 5, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "6 icons", { number: 6, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "7 icons", { number: 7, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "8 icons", { number: 8, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "9 icons", { number: 9, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "10 icons", { number: 10, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "one icon", { number: 1, word: "icon", mode: "==", cardinality: "singular" } );
		assert( "two icons", { number: 2, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "three icons", { number: 3, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "four icons", { number: 4, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "five icons", { number: 5, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "six icons", { number: 6, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "seven icons", { number: 7, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "eight icons", { number: 8, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "nine icons", { number: 9, word: "icon", mode: "==", cardinality: "plural" } );
		assert( "ten icons", { number: 10, word: "icon", mode: "==", cardinality: "plural" } );
	} );

	it( "supports different phrases for describing minimum amounts", () => {
		assert( "at least a button", { number: 1, word: "button", mode: ">=", cardinality: "singular" } );
		assert( "at least an icon", { number: 1, word: "icon", mode: ">=", cardinality: "singular" } );
		( () => assert( "at least the icon", { number: 1, word: "icon", mode: ">=", cardinality: "singular" } ) ).should.throw( /\bunspecific\b/ );
		( () => assert( "at least the icons", { number: 1, word: "icon", mode: ">=", cardinality: "plural" } ) ).should.throw( /\bunspecific\b/ );
		( () => assert( "at least icons", { number: 1, word: "icon", mode: ">=", cardinality: "plural" } ) ).should.throw( /\bwithout particular limit\b/ );
		( () => assert( "at least no icon", { number: 0, word: "icon", mode: ">=", cardinality: "singular" } ) ).should.throw( /\b0 amount\b/ );
		( () => assert( "at least no icons", { number: 0, word: "icon", mode: ">=", cardinality: "plural" } ) ).should.throw( /\b0 amount\b/ );
		assert( "at least 1 icon", { number: 1, word: "icon", mode: ">=", cardinality: "singular" } );
		assert( "at least 2 icons", { number: 2, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "at least 3 icons", { number: 3, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "at least 4 icons", { number: 4, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "at least 5 icons", { number: 5, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "at least 6 icons", { number: 6, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "at least 7 icons", { number: 7, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "at least 8 icons", { number: 8, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "at least 9 icons", { number: 9, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "at least 10 icons", { number: 10, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "at least one icon", { number: 1, word: "icon", mode: ">=", cardinality: "singular" } );
		assert( "at least two icons", { number: 2, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "at least three icons", { number: 3, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "at least four icons", { number: 4, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "at least five icons", { number: 5, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "at least six icons", { number: 6, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "at least seven icons", { number: 7, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "at least eight icons", { number: 8, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "at least nine icons", { number: 9, word: "icon", mode: ">=", cardinality: "plural" } );
		assert( "at least ten icons", { number: 10, word: "icon", mode: ">=", cardinality: "plural" } );
	} );

	it( "supports different phrases for describing maximum amounts", () => {
		assert( "at most a button", { number: 1, word: "button", mode: "<=", cardinality: "singular" } );
		assert( "at most an icon", { number: 1, word: "icon", mode: "<=", cardinality: "singular" } );
		( () => assert( "at most the icon", { number: 1, word: "icon", mode: "<=", cardinality: "singular" } ) ).should.throw( /\bunspecific\b/ );
		( () => assert( "at most the icons", { number: 1, word: "icon", mode: "<=", cardinality: "plural" } ) ).should.throw( /\bunspecific\b/ );
		( () => assert( "at most icons", { number: 1, word: "icon", mode: "<=", cardinality: "plural" } ) ).should.throw( /\bwithout particular limit\b/ );
		( () => assert( "at most no icon", { number: 0, word: "icon", mode: "<=", cardinality: "singular" } ) ).should.throw( /\b0 amount\b/ );
		( () => assert( "at most no icons", { number: 0, word: "icon", mode: "<=", cardinality: "plural" } ) ).should.throw( /\b0 amount\b/ );
		assert( "at most 1 icon", { number: 1, word: "icon", mode: "<=", cardinality: "singular" } );
		assert( "at most 2 icons", { number: 2, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "at most 3 icons", { number: 3, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "at most 4 icons", { number: 4, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "at most 5 icons", { number: 5, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "at most 6 icons", { number: 6, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "at most 7 icons", { number: 7, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "at most 8 icons", { number: 8, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "at most 9 icons", { number: 9, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "at most 10 icons", { number: 10, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "at most one icon", { number: 1, word: "icon", mode: "<=", cardinality: "singular" } );
		assert( "at most two icons", { number: 2, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "at most three icons", { number: 3, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "at most four icons", { number: 4, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "at most five icons", { number: 5, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "at most six icons", { number: 6, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "at most seven icons", { number: 7, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "at most eight icons", { number: 8, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "at most nine icons", { number: 9, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "at most ten icons", { number: 10, word: "icon", mode: "<=", cardinality: "plural" } );

		assert( "up to a button", { number: 1, word: "button", mode: "<=", cardinality: "singular" } );
		assert( "up to an icon", { number: 1, word: "icon", mode: "<=", cardinality: "singular" } );
		( () => assert( "up to the icon", { number: 1, word: "icon", mode: "<=", cardinality: "singular" } ) ).should.throw( /\bunspecific\b/ );
		( () => assert( "up to the icons", { number: 1, word: "icon", mode: "<=", cardinality: "plural" } ) ).should.throw( /\bunspecific\b/ );
		( () => assert( "up to icons", { number: 1, word: "icon", mode: "<=", cardinality: "plural" } ) ).should.throw( /\bwithout particular limit\b/ );
		( () => assert( "up to no icon", { number: 0, word: "icon", mode: "<=", cardinality: "singular" } ) ).should.throw( /\b0 amount\b/ );
		( () => assert( "up to no icons", { number: 0, word: "icon", mode: "<=", cardinality: "plural" } ) ).should.throw( /\b0 amount\b/ );
		assert( "up to 1 icon", { number: 1, word: "icon", mode: "<=", cardinality: "singular" } );
		assert( "up to 2 icons", { number: 2, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "up to 3 icons", { number: 3, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "up to 4 icons", { number: 4, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "up to 5 icons", { number: 5, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "up to 6 icons", { number: 6, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "up to 7 icons", { number: 7, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "up to 8 icons", { number: 8, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "up to 9 icons", { number: 9, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "up to 10 icons", { number: 10, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "up to one icon", { number: 1, word: "icon", mode: "<=", cardinality: "singular" } );
		assert( "up to two icons", { number: 2, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "up to three icons", { number: 3, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "up to four icons", { number: 4, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "up to five icons", { number: 5, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "up to six icons", { number: 6, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "up to seven icons", { number: 7, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "up to eight icons", { number: 8, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "up to nine icons", { number: 9, word: "icon", mode: "<=", cardinality: "plural" } );
		assert( "up to ten icons", { number: 10, word: "icon", mode: "<=", cardinality: "plural" } );
	} );

	it( "supports optional quotes for using words with whitespace", () => {
		assert( 'a "fancy button"', { number: 1, word: "fancy button", mode: ">=", cardinality: "singular" } );
		assert( 'an "fancy icon"', { number: 1, word: "fancy icon", mode: ">=", cardinality: "singular" } );
		assert( 'the "fancy icon"', { number: 1, word: "fancy icon", mode: "==", cardinality: "singular" } );
		assert( 'the "fancy icons"', { number: 2, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( '"fancy icons"', { number: 1, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'no "fancy icon"', { number: 0, word: "fancy icon", mode: "==", cardinality: "singular" } );
		assert( 'no "fancy icons"', { number: 0, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( '1 "fancy icon"', { number: 1, word: "fancy icon", mode: "==", cardinality: "singular" } );
		assert( '2 "fancy icons"', { number: 2, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( '3 "fancy icons"', { number: 3, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( '4 "fancy icons"', { number: 4, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( '5 "fancy icons"', { number: 5, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( '6 "fancy icons"', { number: 6, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( '7 "fancy icons"', { number: 7, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( '8 "fancy icons"', { number: 8, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( '9 "fancy icons"', { number: 9, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( '10 "fancy icons"', { number: 10, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( 'one "fancy icon"', { number: 1, word: "fancy icon", mode: "==", cardinality: "singular" } );
		assert( 'two "fancy icons"', { number: 2, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( 'three "fancy icons"', { number: 3, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( 'four "fancy icons"', { number: 4, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( 'five "fancy icons"', { number: 5, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( 'six "fancy icons"', { number: 6, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( 'seven "fancy icons"', { number: 7, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( 'eight "fancy icons"', { number: 8, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( 'nine "fancy icons"', { number: 9, word: "fancy icon", mode: "==", cardinality: "plural" } );
		assert( 'ten "fancy icons"', { number: 10, word: "fancy icon", mode: "==", cardinality: "plural" } );
	} );

	it( "supports optional quotes for using words with whitespace when describing minimum amounts", () => {
		assert( 'at least a "fancy button"', { number: 1, word: "fancy button", mode: ">=", cardinality: "singular" } );
		assert( 'at least an "fancy icon"', { number: 1, word: "fancy icon", mode: ">=", cardinality: "singular" } );
		( () => assert( 'at least the "fancy icon"', { number: 1, word: "fancy icon", mode: ">=", cardinality: "singular" } ) ).should.throw( /\bunspecific\b/ );
		( () => assert( 'at least the "fancy icons"', { number: 1, word: "fancy icon", mode: ">=", cardinality: "plural" } ) ).should.throw( /\bunspecific\b/ );
		( () => assert( 'at least "fancy icons"', { number: 1, word: "fancy icon", mode: ">=", cardinality: "plural" } ) ).should.throw( /\bwithout particular limit\b/ );
		( () => assert( 'at least no "fancy icon"', { number: 0, word: "fancy icon", mode: ">=", cardinality: "singular" } ) ).should.throw( /\b0 amount\b/ );
		( () => assert( 'at least no "fancy icons"', { number: 0, word: "fancy icon", mode: ">=", cardinality: "plural" } ) ).should.throw( /\b0 amount\b/ );
		assert( 'at least 1 "fancy icon"', { number: 1, word: "fancy icon", mode: ">=", cardinality: "singular" } );
		assert( 'at least 2 "fancy icons"', { number: 2, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'at least 3 "fancy icons"', { number: 3, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'at least 4 "fancy icons"', { number: 4, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'at least 5 "fancy icons"', { number: 5, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'at least 6 "fancy icons"', { number: 6, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'at least 7 "fancy icons"', { number: 7, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'at least 8 "fancy icons"', { number: 8, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'at least 9 "fancy icons"', { number: 9, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'at least 10 "fancy icons"', { number: 10, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'at least one "fancy icon"', { number: 1, word: "fancy icon", mode: ">=", cardinality: "singular" } );
		assert( 'at least two "fancy icons"', { number: 2, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'at least three "fancy icons"', { number: 3, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'at least four "fancy icons"', { number: 4, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'at least five "fancy icons"', { number: 5, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'at least six "fancy icons"', { number: 6, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'at least seven "fancy icons"', { number: 7, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'at least eight "fancy icons"', { number: 8, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'at least nine "fancy icons"', { number: 9, word: "fancy icon", mode: ">=", cardinality: "plural" } );
		assert( 'at least ten "fancy icons"', { number: 10, word: "fancy icon", mode: ">=", cardinality: "plural" } );
	} );

	it( "supports optional quotes for using words with whitespace when describing minimum amounts", () => {
		assert( 'at most a "fancy button"', { number: 1, word: "fancy button", mode: "<=", cardinality: "singular" } );
		assert( 'at most an "fancy icon"', { number: 1, word: "fancy icon", mode: "<=", cardinality: "singular" } );
		( () => assert( 'at most the "fancy icon"', { number: 1, word: "fancy icon", mode: "<=", cardinality: "singular" } ) ).should.throw( /\bunspecific\b/ );
		( () => assert( 'at most the "fancy icons"', { number: 1, word: "fancy icon", mode: "<=", cardinality: "plural" } ) ).should.throw( /\bunspecific\b/ );
		( () => assert( 'at most "fancy icons"', { number: 1, word: "fancy icon", mode: "<=", cardinality: "plural" } ) ).should.throw( /\bwithout particular limit\b/ );
		( () => assert( 'at most no "fancy icon"', { number: 0, word: "fancy icon", mode: "<=", cardinality: "singular" } ) ).should.throw( /\b0 amount\b/ );
		( () => assert( 'at most no "fancy icons"', { number: 0, word: "fancy icon", mode: "<=", cardinality: "plural" } ) ).should.throw( /\b0 amount\b/ );
		assert( 'at most 1 "fancy icon"', { number: 1, word: "fancy icon", mode: "<=", cardinality: "singular" } );
		assert( 'at most 2 "fancy icons"', { number: 2, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'at most 3 "fancy icons"', { number: 3, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'at most 4 "fancy icons"', { number: 4, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'at most 5 "fancy icons"', { number: 5, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'at most 6 "fancy icons"', { number: 6, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'at most 7 "fancy icons"', { number: 7, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'at most 8 "fancy icons"', { number: 8, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'at most 9 "fancy icons"', { number: 9, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'at most 10 "fancy icons"', { number: 10, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'at most one "fancy icon"', { number: 1, word: "fancy icon", mode: "<=", cardinality: "singular" } );
		assert( 'at most two "fancy icons"', { number: 2, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'at most three "fancy icons"', { number: 3, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'at most four "fancy icons"', { number: 4, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'at most five "fancy icons"', { number: 5, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'at most six "fancy icons"', { number: 6, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'at most seven "fancy icons"', { number: 7, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'at most eight "fancy icons"', { number: 8, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'at most nine "fancy icons"', { number: 9, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'at most ten "fancy icons"', { number: 10, word: "fancy icon", mode: "<=", cardinality: "plural" } );

		assert( 'up to a "fancy button"', { number: 1, word: "fancy button", mode: "<=", cardinality: "singular" } );
		assert( 'up to an "fancy icon"', { number: 1, word: "fancy icon", mode: "<=", cardinality: "singular" } );
		( () => assert( 'up to the "fancy icon"', { number: 1, word: "fancy icon", mode: "<=", cardinality: "singular" } ) ).should.throw( /\bunspecific\b/ );
		( () => assert( 'up to the "fancy icons"', { number: 1, word: "fancy icon", mode: "<=", cardinality: "plural" } ) ).should.throw( /\bunspecific\b/ );
		( () => assert( 'up to "fancy icons"', { number: 1, word: "fancy icon", mode: "<=", cardinality: "plural" } ) ).should.throw( /\bwithout particular limit\b/ );
		( () => assert( 'up to no "fancy icon"', { number: 0, word: "fancy icon", mode: "<=", cardinality: "singular" } ) ).should.throw( /\b0 amount\b/ );
		( () => assert( 'up to no "fancy icons"', { number: 0, word: "fancy icon", mode: "<=", cardinality: "plural" } ) ).should.throw( /\b0 amount\b/ );
		assert( 'up to 1 "fancy icon"', { number: 1, word: "fancy icon", mode: "<=", cardinality: "singular" } );
		assert( 'up to 2 "fancy icons"', { number: 2, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'up to 3 "fancy icons"', { number: 3, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'up to 4 "fancy icons"', { number: 4, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'up to 5 "fancy icons"', { number: 5, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'up to 6 "fancy icons"', { number: 6, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'up to 7 "fancy icons"', { number: 7, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'up to 8 "fancy icons"', { number: 8, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'up to 9 "fancy icons"', { number: 9, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'up to 10 "fancy icons"', { number: 10, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'up to one "fancy icon"', { number: 1, word: "fancy icon", mode: "<=", cardinality: "singular" } );
		assert( 'up to two "fancy icons"', { number: 2, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'up to three "fancy icons"', { number: 3, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'up to four "fancy icons"', { number: 4, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'up to five "fancy icons"', { number: 5, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'up to six "fancy icons"', { number: 6, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'up to seven "fancy icons"', { number: 7, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'up to eight "fancy icons"', { number: 8, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'up to nine "fancy icons"', { number: 9, word: "fancy icon", mode: "<=", cardinality: "plural" } );
		assert( 'up to ten "fancy icons"', { number: 10, word: "fancy icon", mode: "<=", cardinality: "plural" } );
	} );
} );

function assert( phrase, data ) {
	const object = type.transformer( ...[].slice.call( type.regexp.exec( phrase ), 1 ) );

	for ( const prop of [ "number", "word", "mode", "cardinality" ] ) {
		( object[prop] === data[prop] )
			.should.be.true( `mismatching ${prop} on phrase "${phrase}", expected: ${data[prop]}, actual: ${object[prop]}` );
	}
}
